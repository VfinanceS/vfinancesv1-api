INSERT INTO `document_mstr` VALUES(18,'SUBMIT BORROWER APPLICATION FORM','pdf,jpg,jpeg,png',10,NOW(),1,NOW(),1,TRUE);
INSERT INTO `document_mstr` VALUES(19,'SACTIONED DOCUMENT','pdf,jpg,jpeg,png',10,NOW(),1,NOW(),1,TRUE);
INSERT INTO `document_mstr` VALUES(20,'DISBURSED DETAILS','pdf,jpg,jpeg,png',10,NOW(),1,NOW(),1,TRUE);


ALTER TABLE document_mapping_mstr ADD (reference_id BIGINT (20));
ALTER TABLE document_user_mapping_mstr ADD (reference_id BIGINT (20));

CREATE TABLE `payout_structure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application_type_id` bigint(20) DEFAULT NULL,
  `disbursment_from_amt` double unsigned DEFAULT NULL,
  `disbursment_to_amt` double unsigned DEFAULT NULL,
  `payout` double unsigned DEFAULT NULL,
  `version` varchar(20) DEFAULT NULL,
  `is_Active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

CREATE TABLE `user_payout_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `application_type_id` bigint(20) NOT NULL,
  `payout_version` varchar(20) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `sequence_data` (
    `sequence_name` VARCHAR(100) NOT NULL,
    `sequence_increment` INT(11) UNSIGNED NOT NULL DEFAULT 1,
    `sequence_min_value` INT(11) UNSIGNED NOT NULL DEFAULT 1,
    `sequence_max_value` BIGINT(20) UNSIGNED NOT NULL DEFAULT 18446744073709551615,
    `sequence_cur_value` BIGINT(20) UNSIGNED DEFAULT 1,
    `sequence_cycle` BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`sequence_name`)
);

INSERT INTO sequence_data (sequence_name) VALUE ('sq_payout_version');

ALTER TABLE applications ADD(
    coapplicant_user_id BIGINT(20) DEFAULT NULL,
    loan_category_id BIGINT(20) DEFAULT NULL,
    loan_br_remark LONGTEXT);
    
CREATE TABLE `loan_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appl_id` varchar(50) NOT NULL,
  `appl_type_id` bigint(20) NOT NULL,
  `br_user_id` bigint(20) DEFAULT NULL,
  `disbursment_date` datetime DEFAULT NULL,
  `disbursment_amount` double unsigned NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `user_coapplicant` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_user_id` bigint(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `gender` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `employment_type` bigint(20) DEFAULT NULL,
  `employer_name` varchar(45) DEFAULT NULL,
  `employment_address` varchar(45) DEFAULT NULL,
  `gross_monthly_income` double DEFAULT NULL,
  `total_work_experience` int(11) DEFAULT NULL,
  `self_employed_type` bigint(20) DEFAULT NULL,
  `entity_name` varchar(45) DEFAULT NULL,
  `entity_type` bigint(20) DEFAULT NULL,
  `salutation` bigint(20) unsigned DEFAULT NULL,
  `communication_add` bigint(20) DEFAULT NULL,
  `permanent_add` bigint(20) DEFAULT NULL,
  `pan_card` varchar(45) DEFAULT NULL,
  `aadhar_card_no` varchar(45) DEFAULT NULL,
  `edu_qualification` varchar(45) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `invitation_count` int(3) unsigned DEFAULT '0',
  `is_same_us_address` bit(1) DEFAULT NULL,
  `contact_person_name` varchar(45) DEFAULT NULL,
  `gst_number` varchar(45) DEFAULT NULL,
  `business_type_id` bigint(20) unsigned DEFAULT NULL,
  `channel_partner_id` bigint(20) unsigned DEFAULT NULL,
  `about_me` longtext,
  `is_profile_filled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `document_mstr` VALUES(21,'ADMIN REPORT','XLS,XLSX',10,NOW(),1,NOW(),1,TRUE);

CREATE TABLE `user_bank_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `bank_name` longtext NOT NULL,
  `account_number` bigint(20) NOT NULL,
  `account_name` longtext,
  `neft_ifsc_code` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bankdetailsuser` (`user_id`),
  CONSTRAINT `FK_bankdetailsuser` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);

CREATE TABLE `pre_applications` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `application_type_id` bigint(20) DEFAULT NULL,
  `loan_type_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `loan_amount` double DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'OPEN',
  `channel_partner_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_application_type_mst_idx` (`application_type_id`),
  KEY `fk_loan_type_mst_idx` (`loan_type_id`)
);

ALTER TABLE applications ADD pre_application_id BIGINT(20);
CREATE TABLE `user_data_configuration` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `agency_type_id` BIGINT(20) NOT NULL,
  `profile_image` BIT(1) DEFAULT NULL,
  `name` BIT(1) DEFAULT NULL,
  `email` BIT(1) DEFAULT NULL,
  `mobile` BIT(1) DEFAULT NULL,
  `first_name` BIT(1) DEFAULT NULL,
  `last_name` BIT(1) DEFAULT NULL,
  `middle_name` BIT(1) DEFAULT NULL,
  `birth_date` BIT(1) DEFAULT NULL,
  `gender` BIT(1) DEFAULT NULL,
  `bank` BIT(1) DEFAULT NULL,
  `created_by` BIGINT(20) DEFAULT NULL,
  `created_date` DATETIME DEFAULT NULL,
  `modified_by` BIGINT(20) DEFAULT NULL,
  `modified_date` DATETIME DEFAULT NULL,
  `employment_type` BIT(1) DEFAULT NULL,
  `employer_name` BIT(1) DEFAULT NULL,
  `employment_address` BIT(1) DEFAULT NULL,
  `gross_monthly_income` BIT(1) DEFAULT NULL,
  `total_work_experience` BIT(1) DEFAULT NULL,
  `self_employed_type` BIT(1) DEFAULT NULL,
  `entity_name` BIT(1) DEFAULT NULL,
  `entity_type` BIT(1) DEFAULT NULL,
  `salutation` BIT(1) DEFAULT NULL,
  `communication_add` BIT(1) DEFAULT NULL,
  `permanent_add` BIT(1) DEFAULT NULL,
  `pan_card` BIT(1) DEFAULT NULL,
  `aadhar_card_no` BIT(1) DEFAULT NULL,
  `edu_qualification` BIT(1) DEFAULT NULL,
  `contact_number` BIT(1) DEFAULT NULL,
  `contact_person_name` BIT(1) DEFAULT NULL,
  `gst_number` BIT(1) DEFAULT NULL,
  `business_type_id` BIT(1) DEFAULT NULL,
  `channel_partner_id` BIT(1) DEFAULT NULL,
  `about_me` BIT(1) DEFAULT NULL,
  `is_active` BIT(1) DEFAULT NULL,
  `coApplicant_details` BIT(1) DEFAULT NULL,
  `br_documents` BIT(1) DEFAULT NULL,
  `responded_documents` BIT(1) DEFAULT NULL,
  `coApplicant_documents` BIT(1) DEFAULT NULL,
  `existing_loan` BIT(1) DEFAULT NULL,
  `closed_loan` BIT(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `verification_reports` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `application_id` bigint(11) NOT NULL,
  `reported_date` datetime NOT NULL,
  `STATUS` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `questions_mstr` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `question_desc` varchar(300) NOT NULL,
  `agency_type` bigint(11) DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `agency_borrower_interview` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `application_id` bigint(11) NOT NULL,
  `question_id` bigint(11) NOT NULL,
  `answer` varchar(500) DEFAULT NULL,
  `agency_type` bigint(11) DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_question_mapping` (`question_id`),
  CONSTRAINT `fk_question_mapping` FOREIGN KEY (`question_id`) REFERENCES `questions_mstr` (`id`)
);

CREATE TABLE `user_data_configuration_version` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `configuration_version` varchar(50) NOT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`configuration_version`)
);

ALTER TABLE user_data_configuration ADD(`version` VARCHAR(50));

CREATE TABLE `promotional_codes` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(50) NOT NULL,
  `application_type_id` bigint(20) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `is_active` bit(1) NOT NULL,
  `used_by` bigint(11) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`id`,`promo_code`),
  UNIQUE KEY `id` (`id`),
  KEY `FK_promotional_codes` (`application_type_id`),
  CONSTRAINT `FK_promotional_codes` FOREIGN KEY (`application_type_id`) REFERENCES `application_type_mstr` (`id`)
) ;

CREATE TABLE `promo_code_transactions` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `application_id` bigint(20) DEFAULT NULL,
  `promo_code_id` bigint(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_promo_code_transactions_codes` (`promo_code_id`),
  CONSTRAINT `FK_promo_code_transactions_codes` FOREIGN KEY (`promo_code_id`) REFERENCES `promotional_codes` (`id`)
) ;

ALTER TABLE applications ADD(`promo_code` VARCHAR(50));
package com.lams.api.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.lams.api.domain.master.BankMstr;
import com.lams.api.repository.master.BankMstrRepository;
import com.lams.model.bo.report.DisbursmentDetails;
import com.lams.model.bo.report.LenderDetails;
import com.lams.model.bo.report.Reports;
import com.lams.model.utils.CommonUtils;

@Repository
public class ReportsJDBCImpl {

	public final static Logger logger = Logger.getLogger(ReportsJDBCImpl.class.getName());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private BankMstrRepository bankMstrRepository;

	public List<Reports> getReportData() {
		String sql=
		" SELECT  a.id applicationId,a.`created_date`,a.`lead_reference_no` loanReferenceNumber, "+
		" CONCAT( u.`first_name`,' ',u.`last_name`) borrowername,a.`user_id` bruserId,a.`coapplicant_user_id`, "+
		" a.`loan_type_id`,b.name applicationtype, a.`status` loanstatus,a.`loan_amount` loanamount,a.`bank_name`, "+ 
		" a.`loan_amount` br_applied_loan_amt  "+
		" FROM applications a, mstr_base b, `user` u "+ 
		" WHERE a.`application_type_id`=b.id   "+
		" AND u.`id`=a.`user_id`   "+
		" ORDER BY a.`created_date`  ";
		
		String getLendersResponded = 
		" SELECT CONCAT(u.`first_name`,' ',u.`last_name`)lenders_responded_name, lbc.status status,u.bank userbankid "+
		" FROM lender_borrower_connection lbc, `user` u "+
		" WHERE lbc.created_by=u.`id` "+
		" AND application_id=? ";
		
		String disbursmentDetails = " SELECT disbursment_date,disbursment_amount FROM `loan_transaction` WHERE appl_id=? ";
		
		String pdStatusSQL = " SELECT status  FROM `verification_reports` WHERE application_id=? ";
		
		List<Reports> reports = new ArrayList<Reports>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

		for (Map<String, Object> row : rows) {
			Reports report = new Reports();
			
			report.setLoanReferenceNumber((String)row.get("loanReferenceNumber"));
			report.setBrUserId((Long)row.get("bruserId"));
			report.setBrFullName((String)row.get("borrowername"));
			report.setLoanType((String)row.get("applicationtype"));
			report.setLoanAmount((Double)row.get("loanamount"));
			report.setLoanStatus((String)row.get("loanstatus"));
			report.setApplTypeId(Long.parseLong(row.get("applicationId")+""));
			
			String pdReportStatus = "";
			
			try {
				pdReportStatus = (String) jdbcTemplate.queryForObject(pdStatusSQL, new Object[] { report.getApplTypeId() }, String.class);
			} catch (EmptyResultDataAccessException e) {
				logger.info("Empty result : "+e.getMessage());
			}
			if((CommonUtils.Status.SANCTIONED.equals(report.getLoanStatus()) || CommonUtils.Status.DISBURSED.equals(report.getLoanStatus())) ) {
				if(CommonUtils.isObjectNullOrEmpty(pdReportStatus)) {
					pdReportStatus = "PD NOT DONE";
				}
			}
			
			if((CommonUtils.Status.OPEN.equals(report.getLoanStatus()) || 
							CommonUtils.Status.RESPONDED.equals(report.getLoanStatus()) ||
							CommonUtils.Status.REJECTED.equals(report.getLoanStatus()) || 
							CommonUtils.Status.NOTINTERESTED.equals(report.getLoanStatus()) || 
							CommonUtils.Status.SUBMIT_FORM.equals(report.getLoanStatus()) )) {
				
				if(CommonUtils.isObjectNullOrEmpty(pdReportStatus)) {
					pdReportStatus = "PD REPORT PENDING";
				}
				
			}
			report.setPdReportStatus(pdReportStatus);
			
			List<LenderDetails> listContact = jdbcTemplate.query(getLendersResponded, new RowMapper<LenderDetails>() {
				public LenderDetails mapRow(ResultSet result, int rowNum) throws SQLException {
					LenderDetails domain = new LenderDetails();
					BankMstr bankDetails = null;
					
					bankDetails = bankMstrRepository.findOne(result.getLong("userbankid"));
					if(bankDetails!=null) {
						domain.setLenderName(bankDetails.getName());
					}
					
					domain.setBrRespondedToLenderStatus(result.getString("status"));
	            	return domain;
				}
	        },report.getApplTypeId());
			
			String respondedLendersName="";
			String rejectedLender="";
			String selectedLenderByBr="";
			List<String> respondedLenders = new ArrayList<String>();
			
			for(LenderDetails lender : listContact) {
				if(lender.getBrRespondedToLenderStatus().equals("REJECTED")) {
					rejectedLender += lender.getLenderName();
				}
				else {
					selectedLenderByBr = lender.getLenderName();
				}
				respondedLenders.add(lender.getLenderName());
			}
			
			for(String s : respondedLenders) {
				respondedLendersName += s +", ";
			}
			
			report.setLendersResponded(respondedLenders);
			report.setLenderRespondedName(respondedLendersName);
			//report.setBrSelectedLender(Long.parseLong(row.get("br_select_lender")+""));
			report.setBrSelectedLenderName(selectedLenderByBr);
			report.setBrAppliedForLoanAmt((Double)row.get("br_applied_loan_amt"));
			
			List<DisbursmentDetails> disbursmentList = jdbcTemplate.query(disbursmentDetails, new RowMapper<DisbursmentDetails>() {
				public DisbursmentDetails mapRow(ResultSet result, int rowNum) throws SQLException {
	            	DisbursmentDetails disbursment = new DisbursmentDetails();
	            	disbursment.setDisbursmentDate(result.getDate("disbursment_date"));
	            	disbursment.setDisbursmentAmount(result.getDouble("disbursment_amount"));
	            	return disbursment;
				}
	        },report.getApplTypeId());
			
			report.setDisbursmentReport(disbursmentList);

			reports.add(report);
		}
		return reports;
	}
	
	public List<Reports> getReportData(Long userId, Long userType) {
		String sql="";
		
		if(userType.equals(new Long(3))) {//Channel Partner
			sql = " SELECT  a.id applicationId,a.`created_date`,a.`lead_reference_no` loanReferenceNumber, "+
					" CONCAT( u.`first_name`,' ',u.`last_name`) borrowername,a.`user_id` bruserId,a.`coapplicant_user_id`, "+
					" a.`loan_type_id`,b.name applicationtype, a.`status` loanstatus,a.`loan_amount` loanamount,a.`bank_name`, "+ 
					" a.`loan_amount` br_applied_loan_amt  "+
					" FROM applications a, mstr_base b, `user` u, pre_applications pa "+ 
					" WHERE a.`application_type_id`=b.id   "+
					" AND u.`id`=a.`user_id`   "+
					" AND pa.id=a.pre_application_id"+
					" AND pa.`channel_partner_id` = ?   "+
					" ORDER BY a.`created_date`  ";
		}
		else if(userType.equals(new Long(1))) {
			sql = " SELECT  lm.user_Id,a.id applicationId,a.`created_date`,a.`lead_reference_no` loanReferenceNumber,  "+
					" CONCAT( u.`first_name`,' ',u.`last_name`) borrowername,a.`user_id` bruserId,a.`coapplicant_user_id`,   "+
					" a.`loan_type_id`,b.name applicationtype, a.`status` loanstatus,a.`loan_amount` loanamount,a.`bank_name`,  "+
					" a.`loan_amount` br_applied_loan_amt    "+
					" FROM applications a, mstr_base b, `user` u, `lender_borrower_connection` lb , `lender_product_mapping` lm "+
					" WHERE a.`application_type_id`=b.id     "+
					" AND u.`id`=a.`user_id`   "+
					" AND lb.lender_application_mapping_id=lm.id "+
					" AND lb.application_id=a.id  "+
					" AND lm.user_Id = ?     "+
					" ORDER BY a.`created_date` ";
		}
		
		
		String getLendersResponded = " SELECT CONCAT(u.`first_name`,' ',u.`last_name`)lenders_responded_name, lbc.status status "+
				" FROM lender_borrower_connection lbc, `user` u "+
				" WHERE lbc.created_by=u.`id` "+
				" AND application_id=? ";
		
		String disbursmentDetails = " SELECT disbursment_date,disbursment_amount FROM `loan_transaction` WHERE appl_id=? ";
		
		List<Reports> reports = new ArrayList<Reports>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, userId);

		for (Map<String, Object> row : rows) {
			Reports report = new Reports();
			
			report.setLoanReferenceNumber((String)row.get("loanReferenceNumber"));
			report.setBrUserId((Long)row.get("bruserId"));
			report.setBrFullName((String)row.get("borrowername"));
			report.setLoanType((String)row.get("applicationtype"));
			report.setLoanAmount((Double)row.get("loanamount"));
			report.setLoanStatus((String)row.get("loanstatus"));
			report.setApplTypeId(Long.parseLong(row.get("applicationId")+""));
			
			List<LenderDetails> listContact = jdbcTemplate.query(getLendersResponded, new RowMapper<LenderDetails>() {
				public LenderDetails mapRow(ResultSet result, int rowNum) throws SQLException {
					LenderDetails domain = new LenderDetails();
					domain.setLenderName(result.getString("lenders_responded_name"));
					domain.setBrRespondedToLenderStatus(result.getString("status"));
	            	return domain;
				}
	        },report.getApplTypeId());
			
			String respondedLendersName="";
			String rejectedLender="";
			String selectedLenderByBr="";
			List<String> respondedLenders = new ArrayList<String>();
			
			for(LenderDetails lender : listContact) {
				if(lender.getBrRespondedToLenderStatus().equals("REJECTED")) {
					rejectedLender += lender.getLenderName();
				}
				else {
					selectedLenderByBr = lender.getLenderName();
				}
				respondedLenders.add(lender.getLenderName());
			}
			
			for(String s : respondedLenders) {
				respondedLendersName += s +", ";
			}
			
			report.setLendersResponded(respondedLenders);
			report.setLenderRespondedName(respondedLendersName);
			//report.setBrSelectedLender(Long.parseLong(row.get("br_select_lender")+""));
			report.setBrSelectedLenderName(selectedLenderByBr);
			report.setBrAppliedForLoanAmt((Double)row.get("br_applied_loan_amt"));
			
			List<DisbursmentDetails> disbursmentList = jdbcTemplate.query(disbursmentDetails, new RowMapper<DisbursmentDetails>() {
				public DisbursmentDetails mapRow(ResultSet result, int rowNum) throws SQLException {
	            	DisbursmentDetails disbursment = new DisbursmentDetails();
	            	disbursment.setDisbursmentDate(result.getDate("disbursment_date"));
	            	disbursment.setDisbursmentAmount(result.getDouble("disbursment_amount"));
	            	return disbursment;
				}
	        },report.getApplTypeId());
			
			report.setDisbursmentReport(disbursmentList);

			reports.add(report);
		}
		return reports;
	}
}
package com.lams.api.utils;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.lams.model.utils.Enums.Sequence;

@Repository
public class SequenceNumberGenerator {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public String generate(String type, Long userId) throws Exception {
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withFunctionName("nextval");
		SqlParameterSource paramMap = new MapSqlParameterSource().addValue("seq_name", "sq_payout_version");
		Object sum = new Object();
		try{
			sum = call.executeFunction(Object.class, paramMap);
		}
		catch(Exception e ){
			e.printStackTrace();
			sum = executeStoredProcedure();
		}

		return "VF-" + type.toUpperCase() + String.valueOf(userId) + sum;
	}
	
	public Object executeStoredProcedure() {
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate);
		call.withProcedureName("sp_nextval");
        SqlParameterSource in = new MapSqlParameterSource().addValue("seq_name", "sq_payout_version");
        Map<String,Object> out = call.execute(in);
        
        return out.get("cur_val");
    }
	
	public String generateSequence(Sequence seq, String type, Long userId) throws Exception {
		SimpleJdbcCall call = new SimpleJdbcCall(jdbcTemplate).withFunctionName("nextval");
		SqlParameterSource paramMap = new MapSqlParameterSource().addValue("seq_name", seq.getValue());
		Object sum = new Object();
		try{
			sum = call.executeFunction(Object.class, paramMap);
		}
		catch(Exception e ){
			e.printStackTrace();
			sum = executeStoredProcedure();
		}

		return "VF-" + type.toUpperCase() + String.valueOf(userId) + sum;
	}
}

enum SequenceName{
	SQ_PAYOUT_VERSION("sq_payout_version");
	
	private String seqName;
	
	SequenceName(String sqName){
		this.seqName = sqName;
	}

	public String getSeqName() {
		return seqName;
	}

	public void setSeqName(String seqName) {
		this.seqName = seqName;
	}
}

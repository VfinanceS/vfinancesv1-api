package com.lams.api.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.env.Environment;

import com.lams.model.bo.report.Reports;

public class VFExcelWriterHelper {
	
	public byte[] generateAdminReport(List<Reports> adminRepo, Environment environment) throws IOException, InvalidFormatException {
		String[] columns = { 
				"Borrower", 
				"Loan Reference Number", 
				"Loan Type", 
				"Loan Amount",
				"Loan Status",
				"PD Report Status",
				"Lender Responded",
				"Borrower Select Lender",
				"Borrower Applied Loan For",
				"Disbursment Date",
				"Disbursment Amount" };
		
		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

		/*
		 * CreationHelper helps us create instances of various things like DataFormat,
		 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
		 */
		CreationHelper createHelper = workbook.getCreationHelper();

		// Create a Sheet
		Sheet sheet = workbook.createSheet("AdminReports");

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 14);
		headerFont.setColor(IndexedColors.RED.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create cells
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

		// Create Other rows and cells with employees data
		int rowNum = 1;
		for (Reports reportData : adminRepo) {
			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue(reportData.getBrFullName());
			row.createCell(1).setCellValue(reportData.getLoanReferenceNumber());
			row.createCell(2).setCellValue(reportData.getLoanType()!=null?reportData.getLoanType():"");
			row.createCell(3).setCellValue(reportData.getLoanAmount()!=null?reportData.getLoanAmount():0);
			row.createCell(4).setCellValue(reportData.getLoanStatus()!=null?reportData.getLoanStatus():"");
			row.createCell(5).setCellValue(reportData.getPdReportStatus()!=null?reportData.getPdReportStatus():"");
			row.createCell(6).setCellValue(reportData.getLenderRespondedName());
			row.createCell(7).setCellValue(reportData.getBrSelectedLenderName());
			row.createCell(8).setCellValue(reportData.getBrAppliedForLoanAmt()!=null?reportData.getBrAppliedForLoanAmt():0);
			
			if(reportData.getDisbursmentReport()!=null && reportData.getDisbursmentReport().size()>0) {
				if(reportData.getDisbursmentReport().size() == 1) {
					continue;
				}
				if(reportData.getDisbursmentReport().size() == 1) {
					row.createCell(9).setCellValue(reportData.getDisbursmentReport().get(0).getDisbursmentDate());
					row.createCell(10).setCellValue(reportData.getDisbursmentReport().get(0).getDisbursmentAmount());
				}
				else if(reportData.getDisbursmentReport().size() > 1) {
					int rowIndexCnt=0;
					do {
						Row anotherRow = sheet.createRow(rowNum++);
						
						anotherRow.createCell(0).setCellValue(" ");
						anotherRow.createCell(1).setCellValue(" ");
						anotherRow.createCell(2).setCellValue(" ");
						anotherRow.createCell(3).setCellValue(" ");
						anotherRow.createCell(4).setCellValue(" ");
						anotherRow.createCell(5).setCellValue(" ");
						anotherRow.createCell(6).setCellValue(" ");
						anotherRow.createCell(7).setCellValue(" ");
						anotherRow.createCell(8).setCellValue(" ");
						
						DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
				        String strDate = "";
				        if(reportData.getDisbursmentReport().get(rowIndexCnt).getDisbursmentDate()!=null) {
				        	strDate = dateFormat.format(reportData.getDisbursmentReport().get(rowIndexCnt).getDisbursmentDate()!=null?reportData.getDisbursmentReport().get(rowIndexCnt).getDisbursmentDate():" ");	
				        }
				        
						anotherRow.createCell(9).setCellValue(strDate);
						anotherRow.createCell(10).setCellValue(reportData.getDisbursmentReport().get(rowIndexCnt).getDisbursmentAmount());
						
						rowIndexCnt++;
					}while(rowIndexCnt < reportData.getDisbursmentReport().size());
				}
			}
			
		}

		// Resize all columns to fit the content size
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		//documentUploadService.upload(storeFileName, getByteContent(workbook), documentRequest);
		// Write the output to a file
		/*FileOutputStream fileOut = new FileOutputStream(storeFileName);
		workbook.write(fileOut);
		String storedFilePath = environment.getRequiredProperty(SERVER_PATH) + "image/" + storeFileName;
		DocumentRequest documentRequest = null;
		//documentUploadService.upload("", fileOut, documentRequest);
		fileOut.close();*/

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		workbook.write(baos);
		
		// Closing the workbook
		workbook.close();
		
		return baos.toByteArray();
	}
}

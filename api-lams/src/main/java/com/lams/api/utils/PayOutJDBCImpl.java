package com.lams.api.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.lams.model.bo.ChannelPartnerCommisionBO;


@Repository
public class PayOutJDBCImpl {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public int deletePayOutStructureEntry(Long applicationTypeId) {
		String query = "delete from payout_structure where application_type_id = ? and version is null";
		Object[] params = {applicationTypeId};
		int[] types = {Types.BIGINT};
		
		return jdbcTemplate.update(query, params, types);
	}
	
	public int deletePayOutByVersion(String versionId) {
		String query = "delete from payout_structure where version = ?";
		Object[] params = {versionId};
		int[] types = {Types.VARCHAR};
		
		return jdbcTemplate.update(query, params, types);
	}
	
	public int deletePayOutMappings(Long userId) {
		String query = "delete from user_payout_mapping where user_id = ?";
		Object[] params = {userId};
		int[] types = {Types.BIGINT};
		
		return jdbcTemplate.update(query, params, types);
	}
	
	public List<ChannelPartnerCommisionBO> getAllChannelPartnerWorkList(Long chPartnerId) {
		StringBuffer sql = new StringBuffer();
		
		sql.append(" SELECT CONCAT(YEAR(disbursment_date),'-',MONTHNAME(disbursment_date)) AS disbursment_date,appl_type_id,appl_id,mb.name AS applname, ");
		sql.append(" SUM(lt.disbursment_amount) disbursment_amount,COUNT(1) as COUNT   ");
		sql.append(" FROM `user` u,loan_transaction lt, mstr_base mb, applications a, pre_applications pa  ");
		sql.append(" WHERE u.id=lt.br_user_id ");
		sql.append(" AND mb.id=lt.`appl_type_id`");
		sql.append(" AND a.id=lt.`appl_id`");
		sql.append(" AND pa.id = a.pre_application_id ");
		sql.append(" AND pa.channel_partner_id=? ");
		sql.append(" AND a.`lead_reference_no` LIKE 'VSCP%'" );
		sql.append(" AND pa.status = 'ACCEPTED'" );
		sql.append(" AND disbursment_amount > 0 ");
		sql.append(" GROUP BY lt.`appl_type_id`, YEAR(disbursment_date), MONTH(disbursment_date) ");
		sql.append(" ORDER BY disbursment_date DESC ");
		
		List<ChannelPartnerCommisionBO> loanTransactions = jdbcTemplate.query(sql.toString(), new RowMapper<ChannelPartnerCommisionBO>() {
			 
            public ChannelPartnerCommisionBO mapRow(ResultSet result, int rowNum) throws SQLException {
            	ChannelPartnerCommisionBO cpCommision = new ChannelPartnerCommisionBO();
            	cpCommision.setDisbursmentMonthYear(result.getString("disbursment_date"));
            	cpCommision.setApplicationName(result.getString("applname"));
            	cpCommision.setApplTypeId(result.getLong("appl_type_id"));
            	cpCommision.setAppId(result.getLong("appl_id"));
            	cpCommision.setTotalDisbursmentAmount(result.getDouble("disbursment_amount"));
            	cpCommision.setTotalCount(result.getLong("COUNT"));
                 
                return cpCommision;
            }
        },chPartnerId);
		
		Set<Long> applTypeIds = new HashSet<Long>();
		for(ChannelPartnerCommisionBO bo : loanTransactions) {
			applTypeIds.add(bo.getApplTypeId());
		}
		
		return loanTransactions;
	}
}
package com.lams.api.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java.sql.Types;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.lams.api.domain.master.AddressMstr;
import com.lams.api.domain.master.CityMstr;
import com.lams.api.repository.master.CityMstrRepository;
import com.lams.model.bo.AddressBO;


@Repository
public class AddressJDBCImpl {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private CityMstrRepository cityMstrRepository;

	public List<AddressMstr> getAdressList(String sql, Object[] o) {

		List<AddressMstr> customers = new ArrayList<AddressMstr>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, o);

		for (Map<String, Object> row : rows) {
			AddressMstr addrMstr = new AddressMstr();
			
			addrMstr.setId((Long) row.get("id"));
			addrMstr.setPremisesAndBuildingName((String) row.get("building_name"));
			addrMstr.setStreetName((String) row.get("street_name"));
			addrMstr.setLandMark((String) row.get("land_mark"));
			addrMstr.setPincode((String) row.get("pincode"));
			CityMstr ctyMstr = cityMstrRepository.findOne(Long.parseLong(row.get("city_id").toString()));
			addrMstr.setCity(ctyMstr);
			addrMstr.setAddType((Integer) row.get("add_type"));

			customers.add(addrMstr);
		}

		return customers;
	}

	public void updateAddress(AddressBO permanentAdd, Long id, int permanent) {
		
		String updateSql = "UPDATE address_mstr SET "
				+ "street_name = ?,"
				+ "land_mark=?,"
				+ "pincode=?,"
				+ "building_name=?,"
				+ "city_id=?,"
				+ "add_type=? "
				+ "WHERE id = ?";
		Object[] params = { permanentAdd.getStreetName(),
				permanentAdd.getLandMark(),
				permanentAdd.getPincode(),
				permanentAdd.getPremisesAndBuildingName(),
				permanentAdd.getCity().getId(),
				permanentAdd.getAddType(), 
				permanentAdd.getId()};
		int[] types = {Types.VARCHAR,
				Types.VARCHAR,
				Types.VARCHAR,
				Types.VARCHAR, 
				Types.BIGINT, 
				Types.BIGINT, 
				Types.BIGINT};

		jdbcTemplate.update(updateSql, params, types);
	}
}
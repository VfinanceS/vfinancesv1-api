package com.lams.api.domain.payment;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "payout_structure")
public class PayOutStructure  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "application_type_id")
	private Long applicationTypeId;
	
	@Column(name = "disbursment_from_amt")
	private Double disbursmentFromAmt;
	
	@Column(name = "disbursment_to_amt")
	private Double disbursmentToAmt;
	
	@Column(name = "payout")
	private Double payOutInPerc;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "created_by")
	private Long createdBy;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	@Column(name = "modified_by")
	private Double modifiedBy;
	
	@Column(name = "is_active")
	private Boolean isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplicationTypeId() {
		return applicationTypeId;
	}

	public void setApplicationTypeId(Long applicationTypeId) {
		this.applicationTypeId = applicationTypeId;
	}

	public Double getDisbursmentFromAmt() {
		return disbursmentFromAmt;
	}

	public void setDisbursmentFromAmt(Double disbursmentFromAmt) {
		this.disbursmentFromAmt = disbursmentFromAmt;
	}

	public Double getDisbursmentToAmt() {
		return disbursmentToAmt;
	}

	public void setDisbursmentToAmt(Double disbursmentToAmt) {
		this.disbursmentToAmt = disbursmentToAmt;
	}

	public Double getPayOutInPerc() {
		return payOutInPerc;
	}

	public void setPayOutInPerc(Double payOutInPerc) {
		this.payOutInPerc = payOutInPerc;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Double getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Double modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}

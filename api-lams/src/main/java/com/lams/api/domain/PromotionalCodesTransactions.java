package com.lams.api.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "promo_code_transactions")
public class PromotionalCodesTransactions implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "application_id")
	private Long applicatinId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "created_by")
	private Long createdBy;
	
	@OneToOne
	@JoinColumn(name="promo_code_id")
    private PromotionalCodes promotionalCodes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplicatinId() {
		return applicatinId;
	}

	public void setApplicatinId(Long applicatinId) {
		this.applicatinId = applicatinId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public PromotionalCodes getPromotionalCodes() {
		return promotionalCodes;
	}

	public void setPromotionalCodes(PromotionalCodes promotionalCodes) {
		this.promotionalCodes = promotionalCodes;
	}
	
}

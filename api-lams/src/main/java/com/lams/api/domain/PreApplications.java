package com.lams.api.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.lams.api.domain.master.ApplicationTypeMstr;
import com.lams.api.domain.master.Auditor;
import com.lams.api.domain.master.LoanTypeMstr;

@Entity
@Table(name = "pre_applications")
@Inheritance(strategy = InheritanceType.JOINED)
public class PreApplications extends Auditor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "application_type_id")
	private ApplicationTypeMstr applicationTypeId;

	@ManyToOne
	@JoinColumn(name = "loan_type_id")
	private LoanTypeMstr loanTypeId;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "loan_amount")
	private Double loanAmount;

	@Column(name = "status")
	private String status;

	@Column(name = "channel_partner_id")
	private Long channelPartnerId;
	
	@OneToMany(mappedBy = "preApplicationId", fetch=FetchType.EAGER)
	private List<Applications> applicationId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ApplicationTypeMstr getApplicationTypeId() {
		return applicationTypeId;
	}

	public void setApplicationTypeId(ApplicationTypeMstr applicationTypeId) {
		this.applicationTypeId = applicationTypeId;
	}

	public LoanTypeMstr getLoanTypeId() {
		return loanTypeId;
	}

	public void setLoanTypeId(LoanTypeMstr loanTypeId) {
		this.loanTypeId = loanTypeId;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Long channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public List<Applications> getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(List<Applications> applicationId) {
		this.applicationId = applicationId;
	}
	
}

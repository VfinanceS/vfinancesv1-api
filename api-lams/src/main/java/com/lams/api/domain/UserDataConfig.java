package com.lams.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.lams.api.domain.master.Auditor;

@Entity
@Table(name = "user_data_configuration")
public class UserDataConfig extends Auditor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "agency_type_id")
	private Long agencyTypeId;
	
	@Column(name = "profile_image")
	private Boolean profileImage;
	
	@Column(name = "name")
	private Boolean name;

	@Column(name = "email")
	private Boolean email;

	@Column(name = "mobile")
	private Boolean mobile;

	@Column(name = "first_name")
	private Boolean firstName;

	@Column(name = "last_name")
	private Boolean lastName;

	@Column(name = "middle_name")
	private Boolean middleName;

	@Column(name = "communication_add")
	private Boolean communicationAdd;

	@Column(name = "permanent_add")
	private Boolean permanentAdd;

	@Column(name = "birth_date")
	private Boolean birthDate;

	private Boolean gender;

	@Column(name = "salutation")
	private Boolean salutation;

	@Column(name = "bank")
	private Boolean bank;

	@Column(name = "pan_card")
	private Boolean panCard;

	@Column(name = "aadhar_card_no")
	private Boolean aadharCardNo;

	@Column(name = "edu_qualification")
	private Boolean eduQualification;

	@Column(name = "contact_number")
	private Boolean contactNumber;

	@Column(name = "employment_type")
	private Boolean employmentType;

	@Column(name = "employer_name")
	private Boolean employerName;

	@Column(name = "employment_address")
	private Boolean employmentAddress;

	@Column(name = "gross_monthly_income")
	private Boolean grossMonthlyIncome;

	@Column(name = "total_work_experience")
	private Boolean totalWorkExperience;

	@Column(name = "entity_name")
	private Boolean entityName;

	@Column(name = "contact_person_name")
	private Boolean contactPersonName;

	@Column(name = "gst_number")
	private Boolean gstNumber;

	@Column(name = "business_type_id")
	private Boolean businessTypeMstr;

	@Column(name = "channel_partner_id")
	private Boolean channelPartnerId;

	@Column(name = "about_me")
	private Boolean aboutMe;
	
	@Column(name = "entity_type")
	private Boolean entityType;

	@Column(name = "self_employed_type")
	private Boolean selfEmployedType;
	
	@Column(name = "coApplicant_details")
	private Boolean coApplicantDetails;
	
	@Column(name = "br_documents")
	private Boolean brDocuments;
	
	@Column(name = "responded_documents")
	private Boolean respondedDocuments;
	
	@Column(name = "coApplicant_documents")
	private Boolean coApplicantDocuments;
	
	@Column(name = "existing_loan")
	private Boolean existingLoan;
	
	@Column(name = "closed_loan")
	private Boolean closedLoan;

	public UserDataConfig() {
		super();
	}

	public UserDataConfig(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAgencyTypeId() {
		return agencyTypeId;
	}

	public void setAgencyTypeId(Long agencyTypeId) {
		this.agencyTypeId = agencyTypeId;
	}

	public Boolean getName() {
		return name;
	}

	public void setName(Boolean name) {
		this.name = name;
	}

	public Boolean getEmail() {
		return email;
	}

	public void setEmail(Boolean email) {
		this.email = email;
	}

	public Boolean getMobile() {
		return mobile;
	}

	public void setMobile(Boolean mobile) {
		this.mobile = mobile;
	}

	public Boolean getFirstName() {
		return firstName;
	}

	public void setFirstName(Boolean firstName) {
		this.firstName = firstName;
	}

	public Boolean getLastName() {
		return lastName;
	}

	public void setLastName(Boolean lastName) {
		this.lastName = lastName;
	}

	public Boolean getMiddleName() {
		return middleName;
	}

	public void setMiddleName(Boolean middleName) {
		this.middleName = middleName;
	}

	public Boolean getCommunicationAdd() {
		return communicationAdd;
	}

	public void setCommunicationAdd(Boolean communicationAdd) {
		this.communicationAdd = communicationAdd;
	}

	public Boolean getPermanentAdd() {
		return permanentAdd;
	}

	public void setPermanentAdd(Boolean permanentAdd) {
		this.permanentAdd = permanentAdd;
	}

	public Boolean getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Boolean birthDate) {
		this.birthDate = birthDate;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Boolean getSalutation() {
		return salutation;
	}

	public void setSalutation(Boolean salutation) {
		this.salutation = salutation;
	}

	public Boolean getBank() {
		return bank;
	}

	public void setBank(Boolean bank) {
		this.bank = bank;
	}

	public Boolean getPanCard() {
		return panCard;
	}

	public void setPanCard(Boolean panCard) {
		this.panCard = panCard;
	}

	public Boolean getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(Boolean aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public Boolean getEduQualification() {
		return eduQualification;
	}

	public void setEduQualification(Boolean eduQualification) {
		this.eduQualification = eduQualification;
	}

	public Boolean getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Boolean contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Boolean getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(Boolean employmentType) {
		this.employmentType = employmentType;
	}

	public Boolean getEmployerName() {
		return employerName;
	}

	public void setEmployerName(Boolean employerName) {
		this.employerName = employerName;
	}

	public Boolean getEmploymentAddress() {
		return employmentAddress;
	}

	public void setEmploymentAddress(Boolean employmentAddress) {
		this.employmentAddress = employmentAddress;
	}

	public Boolean getGrossMonthlyIncome() {
		return grossMonthlyIncome;
	}

	public void setGrossMonthlyIncome(Boolean grossMonthlyIncome) {
		this.grossMonthlyIncome = grossMonthlyIncome;
	}

	public Boolean getTotalWorkExperience() {
		return totalWorkExperience;
	}

	public void setTotalWorkExperience(Boolean totalWorkExperience) {
		this.totalWorkExperience = totalWorkExperience;
	}

	public Boolean getEntityName() {
		return entityName;
	}

	public void setEntityName(Boolean entityName) {
		this.entityName = entityName;
	}

	public Boolean getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(Boolean contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public Boolean getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(Boolean gstNumber) {
		this.gstNumber = gstNumber;
	}

	public Boolean getBusinessTypeMstr() {
		return businessTypeMstr;
	}

	public void setBusinessTypeMstr(Boolean businessTypeMstr) {
		this.businessTypeMstr = businessTypeMstr;
	}

	public Boolean getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Boolean channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public Boolean getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(Boolean aboutMe) {
		this.aboutMe = aboutMe;
	}

	public Boolean getEntityType() {
		return entityType;
	}

	public void setEntityType(Boolean entityType) {
		this.entityType = entityType;
	}

	public Boolean getSelfEmployedType() {
		return selfEmployedType;
	}

	public void setSelfEmployedType(Boolean selfEmployedType) {
		this.selfEmployedType = selfEmployedType;
	}

	public Boolean getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(Boolean profileImage) {
		this.profileImage = profileImage;
	}

	public Boolean getCoApplicantDetails() {
		return coApplicantDetails;
	}

	public void setCoApplicantDetails(Boolean coApplicantDetails) {
		this.coApplicantDetails = coApplicantDetails;
	}

	public Boolean getBrDocuments() {
		return brDocuments;
	}

	public void setBrDocuments(Boolean brDocuments) {
		this.brDocuments = brDocuments;
	}

	public Boolean getRespondedDocuments() {
		return respondedDocuments;
	}

	public void setRespondedDocuments(Boolean respondedDocuments) {
		this.respondedDocuments = respondedDocuments;
	}

	public Boolean getCoApplicantDocuments() {
		return coApplicantDocuments;
	}

	public void setCoApplicantDocuments(Boolean coApplicantDocuments) {
		this.coApplicantDocuments = coApplicantDocuments;
	}

	public Boolean getExistingLoan() {
		return existingLoan;
	}

	public void setExistingLoan(Boolean existingLoan) {
		this.existingLoan = existingLoan;
	}

	public Boolean getClosedLoan() {
		return closedLoan;
	}

	public void setClosedLoan(Boolean closedLoan) {
		this.closedLoan = closedLoan;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}

package com.lams.api.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "loan_transaction")
public class LoanTransaction implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "appl_id")
	private Long applId;
	
	@Column(name = "appl_type_id")
	private Long appTypeId;
	
	@Column(name = "br_user_id")
	public Long brUserId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "disbursment_date")
	private Date disbursmentDate;
	
	@Column(name = "disbursment_amount")
	private Double disbursmentAmount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "created_by")
	public Long createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "modified_by")
	public Long modifiedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplId() {
		return applId;
	}

	public void setApplId(Long applId) {
		this.applId = applId;
	}

	public Long getBrUserId() {
		return brUserId;
	}

	public void setBrUserId(Long brUserId) {
		this.brUserId = brUserId;
	}

	public Date getDisbursmentDate() {
		return disbursmentDate;
	}

	public void setDisbursmentDate(Date disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}

	public Long getAppTypeId() {
		return appTypeId;
	}

	public void setAppTypeId(Long appTypeId) {
		this.appTypeId = appTypeId;
	}
}

package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.lams.api.domain.LoanTransaction;

public interface LoanTransactionRepository  extends JpaRepository<LoanTransaction, Long>{

	public List<LoanTransaction> findByApplIdAndBrUserId(Long applId, Long brUserId);
	
}

package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.payment.UserPayOutMapping;

public interface UserPayOutMappingRepository  extends JpaRepository<UserPayOutMapping, Long>{

	public List<UserPayOutMapping> findById(Long id);
	
	public UserPayOutMapping findByUserIdAndApplTypeIdAndIsActive(Long userId, Long applTypeId, Boolean flag);
	
	public List<UserPayOutMapping> findByUserIdAndIsActiveAndApplTypeId(Long userId, Boolean flag, Long applTypeId);
	
	public UserPayOutMapping findByUserIdAndIsActive(Long userId, Boolean flag);
	
}

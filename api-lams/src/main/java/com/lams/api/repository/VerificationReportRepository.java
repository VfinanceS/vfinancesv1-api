package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.lams.api.domain.VerificationReports;

public interface VerificationReportRepository  extends JpaRepository<VerificationReports, Long>{

	public VerificationReports findByApplicationId(Long applicationId);
	
	public VerificationReports findByIdAndIsActive(Long id, Boolean isActive);

	@Query(value="select vrp from VerificationReports vrp where vrp.createdBy = :createdBy and vrp.isActive = :isActive ")
	public List<VerificationReports> findByCreatedBy(@Param("createdBy")Long createdBy, @Param("isActive")Boolean isActive);
	
	@Modifying
	@Query(value = "update VerificationReports app set app.status = :status where app.id =:appId")
	public int inActiveByUserId(@Param("appId")Long appId, @Param("status")String status);

}

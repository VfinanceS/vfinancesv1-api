package com.lams.api.repository.master;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.master.BankDetails;

public interface BankDetailsRepository extends JpaRepository<BankDetails, Long> {

	public BankDetails findByUserId(Long userId);

	public BankDetails findById(Long id);

}

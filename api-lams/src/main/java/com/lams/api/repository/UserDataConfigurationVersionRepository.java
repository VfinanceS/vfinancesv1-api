package com.lams.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.UserDataConfigVersions;

public interface UserDataConfigurationVersionRepository extends JpaRepository<UserDataConfigVersions, Long>{
	
	public UserDataConfigVersions findByUserIdAndIsActive(Long userId, Boolean isActive);
	
	public UserDataConfigVersions findByUserIdAndConfgVersionAndIsActive(Long userId, String version, Boolean isActive);
	
	public UserDataConfigVersions findByConfgVersion(String version);
	
}

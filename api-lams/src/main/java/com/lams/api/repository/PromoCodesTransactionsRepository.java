package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.PromotionalCodes;
import com.lams.api.domain.PromotionalCodesTransactions;

public interface PromoCodesTransactionsRepository  extends JpaRepository<PromotionalCodesTransactions, Long>{

	public List<PromotionalCodesTransactions> findById(Long id);
	
	public PromotionalCodesTransactions findByPromotionalCodes(PromotionalCodes promoCode);
	
}

package com.lams.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.UserDataConfig;

public interface UserDataConfigurationRepository extends JpaRepository<UserDataConfig, Long>{
	
	public UserDataConfig findByAgencyTypeIdAndIsActive(Long userType, Boolean isActive);
	
	public UserDataConfig findByAgencyTypeIdAndIsActiveAndVersionIsNull(Long userType, Boolean isActive);
	
	public UserDataConfig findByAgencyTypeId(Long userType);
	
	public UserDataConfig findByVersion(String version);
}

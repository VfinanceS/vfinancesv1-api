package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.lams.api.domain.PreApplications;

public interface PreApplicationsRepository  extends JpaRepository<PreApplications, Long>{

	public List<PreApplications> findByUserIdAndIsActiveAndChannelPartnerId(Long userId, Boolean isActive, Long channelPartnerId);
	
	public PreApplications findByIdAndIsActive(Long id, Boolean isActive);
	
	public List<PreApplications> findByUserIdAndIsActiveAndStatus(Long userId, Boolean isActive, String status);
	
	public List<PreApplications> findByUserIdAndChannelPartnerIdAndIsActive(Long userId,Long channelPartnerId, Boolean isActive);
	
	public List<PreApplications> findByUserIdAndChannelPartnerId(Long userId,Long channelPartnerId);
	
	public List<PreApplications> findByChannelPartnerIdAndIsActive(Long channelPartner, Boolean isActive);
	
	public List<PreApplications> findByChannelPartnerId(Long channelPartner);
	
	@Query(value = "SELECT lead_reference_no FROM lams.pre_applications where application_type_id =:applicationType order by id desc limit 1",nativeQuery = true)
	public String getLastLeadReferenceNo(@Param("applicationType") Long applicationType);
	
	@Modifying
	@Query(value = "update PreApplications app set app.isActive = false,app.status=:status where app.userId =:userId and app.channelPartnerId=:channelPartnerId")
	public int inActiveByApplicationUserIdAndChannelPartnerId(@Param("userId")Long userId, @Param("channelPartnerId")Long channelPartnerId, @Param("status")String status);
	
	@Modifying
	@Query(value = "update PreApplications app set app.isActive = :isActive,app.status=:status where app.applicationTypeId.id=:appTypeId and app.userId =:userId and app.status = 'OPEN'")
	public int inActiveByApplicationUserId(@Param("userId")Long userId, @Param("status")String status, @Param("appTypeId")Long appTypeId, @Param("isActive")Boolean isActive );
	
	@Modifying
	@Query(value = "update PreApplications app set app.isActive = :isActive,app.status=:status where app.id=:appId and app.userId =:userId and app.channelPartnerId=:channelPartnerId")
	public int inActiveByAppIdApplicationUserIdAndChannelPartnerId(@Param("status")String status, @Param("appId")Long appId,@Param("userId")Long userId, @Param("channelPartnerId")Long channelPartnerId, @Param("isActive")Boolean isActive );

	@Query(value = "SELECT lead_reference_no FROM lams.pre_applications where application_type_id =:applicationType and lead_reference_no like %:cpCode% order by id desc limit 1",nativeQuery = true)
	public String getLastLeadReferenceNoForCP(@Param("applicationType") Long applicationType,@Param("cpCode")String cpCode);
	
	@Query(value = "SELECT distinct channel_partner_id FROM lams.pre_applications where user_id =:userId and status <> 'REJECTED' and is_active=true",nativeQuery = true)
	public List<Long> getDistinctChannelPartnerIdByUserIdAndIsActive(@Param("userId") Long userId);
}

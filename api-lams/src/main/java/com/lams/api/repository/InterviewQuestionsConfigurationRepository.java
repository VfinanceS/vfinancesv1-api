package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.InterviewQuestions;

public interface InterviewQuestionsConfigurationRepository extends JpaRepository<InterviewQuestions, Long>{
	
	public InterviewQuestions findByIdAndIsActive(Long id, Boolean isActive);
	
	public List<InterviewQuestions> findByAgencyTypeAndIsActive(Long agencyType, Boolean isActive);
	
	public List<InterviewQuestions> findByAgencyType(Long agencyType);
}

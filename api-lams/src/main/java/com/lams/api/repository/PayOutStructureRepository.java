package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.payment.PayOutStructure;

public interface PayOutStructureRepository  extends JpaRepository<PayOutStructure, Long>{

	public List<PayOutStructure> findById(Long id);
	
	public List<PayOutStructure> findByApplicationTypeId(Long applicationTypeId);
	
	public List<PayOutStructure> findByApplicationTypeIdAndVersionIsNull(Long applicationTypeId);
	
	public List<PayOutStructure> findByVersion(String versionId);
	
	public List<PayOutStructure> findByVersionAndApplicationTypeId(String versionId, Long applicationTypeId);
	
}

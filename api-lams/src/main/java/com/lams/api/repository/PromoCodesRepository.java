package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.PromotionalCodes;

public interface PromoCodesRepository  extends JpaRepository<PromotionalCodes, Long>{

	public List<PromotionalCodes> findById(Long id);
	
	public PromotionalCodes findByPromoCode(String couponCode);
	
	public PromotionalCodes findByPromoCodeAndApplicationTypeId(String couponCode, Long applicationTypeId);
	
}

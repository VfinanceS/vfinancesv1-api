package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.lams.api.domain.UserCoapplicant;

public interface CoApplicantsRepository  extends JpaRepository<UserCoapplicant, Long>{

	public List<UserCoapplicant> findByIdAndParentUserId(Long id, int parentUserId);
	
	@Query(value = "SELECT * FROM lams.user_coapplicant where parent_user_id =:id ",nativeQuery = true)
	public List<UserCoapplicant> getAllCoApplicants(@Param("id") Long id);
}

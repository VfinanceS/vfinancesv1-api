package com.lams.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lams.api.domain.InterviewQuestionsAnswers;

public interface InterviewQuestionsAnswerConfigurationRepository extends JpaRepository<InterviewQuestionsAnswers, Long>{
	
	public InterviewQuestionsAnswers findByIdAndIsActive(Long id, Boolean isActive);
	
	public List<InterviewQuestionsAnswers> findByAgencyTypeAndIsActive(Long agencyType, Boolean isActive);
	
	public List<InterviewQuestionsAnswers> findByAgencyTypeAndApplicationIdAndIsActive(Long agencyType, Long applicationId, Boolean isActive);
	
	public List<InterviewQuestionsAnswers> findByApplicationIdAndIsActive(Long applicationId, Boolean isActive);
	
	public InterviewQuestionsAnswers findByQuestionIdAndAgencyTypeAndApplicationIdAndIsActive(Long questionId, Long agencyType, Long applicationId, Boolean isActive);
}

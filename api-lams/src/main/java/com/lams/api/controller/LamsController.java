package com.lams.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.CoApplicantsService;
import com.lams.api.service.LenderApplicationMappingService;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.LenderApplicationMappingBO;
import com.lams.model.bo.UserCoApplicantBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.Enums;

@RestController
//@RequestMapping(value = "/lams")
public class LamsController {


	@Autowired
	private CoApplicantsService coApplicantsService;

	public static final Logger logger = Logger.getLogger(LamsController.class);
	
	@RequestMapping(value = "/getCoApplicantst", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getCoApplicantst(HttpServletRequest httpServletRequest) {
		logger.info("Enter in getCoApplicants");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		Long userType = (Long) httpServletRequest.getAttribute(CommonUtils.USER_TYPE);
		try {
			if (Enums.UserType.BORROWER.getId() == userType) {
				List<UserCoApplicantBO> userCoapplicants = coApplicantsService.getAllCoApplicants(userId);
				
				LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", userCoapplicants);
				
				return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
			} else if (Enums.UserType.LENDER.getId() == userType) {
				//List<LenderApplicationMappingBO> list = applicationMappingService.getApplicationTypeByUserIdAndIsActive(userId, true);
				return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Successfully get data", null), HttpStatus.OK);
			} else {
				return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Invalid User"),HttpStatus.OK);
			}
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

package com.lams.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.ReportStructureService;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.report.Reports;
import com.lams.model.utils.CommonUtils;

@RestController
@RequestMapping(value = "/report")
public class ReportController {

	@Autowired
	private ReportStructureService reportStructureService;

	public static final Logger logger = Logger.getLogger(ReportController.class);

	@RequestMapping(value = "/getAdminReport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAdminReport(HttpServletRequest httpServletRequest) {
		logger.info("Enter in application list");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		try {
			List<Reports> applicationsBO = reportStructureService.getAdminReport(userId);
			
			LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", applicationsBO);
			
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}

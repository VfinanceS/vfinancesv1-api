package com.lams.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.ApplicationsService;
import com.lams.api.service.PayOutStructureService;
import com.lams.model.bo.ApplicationTypeWithPayOutBO;
import com.lams.model.bo.ChannelPartnerCommisionBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.utils.CommonUtils;

@RestController
@RequestMapping(value = "/payout")
public class PayOutController {

	@Autowired
	private ApplicationsService applicationsService;

	@Autowired
	private PayOutStructureService payOutStructureService;

	public static final Logger logger = Logger.getLogger(PayOutController.class);

	@RequestMapping(value = "/getAllPayouts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAll(HttpServletRequest httpServletRequest) {
		logger.info("Enter in application list");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		try {
			List<ApplicationTypeWithPayOutBO> applicationsBO = payOutStructureService.getReportingOfPayouts();
			
			LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", applicationsBO);
			
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/getPaymentDetailsForUser/{chnlprtnrId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getPaymentDetailsForUser(HttpServletRequest httpServletRequest, @PathVariable("chnlprtnrId") Long chnlprtnrId) {
		logger.info("Enter in application by id");
		try {
			Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
			List<ApplicationTypeWithPayOutBO> applicationsBO = payOutStructureService.getReportingOfPayoutsForUser(chnlprtnrId);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get data", applicationsBO), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application by id ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getPaymentDetailsForUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getPaymentDetailsForChnlPartner(HttpServletRequest httpServletRequest) {
		logger.info("Enter in application by id");
		try {
			Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
			List<ApplicationTypeWithPayOutBO> applicationsBO = payOutStructureService.getReportingOfPayoutsForUser(userId);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get data", applicationsBO), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application by id ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/savePayoutsFor", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> save(@RequestBody ApplicationTypeWithPayOutBO applicationTypeWithPayBO,
			HttpServletRequest httpServletRequest) {
		logger.info("Enter in save application");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);

		try {
			if (!CommonUtils.isObjectNullOrEmpty(userId)) {
				applicationTypeWithPayBO.setUserId(userId);
				payOutStructureService.save(applicationTypeWithPayBO);
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.OK.value(), "Successfully save data", "OK"), HttpStatus.OK);
			} else {
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Request"), HttpStatus.OK);
			}

		} catch (Exception e) {
			logger.info("Throw Exception while save application by id ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/savePayoutsForChnlPrtnr/{chnlPartnerId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> savePayoutForChnlPrtnr(@RequestBody ApplicationTypeWithPayOutBO applicationTypeWithPayBO,
			HttpServletRequest httpServletRequest, @PathVariable("chnlPartnerId") Long chnlprtnrId) {
		logger.info("Enter in save application");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);

		try {
			if (!CommonUtils.isObjectNullOrEmpty(userId)) {
				applicationTypeWithPayBO.setUserId(userId);
				payOutStructureService.savePayoutForChnlPartner(applicationTypeWithPayBO, chnlprtnrId);
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.OK.value(), "Successfully save data", "OK"), HttpStatus.OK);
			} else {
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Request"), HttpStatus.OK);
			}

		} catch (Exception e) {
			logger.info("Throw Exception while save application by id ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getCpCommisionCalculationList/{chnlprtnrId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getCpCommisionCalculationList(HttpServletRequest httpServletRequest, @PathVariable("chnlprtnrId") Long chnlprtnrId) {
		logger.info("Enter in application by id");
		try {
			Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
			List<ChannelPartnerCommisionBO> chCommisionCalculations = payOutStructureService.getChannelPartnerCommisionYearMonthWise(chnlprtnrId);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get data", chCommisionCalculations), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application by id ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

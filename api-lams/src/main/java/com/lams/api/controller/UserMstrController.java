package com.lams.api.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.CoApplicantsService;
import com.lams.api.service.InterviewQuestionsAnswerConfigurationService;
import com.lams.api.service.InterviewQuestionsConfigurationService;
import com.lams.api.service.UserDataConfigurationService;
import com.lams.api.service.UserMstrService;
import com.lams.model.bo.ApplicationsBO;
import com.lams.model.bo.EmailBo;
import com.lams.model.bo.InterviewQuestionsAnswersConfigWrapperBO;
import com.lams.model.bo.InterviewQuestionsConfigBO;
import com.lams.model.bo.InterviewQuestionsConfigWrapperBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.PreApplicationsBO;
import com.lams.model.bo.UserBO;
import com.lams.model.bo.UserCoApplicantBO;
import com.lams.model.bo.UserDataConfigBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.CommonUtils.Status;
import com.lams.model.utils.Enums.OTPType;

//@CrossOrigin(origins = {"http://localhost:*","http://localhost:*"})
@RestController
public class UserMstrController {

	public final static Logger logger = Logger.getLogger(UserMstrController.class.getName());

	@Autowired
	private UserMstrService userMstrService;
	
	@Autowired
	private UserDataConfigurationService userDataConfigurationService;
	
	@Autowired
	private InterviewQuestionsConfigurationService interviewQuestionsConfigurationService;
	
	@Autowired
	private InterviewQuestionsAnswerConfigurationService interviewQuestionsAnswerConfigurationService;
	
	@Autowired
	private CoApplicantsService coApplicantsService;

	@RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> registration(@RequestBody UserBO userBO) {
		logger.info("Enter in registration process");
		if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getMobile())) {
			logger.info("Mobile is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Password is Null Or Empty"), HttpStatus.OK);
		}

		try {
			return new ResponseEntity<LamsResponse>(userMstrService.registration(userBO, null), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while registrion ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getUsersByType/{userType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getUsersByType(@PathVariable(value = "userType") Long userType) {
		logger.info("Enter in get users by user type");
		try {
			List<UserBO> userList = userMstrService.getUsersByUserType(userType);
			logger.info("Successfully get users data by type id ------------>" + userType);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", userList), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getUsersConfigByType/{userType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getUsersConfigByType(@PathVariable(value = "userType") Long userType) {
		logger.info("Enter in get users by user type");
		try {
			UserDataConfigBO userList = userDataConfigurationService.getUserById(userType);
			logger.info("Successfully get users data by type id ------------>" + userType);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", userList), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getUsersConfig/{userType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getUsersConfig(@PathVariable(value = "userType") Long userType) {
		logger.info("Enter in get users configuration by user type");
		try {
			UserDataConfigBO userDataConfigBO = userDataConfigurationService.getUserByTypeAndIsActive(userType);
			logger.info("Successfully get users data by type id ------------>" + userType);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get User Configuration", userDataConfigBO), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users configuration by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/get_user_details", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getLoggedInUserDetails(HttpServletRequest request) {
		logger.info("=======>> Enter in get users by user type without userid url param");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		System.out.println("*************"+userId);
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while getting Loggedin User Details ------------>{}"+userId, userId);
				return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.UNAUTHORIZED.value(), "Failed", null), HttpStatus.UNAUTHORIZED);
			}
			UserBO userData = userMstrService.getUserById(userId);
			logger.log(Level.INFO, "Successfully get Logged in User Details ------------>{}" + userId, userData.toString());

			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Success", userData), HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while Getting user Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG), HttpStatus.OK);
		}
	}
	
	//
	@RequestMapping(value = "/get_coApplicantuser_details/{id}/{brCoApplicantParentUserId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getCoplicantUserDetails(
			HttpServletRequest request, 
			@PathVariable(value = "id") Long coApplicantId, 
			@PathVariable(value = "brCoApplicantParentUserId") Long coApplicantParentUserId) {
		logger.info("getCoplicantUserDetails() started coApplicantId:-"+coApplicantId+", coApplicantParentUserId:-"+coApplicantParentUserId);
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		System.out.println("*************"+userId);
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while getting Loggedin User Details ------------>{}"+userId, userId);
				return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.UNAUTHORIZED.value(), "Failed", null), HttpStatus.UNAUTHORIZED);
			}
			UserCoApplicantBO coApplicantUserData = coApplicantsService.getCoApplicantProfile(coApplicantId, coApplicantParentUserId);
			logger.log(Level.INFO, "Successfully get CoApplicant User Details ------------>{}" + coApplicantId, coApplicantUserData.toString());

			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Success", coApplicantUserData), HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while Getting user Details based on UserId===>{}", coApplicantId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG), HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get_user_details/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getLoggedInUserDetailsById(@PathVariable("userId")Long userId,HttpServletRequest request) {
		logger.info("=======>> Enter in get users by user type with userid");
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while getting User Details By IDs------------>{}",
						userId);
			}
			UserBO userData = userMstrService.getUserById(userId);
			logger.log(Level.INFO, "Successfully get Logged in User Details ------------>{}", userData.toString());

			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Success", userData),
					HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while Getting user Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/update_user_details", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateUserDetails(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in get users by user type");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while Updating  User Details ------------>{}",
						userId);
			}
			userBO.setId(userId);
			return new ResponseEntity<LamsResponse>(userMstrService.updateUserDetails(userBO), HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while updating User Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getPdUserPermissionMatrix", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getPdUserPermissionMatrix(HttpServletRequest request) {
		logger.info("Enter in get users by user type");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		UserBO user = userMstrService.getUserById(userId);
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while Updating  User Details ------------>{}",
						userId);
			}
			return new ResponseEntity<LamsResponse>(userDataConfigurationService.getUserByAgencyType(user.getUserType()), HttpStatus.OK);
		} catch (Exception e) {
			//logger.log(Level.SEVERE, "Error while updating User Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/update_user_coapplicant_details/{coApplicantUserId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateUserCoApplicantDetails(@RequestBody UserBO userBO,@PathVariable("coApplicantUserId")String coApplicantUserId, HttpServletRequest request) {
		logger.info("updateUserCoApplicantDetails() started");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while Updating  User Details ------------>{}", userId);
			}
			userBO.setId(userId);
			return new ResponseEntity<LamsResponse>(userMstrService.updateUserCoApplicantDetails(userBO, new Long(coApplicantUserId)), HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while updating User coapplicant Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/createCoApplicantForUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> createCoApplicantForUser(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in creation of co-applicant");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		try {
			if (CommonUtils.isObjectNullOrEmpty(userId)) {
				logger.log(Level.WARNING, "UserId must not be null while Updating  User Details ------------>{}", userId);
			}
			userBO.setId(userId);
			userMstrService.createCoApplicantProfile(userBO);
			List<UserCoApplicantBO> userCoapplicants = coApplicantsService.getAllCoApplicants(userId);
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Successfully get data", userCoapplicants), HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while updating User Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG), HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/update_lender_details", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateLenderDetails(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in update lender details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getMobile())) {
			logger.info("Mobile is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Password is Null Or Empty"), HttpStatus.OK);
		}

		try {
			return new ResponseEntity<LamsResponse>(userMstrService.registration(userBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/update_pd_agency_details", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updatePDAgencyDetails(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in update pd agency details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getMobile())) {
			logger.info("Mobile is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Password is Null Or Empty"), HttpStatus.OK);
		}

		try {
			return new ResponseEntity<LamsResponse>(userMstrService.registration(userBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/update_pd_agency_configuration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updatePDAgencyConfiguration(@RequestBody UserDataConfigBO userBO, HttpServletRequest request) {
		logger.info("Enter in update pd agency details process");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);

		try {
			userBO.setIsActive(true);
			return new ResponseEntity<LamsResponse>(userDataConfigurationService.updateUserConfiguration(userBO, userId, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/update_fci_agency_details", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateFCIAgencyDetails(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in update fci agency details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getMobile())) {
			logger.info("Mobile is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Password is Null Or Empty"), HttpStatus.OK);
		}

		try {
			return new ResponseEntity<LamsResponse>(userMstrService.registration(userBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/update_admin_details", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateAdminDetails(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in update lender details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getMobile())) {
			logger.info("Mobile is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Null Or Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Password is Null Or Empty"), HttpStatus.OK);
		}

		try {
			return new ResponseEntity<LamsResponse>(userMstrService.adminRegistration(userBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/save_cp_borrower", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> saveCPBorrower(@RequestBody UserBO userBO,
			HttpServletRequest httpServletRequest) {
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userId)) {
			logger.info("User Id Null Or Empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "UnAuthorized! Please try again to ReLogin"),
					HttpStatus.OK);
		}
		
		try {
			return new ResponseEntity<LamsResponse>(userMstrService.addCpBorrowerOld(userBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while Saving Borrower From Channel Partnert----------------> For UserId===" + userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/confirmPreApplicationByBorrower", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> saveCPBorrowerLatest(@RequestBody PreApplicationsBO preApplicationsBO,Long id,
			HttpServletRequest httpServletRequest) {
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userId)) {
			logger.info("User Id Null Or Empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "UnAuthorized! Please try again to ReLogin"),
					HttpStatus.OK);
		}
		
		try {
			return new ResponseEntity<LamsResponse>(userMstrService.addCpBorrower(preApplicationsBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while Saving Borrower From Channel Partnert----------------> For UserId===" + userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get_cp_users/{userType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getCPBorrower(@PathVariable("userType")Long userType ,HttpServletRequest httpServletRequest) {
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userId)) {
			logger.info("User Id Null Or Empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "UnAuthorized! Please try again to ReLogin"),
					HttpStatus.OK);
		}
		
		try {
			LamsResponse response = userMstrService.getCpUsersByUserType(userId, userType);
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while Saving Borrower From Channel Partnert----------------> For UserId===" + userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get_cp_users/{userType}/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getCPBorrowerForAdmin(@PathVariable("userType")Long userType ,@PathVariable("userId")Long userId, HttpServletRequest httpServletRequest) {
		if (CommonUtils.isObjectNullOrEmpty(userId)) {
			logger.info("User Id Null Or Empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "UnAuthorized! Please try again to ReLogin"),
					HttpStatus.OK);
		}
		
		try {
			return new ResponseEntity<LamsResponse>(userMstrService.getCpUsersByUserType(userId, userType), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while Saving Borrower From Channel Partnert----------------> For UserId===" + userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	

	@RequestMapping(value = "/invite_lender", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> inviteLender(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in update lender details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email Must not be Empty"), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getMobile())) {
			logger.info("Mobile is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile Must not be Empty"), HttpStatus.OK);
		}

		try {
			userBO = userMstrService.inviteLender(userBO, userId);
			logger.log(Level.INFO, "Response After Invited to Lender===>{0}", userBO.toString());
			LamsResponse lamsResponse = new LamsResponse(HttpStatus.OK.value(), "Invitation successfully sent.", userBO);
			return new ResponseEntity<LamsResponse>(lamsResponse, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/verify_otp/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> verifyOTP(@RequestBody UserBO userBO, @PathVariable("type") String typeCode,
			HttpServletRequest request) {
		logger.info("Enter in update lender details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			userBO.setId(userId);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			logger.info("ID is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getOtp())) {
			logger.info("OTP is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "OTP Must not be Empty."), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(typeCode)) {
			logger.info("TYPE is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}

		OTPType otpType = OTPType.getType(typeCode);
		if (CommonUtils.isObjectNullOrEmpty(typeCode)) {
			logger.info("otpType Enum Object is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}
		try {
			return new ResponseEntity<LamsResponse>(userMstrService.verifyOTP(userBO, otpType), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/resend_otp/{type}/{templateName}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> resendOTP(@RequestBody UserBO userBO, @PathVariable("type") String typeCode,
			@PathVariable("templateName") String templateName, HttpServletRequest request) {
		logger.info("Enter in resendOTP");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			userBO.setId(userId);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			logger.info("UsesrId is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(templateName)) {
			logger.info("templateName is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(typeCode)) {
			logger.info("TYPE is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}

		OTPType otpType = OTPType.getType(typeCode);
		if (CommonUtils.isObjectNullOrEmpty(typeCode)) {
			logger.info("otpType Enum Object is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}
		try {
			return new ResponseEntity<LamsResponse>(userMstrService.resendOtp(userBO, otpType, templateName),
					HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/change_password", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> changePassword(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in resendOTP");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
		if (CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			userBO.setId(userId);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			logger.info("UsesrId is null or empty");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getTempPassword())) {
			logger.info("Temp (Current) password is NUll");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Please Enter Current Password."), HttpStatus.OK);
		}

		if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("New password is NUll");
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Please Enter New Password."), HttpStatus.OK);
		}
		try {
			return new ResponseEntity<LamsResponse>(userMstrService.changePassword(userBO), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/verify_email/{link}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> verifyEmail(@PathVariable("link") String link, HttpServletRequest request) {
		logger.info("Enter in verifyAccount");
		try {
			if (CommonUtils.isObjectNullOrEmpty(link)) {
				logger.info("Link is NUll");
				return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.BAD_REQUEST.value(),
						CommonUtils.INVALID_REQUEST + " Please try Again!"), HttpStatus.OK);
			}
			logger.log(Level.INFO, "Response After Invited to Lender===>{0}", new Object[] { link });
			return new ResponseEntity<LamsResponse>(userMstrService.verifyEmail(link), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while Verifying Email ---------------->{0}" + new Object[] { link });
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/send_link", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> sendLink(@RequestBody UserBO userBO, HttpServletRequest request) {
		logger.info("Enter in sendLink");
		try {
			if (CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
				logger.info("email is NUll");
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Please Enter Email."), HttpStatus.OK);
			}
			logger.log(Level.INFO, "Email for Forgot Password===>{0}", new Object[] { userBO.getEmail() });
			return new ResponseEntity<LamsResponse>(userMstrService.sendForgotPasswordLink(userBO.getEmail()), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while Sending Link on Forgot Password ---------------->{0}"
					+ new Object[] { userBO.getEmail()});
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/reset_password/{link}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> resetPassword(@RequestBody UserBO userBO,@PathVariable("link")String link, HttpServletRequest request) {
		logger.info("Enter in resetPassword");
		try {
			if (CommonUtils.isObjectNullOrEmpty(link)) {
				logger.info("Link is NUll");
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Email"), HttpStatus.OK);
			}
			
			if (CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
				logger.info("Password is NUll");
				return new ResponseEntity<LamsResponse>(
						new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Password must not be Empty."), HttpStatus.OK);
			}
			logger.log(Level.INFO, "Link for Reset Password===>{0}", new Object[] { link });
			return new ResponseEntity<LamsResponse>(userMstrService.resetPassword(userBO,link), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while updating Password on Link ---------------->{0}"
					+ new Object[] { link});
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get_user_details_by_id/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getUserDetailsById(@PathVariable Long userId, HttpServletRequest request) {
		Long loggedInUserId = (Long) request.getAttribute(CommonUtils.USER_ID);
		try {
			UserBO userData = userMstrService.getUserById(userId);
			UserBO loggedInUserData = userMstrService.getUserById(loggedInUserId);
			
			for (ApplicationsBO appBO : userData.getApplications()) {
				if(loggedInUserData!=null && (loggedInUserData.getUserType() == 1 || loggedInUserData.getUserType() == 0)) {
					
					switch(loggedInUserData.getUserType().intValue()) {
					
					case 0://Borrower
						if(appBO.getStatus().equals(Status.REJECTED)) {
							appBO.setLenderButtonActionName("REJECTED");
						}
						if(appBO.getStatus().equals(Status.ACCEPTED)) {
							appBO.setLenderButtonActionName("SUBMIT FORM");
						}
						else if(appBO.getStatus().equals(Status.SUBMIT_FORM)) {
							appBO.setLenderButtonActionName("SUBMIT SACTION FORM");
						}
						else if(appBO.getStatus().equals(Status.SANCTIONED)) {
							appBO.setLenderButtonActionName("SUBMIT DISBURSMENT FORM");
						}
						else if(appBO.getStatus().equals(Status.DISBURSED)) {
							appBO.setLenderButtonActionName("DONE");
						}
						break;
						
					case 1://Lender
						if(appBO.getStatus().equals(Status.REJECTED)) {
							appBO.setLenderButtonActionName("REJECTED");
						}
						if(appBO.getStatus().equals(Status.ACCEPTED)) {
							appBO.setLenderButtonActionName("SUBMIT FORM");
						}
						else if(appBO.getStatus().equals(Status.SUBMIT_FORM)) {
							appBO.setLenderButtonActionName("SUBMIT SACTION FORM");
						}
						else if(appBO.getStatus().equals(Status.SANCTIONED)) {
							appBO.setLenderButtonActionName("Done");
						}
						
						break;
					}
				}
			}
			
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Success", userData),
					HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while Getting user Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/get_br_coappl_user_details/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getBrCoapplicantUserDetailsById(@PathVariable Long userId, HttpServletRequest request) {
		try {
			UserCoApplicantBO userData = coApplicantsService.getCoApplicantProfile(userId);
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Success", userData), HttpStatus.OK);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while Getting user Details based on UserId===>{}", userId);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG), HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/sendMailRequestFromSite", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> sendMailRequestFromSite(@RequestBody EmailBo emailBO, HttpServletRequest request) {
		logger.info("In Sending mail from wesite");
		try {
			return new ResponseEntity<LamsResponse>(new LamsResponse(HttpStatus.OK.value(), "Success", 
					userMstrService.sendMailRequestFromSite(emailBO.getEmail(), emailBO.getName(), emailBO.getMsg(), emailBO.getContact())),
					HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/update_question", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateQuestion(@RequestBody InterviewQuestionsConfigBO questionBO, HttpServletRequest request) {
		logger.info("Enter in update pd agency details process");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);

		try {
			return new ResponseEntity<LamsResponse>(interviewQuestionsConfigurationService.updateQuestions(questionBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + questionBO);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getAllQuestionByAgencyType/{userType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAllQuestionByAgencyType(@PathVariable(value = "userType") Long userType) {
		logger.info("Enter in get users by user type");
		try {
			List<InterviewQuestionsConfigBO> questionMstrs = interviewQuestionsConfigurationService.getQuestionsByAgencyType(userType, true);
			logger.info("Successfully get users data by type id ------------>" + userType);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", questionMstrs), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/update_interview_questions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateInterviewQuestions(@RequestBody InterviewQuestionsConfigWrapperBO interviewQuestionsConfigBO, HttpServletRequest request) {
		logger.info("Enter in update pd agency details process");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);

		try {
			return new ResponseEntity<LamsResponse>(interviewQuestionsConfigurationService.saveAllQuestions(interviewQuestionsConfigBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + interviewQuestionsConfigBO);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/borrower_answers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> submitBorrowersAnswers(@RequestBody InterviewQuestionsAnswersConfigWrapperBO interviewQuestionsConfigBO, HttpServletRequest request) {
		logger.info("Enter in update pd agency details process");
		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);

		try {
			UserBO user = userMstrService.getUserById(userId);
			interviewQuestionsConfigBO.setAgencyType(user.getUserType());
			return new ResponseEntity<LamsResponse>(interviewQuestionsAnswerConfigurationService.saveAllQuestions(interviewQuestionsConfigBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + interviewQuestionsConfigBO);
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getAllQuestionAnsByAgencyType/{applId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAllQuestionAnsByAgencyType(@PathVariable(value = "applId") Long applId, HttpServletRequest request) {
		logger.info("Enter in get users by user type");
		try {
			Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
			UserBO user = userMstrService.getUserById(userId);
			
			InterviewQuestionsAnswersConfigWrapperBO questionMstrs = interviewQuestionsAnswerConfigurationService.getQuestionsAnsByAgencyType(user.getUserType(), applId);
			logger.info("Successfully get users data by type id ------------>" + user.getUserType());
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", questionMstrs), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getAllQuestionAns/{applId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAllQuestionAns(@PathVariable(value = "applId") Long applId, HttpServletRequest request) {
		logger.info("Enter in get users by user type");
		try {
			Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
			
			InterviewQuestionsAnswersConfigWrapperBO questionMstrs = interviewQuestionsAnswerConfigurationService.getQuestionsAnsByAgencyType(applId);
			logger.info("Successfully get users data by type id ------------>" );
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", questionMstrs), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getUsersConfigByUser/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getUsersConfigByUser(@PathVariable(value = "userId") Long userId) {
		logger.info("Enter in get users by user type");
		try {
			UserDataConfigBO userList = userDataConfigurationService.getConfigurationDetails(userId);
			logger.info("Successfully get users data by type id ------------>" + userId);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", userList), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/getAgencyUsersConfigByUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getUsersConfigByLoggedUser(HttpServletRequest request) {
		logger.info("Enter in get users by user type");
		try {
			Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);
			UserDataConfigBO userList = userDataConfigurationService.getConfigurationDetails(userId);
			logger.info("Successfully get users data by type id ------------>" + userId);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get userList", userList), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get users by user type");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
}

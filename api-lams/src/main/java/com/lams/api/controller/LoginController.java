package com.lams.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.lams.api.service.LoginService;
import com.lams.model.bo.LoginResponse;
import com.lams.model.bo.UserBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.Enums;


@RestController
//@CrossOrigin(origins = {"http://localhost:*","http://localhost:*"})
public class LoginController {

	public static final Logger logger = Logger.getLogger(LoginController.class);
	
	@Autowired
	private LoginService loginService;
    
	@RequestMapping(value="/loginadmin",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoginResponse> loginAdmin (@RequestBody UserBO userBO){
		logger.info("Enter in login service");
		if(CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email id null or empty");
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.BAD_REQUEST.value(), "Email can't be null or empty"),HttpStatus.OK);
		}
		if(CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password id null or empty");
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.BAD_REQUEST.value(), "Password can't be null or empty"),HttpStatus.OK);
		}
		try {
			LoginResponse loginResponse = loginService.loginAdmin(userBO);
			if(loginResponse.getStatus().equals(HttpStatus.OK.value())){
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				request.setAttribute(CommonUtils.USER_ID, userBO.getId());
				request.setAttribute(CommonUtils.USER_TYPE, userBO.getUserType());
				request.getSession().setAttribute(CommonUtils.USER_ID, userBO.getId());
			}
			return new ResponseEntity<LoginResponse>(loginResponse,HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while login user ---------------->"  +userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/login",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoginResponse> login (@RequestBody UserBO userBO){
		logger.info("Enter in login service");
		if(CommonUtils.isObjectNullOrEmpty(userBO.getEmail())) {
			logger.info("Email id null or empty");
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.BAD_REQUEST.value(), "Email can't be null or empty"),HttpStatus.OK);
		}
		if(CommonUtils.isObjectNullOrEmpty(userBO.getPassword())) {
			logger.info("Password id null or empty");
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.BAD_REQUEST.value(), "Password can't be null or empty"),HttpStatus.OK);
		}
		try {
			userBO.setUserType(new Long(Enums.UserType.BORROWER.getId()));
			LoginResponse loginResponse = loginService.login(userBO);
			if(loginResponse.getStatus().equals(HttpStatus.OK.value())){
				HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
				request.setAttribute(CommonUtils.USER_ID, userBO.getId());
				request.setAttribute(CommonUtils.USER_TYPE, userBO.getUserType());
				request.getSession().setAttribute(CommonUtils.USER_ID, userBO.getId());
			}
			return new ResponseEntity<LoginResponse>(loginResponse,HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while login user ---------------->"  +userBO.getEmail());
			e.printStackTrace();
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/logout",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LoginResponse> login (HttpServletRequest httpServletRequest){
		logger.info("Enter in logout service");
		String token = httpServletRequest.getHeader("token");
		if(CommonUtils.isObjectNullOrEmpty(token)) {
			logger.info("Token id null or empty");
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Requet"),HttpStatus.OK);
		}
		try {
			return new ResponseEntity<LoginResponse>(loginService.logout(token),HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while logout user ---------------->"  +token);
			e.printStackTrace();
			return new ResponseEntity<LoginResponse>(
					new LoginResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

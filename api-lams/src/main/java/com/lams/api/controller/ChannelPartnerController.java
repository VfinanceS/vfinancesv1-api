package com.lams.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.ReportStructureService;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.report.Reports;
import com.lams.model.utils.CommonUtils;

@RestController
@RequestMapping(value = "/channelpartner")
public class ChannelPartnerController {

	@Autowired
	private ReportStructureService reportStructureService;

	public static final Logger logger = Logger.getLogger(ChannelPartnerController.class);

	@RequestMapping(value = "/getAllPayouts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAllPayouts(HttpServletRequest httpServletRequest) {
		logger.info("Enter in application list");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		Long userType = (Long) httpServletRequest.getAttribute(CommonUtils.USER_TYPE);
		try {
			List<Reports> applicationsBO = reportStructureService.getGeneralReport(userId, userType);
			
			LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", applicationsBO);
			
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*@RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAll(@PathVariable("id") Long id) {
		logger.info("Enter in application by id");
		try {
			PayOutStructureBO applicationsBO = payOutStructureService.get(id);
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.OK.value(), "Successfully get data", applicationsBO), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application by id ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/


}

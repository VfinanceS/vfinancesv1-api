package com.lams.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.master.BankService;
import com.lams.model.bo.BankBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.utils.CommonUtils;

@RestController
@RequestMapping(value = "/bank")
public class BankController {

	@Autowired
	private BankService bankService;

	public static final Logger logger = Logger.getLogger(BankController.class);

	@RequestMapping(value = "/getAllBanks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAllBanks(HttpServletRequest httpServletRequest) {
		logger.info("Enter in application list");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		try {
			List<BankBO> banksBO = bankService.getBanks();
			
			LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", banksBO);
			
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/update_bank_details", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> updateBankDetails(@RequestBody BankBO bankBO, HttpServletRequest request) {
		logger.info("Enter in update lender details process");

		Long userId = (Long) request.getAttribute(CommonUtils.USER_ID);

		try {
			return new ResponseEntity<LamsResponse>(bankService.saveBankDetails(bankBO, userId), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while update lender details ---------------->" + bankBO.getCode());
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), CommonUtils.SOMETHING_WENT_WRONG),
					HttpStatus.OK);
		}
	}
	
}

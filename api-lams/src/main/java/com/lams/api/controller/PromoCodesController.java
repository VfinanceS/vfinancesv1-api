package com.lams.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lams.api.service.PromoCodesService;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.PromoCodeBO;
import com.lams.model.utils.CommonUtils;

@RestController
@RequestMapping(value = "/promovouchers")
public class PromoCodesController {

	@Autowired
	private PromoCodesService promoCodesService;

	
	public static final Logger logger = Logger.getLogger(PromoCodesController.class);

	@RequestMapping(value = "/getAllPromoCodes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> getAll(HttpServletRequest httpServletRequest) {
		logger.info("Enter in application list");
		try {
			List<PromoCodeBO> list = promoCodesService.getAllPromoCodes();
			
			LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", list);
			
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/generateFreshCoupons/{quantity}/{length}/{validDays}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> generateFreshCoupons(HttpServletRequest httpServletRequest,
			@PathVariable(value = "quantity") Long quantity,
			@PathVariable(value = "length") Long length,
			@PathVariable(value = "validDays") Long validDays) {
		logger.info("Enter in application list");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		try {

			List<PromoCodeBO> list = promoCodesService.generateNewFreeCoupons(length, quantity.intValue(), userId, validDays);
			
			LamsResponse response = new LamsResponse(HttpStatus.OK.value(), "Successfully get data", list);
			
			return new ResponseEntity<LamsResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/generateFreshCouponsManually", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LamsResponse> generateFreshCouponsManually(HttpServletRequest httpServletRequest,
			@RequestBody PromoCodeBO promoCodeBO/*,
			@PathVariable(value = "promoCode") String promoCode,
			@PathVariable(value = "promoCodeDesc") String promoCodeDesc,
			@PathVariable(value = "applicationTypeId") Long applicationTypeId,
			@PathVariable(value = "isActive") Boolean isActive*/) {
		logger.info("Enter in application list");
		Long userId = (Long) httpServletRequest.getAttribute(CommonUtils.USER_ID);
		try {
			
			return new ResponseEntity<LamsResponse>(promoCodesService.generateNewFreeCouponManually(promoCodeBO, userId), HttpStatus.OK);
			
		} catch (Exception e) {
			logger.info("Throw Exception while get application list ---------------->");
			e.printStackTrace();
			return new ResponseEntity<LamsResponse>(
					new LamsResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

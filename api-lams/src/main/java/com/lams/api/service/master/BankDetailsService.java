package com.lams.api.service.master;

import com.lams.model.bo.BankDetailsBO;

public interface BankDetailsService {

	public void saveBankDetails(BankDetailsBO bankDetailsBO,Long userId);
	
}

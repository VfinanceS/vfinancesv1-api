package com.lams.api.service.master;

import java.util.List;

import com.lams.model.bo.BankBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.master.MasterBaseBO;

public interface BankService {
	
	public List<MasterBaseBO> getBanksByMode(Integer mode);
	
	public List<BankBO> getBanks();
	
	public LamsResponse saveBankDetails(BankBO bo, Long createdBy);
}

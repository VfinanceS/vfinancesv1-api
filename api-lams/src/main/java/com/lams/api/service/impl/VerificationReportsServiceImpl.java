package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.VerificationReports;
import com.lams.api.repository.VerificationReportRepository;
import com.lams.api.service.VerificationReportService;
import com.lams.model.bo.VerificationReportsBO;

@Service
@Transactional
public class VerificationReportsServiceImpl implements VerificationReportService {

	public static final Logger logger = Logger.getLogger(VerificationReportsServiceImpl.class);

	@Autowired
	private VerificationReportRepository verificationReportRepository;

	/**
	 * SAVE AND UPDATE APPLICATION DATA
	 */
	@Override
	public Long save(VerificationReportsBO vrBO) {
		logger.info("Enter in Save Application Sevice Impl--------------type----> " + vrBO.getId());
		VerificationReports vr = verificationReportRepository.findByApplicationId(vrBO.getApplicationId());
		
		if(vr != null) {
			vr.setModifiedBy(vrBO.getModifiedBy());
			vr.setModifiedDate(new Date());
		}
		else {
			vr = new VerificationReports();
			vr.setCreatedBy(vrBO.getCreatedBy());
			vr.setCreatedDate(new Date());
		}
		
		vr.setApplicationId(vrBO.getApplicationId());
		vr.setStatus("PD REPORT UPDATED");
		vr.setIsActive(true);
		vr.setReportedDate(vrBO.getReportedDate());
		
		verificationReportRepository.save(vr);
		
		return null;
	}

	@Override
	public List<VerificationReportsBO> getAll(Long userId) {
		List<VerificationReportsBO> boList = new ArrayList<VerificationReportsBO>();
		List<VerificationReports> list = verificationReportRepository.findByCreatedBy(userId, true);
		
		for(VerificationReports vr : list) {
			VerificationReportsBO vrBO = new VerificationReportsBO();
			BeanUtils.copyProperties(vr, vrBO);
			boList.add(vrBO);
		}
		return boList;
	}
}

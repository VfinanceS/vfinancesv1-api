package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.Applications;
import com.lams.api.domain.LenderBorrowerConnection;
import com.lams.api.domain.User;
import com.lams.api.domain.UserCoapplicant;
import com.lams.api.domain.VerificationReports;
import com.lams.api.repository.ApplicationsRepository;
import com.lams.api.repository.CoApplicantsRepository;
import com.lams.api.repository.LenderApplicationMappingRepository;
import com.lams.api.repository.LenderBorrowerConnectionRepository;
import com.lams.api.repository.PreApplicationsRepository;
import com.lams.api.repository.UserMstrRepository;
import com.lams.api.repository.VerificationReportRepository;
import com.lams.api.service.ApplicationsService;
import com.lams.api.service.CoApplicantsService;
import com.lams.api.service.loan.BankGuaranteeLoanDetailsService;
import com.lams.api.service.loan.CCFacilitiesLoanDetailsService;
import com.lams.api.service.loan.CarLoanDetailsService;
import com.lams.api.service.loan.DropLineOdFacilitiesLoanDetailsService;
import com.lams.api.service.loan.EducationLoanDetailsService;
import com.lams.api.service.loan.GoldLoanDetailsService;
import com.lams.api.service.loan.HomeLoanDetailsService;
import com.lams.api.service.loan.LoanAgainstFDsDetailsService;
import com.lams.api.service.loan.LoanAgainstPropertyDetailsService;
import com.lams.api.service.loan.LoanAgainstSecuritiesLoanDetailsService;
import com.lams.api.service.loan.OthersLoanDetailsService;
import com.lams.api.service.loan.OverDraftFacilitiesLoanDetailsService;
import com.lams.api.service.loan.PersonalLoanDetailsService;
import com.lams.api.service.loan.PrivateEquityFinanceLoanDetailsService;
import com.lams.api.service.loan.ProjectFinanceLoanDetailsService;
import com.lams.api.service.loan.SecuredBusinessLoanDetailsService;
import com.lams.api.service.loan.TermLoanDetailsService;
import com.lams.api.service.loan.WorkingCapitalLoanDetailsService;
import com.lams.api.utils.MailAsynComponent;
import com.lams.model.bo.ApplicationRequestBO;
import com.lams.model.bo.ApplicationsBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.LenderBorrowerConnectionBO;
import com.lams.model.bo.UserBO;
import com.lams.model.loan.bo.BankGuaranteeLoanDetailsBO;
import com.lams.model.loan.bo.CCFacilitiesLoanDetailsBO;
import com.lams.model.loan.bo.CarLoanDetailsBO;
import com.lams.model.loan.bo.DropLineOdFacilitiesLoanDetailsBO;
import com.lams.model.loan.bo.EducationLoanDetailsBO;
import com.lams.model.loan.bo.GoldLoanDetailsBO;
import com.lams.model.loan.bo.HomeLoanDetailsBO;
import com.lams.model.loan.bo.LoanAgainstFDsDetailsBO;
import com.lams.model.loan.bo.LoanAgainstPropertyDetailsBO;
import com.lams.model.loan.bo.LoanAgainstSecuritiesLoanDetailsBO;
import com.lams.model.loan.bo.OthersLoanDetailsBO;
import com.lams.model.loan.bo.OverDraftFacilitiesLoanDetailsBO;
import com.lams.model.loan.bo.PersonalLoanDetailsBO;
import com.lams.model.loan.bo.PrivateEquityFinanceLoanDetailsBO;
import com.lams.model.loan.bo.ProjectFinanceLoanDetailsBO;
import com.lams.model.loan.bo.SecuredBusinessLoanDetailsBO;
import com.lams.model.loan.bo.TermLoanDetailsBO;
import com.lams.model.loan.bo.WorkingCapitalLoanDetailsBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.CommonUtils.ApplicationType;
import com.lams.model.utils.CommonUtils.Status;
import com.lams.model.utils.MultipleJSONObjectHelper;

@Service
@Transactional
public class ApplicationsServiceImpl implements ApplicationsService {

	public static final Logger logger = Logger.getLogger(ApplicationsServiceImpl.class);

	@Autowired
	private ApplicationsRepository applicationsRepository;

	@Autowired
	private UserMstrRepository userMstrRepository;

	@Autowired
	private BankGuaranteeLoanDetailsService bankGuaranteeLoanDetailsService;

	@Autowired
	private CarLoanDetailsService carLoanDetailsService;

	@Autowired
	private CCFacilitiesLoanDetailsService ccFacilitiesLoanDetailsService;

	@Autowired
	private DropLineOdFacilitiesLoanDetailsService dropLineOdFacilitiesLoanDetailsService;

	@Autowired
	private EducationLoanDetailsService educationLoanDetailsService;

	@Autowired
	private GoldLoanDetailsService goldLoanDetailsService;

	@Autowired
	private HomeLoanDetailsService homeLoanDetailsService;

	@Autowired
	private LoanAgainstFDsDetailsService loanAgainstFDsDetailsService;

	@Autowired
	private LoanAgainstPropertyDetailsService loanAgainstPropertyDetailsService;

	@Autowired
	private LoanAgainstSecuritiesLoanDetailsService loanAgainstSecuritiesLoanDetailsService;

	@Autowired
	private OthersLoanDetailsService othersLoanDetailsService;

	@Autowired
	private OverDraftFacilitiesLoanDetailsService overDraftFacilitiesLoanDetailsService;

	@Autowired
	private PersonalLoanDetailsService personalLoanDetailsService;

	@Autowired
	private PrivateEquityFinanceLoanDetailsService privateEquityFinanceLoanDetailsService;

	@Autowired
	private ProjectFinanceLoanDetailsService projectFinanceLoanDetailsService;

	@Autowired
	private SecuredBusinessLoanDetailsService securedBusinessLoanDetailsService;

	@Autowired
	private TermLoanDetailsService termLoanDetailsService;

	@Autowired
	private WorkingCapitalLoanDetailsService workingCapitalLoanDetailsService;

	@Autowired
	private LenderApplicationMappingRepository lenderApplicationMappingRepository;

	@Autowired
	private LenderBorrowerConnectionRepository lenderBorrowerConnectionRepository;
	
	@Autowired
	private MailAsynComponent mailAsynComponent;
	
	@Autowired
	private CoApplicantsRepository coApplicantsRepository;
	
	@Autowired
	private CoApplicantsService coApplicantsService;
	
	@Autowired
	private PreApplicationsRepository preApplicationsRepository;
	
	@Autowired
	private VerificationReportRepository verificationReportRepository;

	/*
	 * GET ALL APPLICATIONS BY USER ID AND IS ACTIVE TRUE
	 */
	@Override
	public List<ApplicationsBO> getAll(Long userId) {
		List<Applications> applicationsList = applicationsRepository.findByUserIdAndIsActive(userId, true);
		List<ApplicationsBO> applicationsBOList = new ArrayList<>(applicationsList.size());
		for (Applications applications : applicationsList) {
			ApplicationsBO abo = convertDomainToBO(applications);
			
			if(applications.getCoApplicantUserId()!=null) {
				UserCoapplicant uc = coApplicantsRepository.findOne(applications.getCoApplicantUserId());
				if(uc!=null) {
					abo.setCoApplicantName(uc.getFirstName()!=null?uc.getFirstName():""+" "+uc.getLastName()!=null?uc.getLastName():"");
					abo.setUserCoApplicantBO(coApplicantsService.convertDomainToBO(uc));
				}
			}
			applicationsBOList.add(abo);
		}
		return applicationsBOList;
	}
	
	/**
	 * Get All Application Of Borrower which is  Added by Channel Partner 
	 */

	@Override
	public List<ApplicationsBO> getAllByCP(Long userId, Long cpUserId) {
		List<Applications> applicationsList = applicationsRepository.getAllAppByUserIdAndCpId(userId, cpUserId);
		List<ApplicationsBO> applicationsBOList = new ArrayList<>(applicationsList.size());
		for (Applications applications : applicationsList) {
			applicationsBOList.add(convertDomainToBO(applications));
		}
		return applicationsBOList;
	}



	/**
	 * GET APPLICATION DETAILS BY APPLICATION ID
	 */
	@Override
	public ApplicationsBO get(Long id) {
		Applications applications = applicationsRepository.findByIdAndIsActive(id, true);
		return convertDomainToBO(applications);
	}

	/**
	 * SAVE AND UPDATE APPLICATION DATA
	 */
	@Override
	public Long save(ApplicationRequestBO applicationRequestBO) {
		logger.info("Enter in Save Application Sevice Impl--------------type----> "
				+ applicationRequestBO.getApplicationTypeId());
		
		Long userId = applicationRequestBO.getUserId();
		Long applicationId = null;
		try {
			switch (applicationRequestBO.getApplicationTypeId().intValue()) {

			case ApplicationType.HOME_LOAN:
				HomeLoanDetailsBO homeLoanDetailsBO = (HomeLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), HomeLoanDetailsBO.class);
				homeLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				homeLoanDetailsBO.setUserId(userId);
				homeLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(homeLoanDetailsBO.getCoApplicantId()!=null && homeLoanDetailsBO.getCoApplicantId()<=0) {
					homeLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				homeLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = homeLoanDetailsService.save(homeLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, homeLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
			case ApplicationType.LOAN_AGAINST_PROPERTY:
				LoanAgainstPropertyDetailsBO loanAgainstPropertyDetailsBO = (LoanAgainstPropertyDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								LoanAgainstPropertyDetailsBO.class);
				loanAgainstPropertyDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				loanAgainstPropertyDetailsBO.setUserId(userId);
				loanAgainstPropertyDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(loanAgainstPropertyDetailsBO.getCoApplicantId()!=null && loanAgainstPropertyDetailsBO.getCoApplicantId()<=0) {
					loanAgainstPropertyDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				loanAgainstPropertyDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = loanAgainstPropertyDetailsService.save(loanAgainstPropertyDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, loanAgainstPropertyDetailsBO.getIsLoanDetailsLock());
				return applicationId;

			case ApplicationType.SECURED_BUSINESS_LOAN:
				SecuredBusinessLoanDetailsBO securedBusinessLoanDetailsBO = (SecuredBusinessLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								SecuredBusinessLoanDetailsBO.class);
				securedBusinessLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				securedBusinessLoanDetailsBO.setUserId(userId);
				securedBusinessLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(securedBusinessLoanDetailsBO.getCoApplicantId()!=null && securedBusinessLoanDetailsBO.getCoApplicantId()<=0) {
					securedBusinessLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				securedBusinessLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = securedBusinessLoanDetailsService.save(securedBusinessLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, securedBusinessLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;

			case ApplicationType.WORKING_CAPITAL_LOAN:
				WorkingCapitalLoanDetailsBO workingCapitalLoanDetailsBO = (WorkingCapitalLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								WorkingCapitalLoanDetailsBO.class);
				workingCapitalLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				workingCapitalLoanDetailsBO.setUserId(userId);
				workingCapitalLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(workingCapitalLoanDetailsBO.getCoApplicantId()!=null && workingCapitalLoanDetailsBO.getCoApplicantId()<=0) {
					workingCapitalLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				workingCapitalLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = workingCapitalLoanDetailsService.save(workingCapitalLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, workingCapitalLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.EDUCATION_LOAN:
				EducationLoanDetailsBO educationLoanDetailsBO = (EducationLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), EducationLoanDetailsBO.class);
				educationLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				educationLoanDetailsBO.setUserId(userId);
				educationLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(educationLoanDetailsBO.getCoApplicantId()!=null && educationLoanDetailsBO.getCoApplicantId()<=0) {
					educationLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				educationLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = educationLoanDetailsService.save(educationLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, educationLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;

			case ApplicationType.CAR_LOAN:
				CarLoanDetailsBO carLoanDetailsBO = (CarLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), CarLoanDetailsBO.class);
				carLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				carLoanDetailsBO.setUserId(userId);
				carLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(carLoanDetailsBO.getCoApplicantId()!=null && carLoanDetailsBO.getCoApplicantId()<=0) {
					carLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				carLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = carLoanDetailsService.save(carLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, carLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;

			case ApplicationType.OVERDRAFT_FACILITIES_LOAN:
				OverDraftFacilitiesLoanDetailsBO overDraftFacilitiesLoanDetailsBO = (OverDraftFacilitiesLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								OverDraftFacilitiesLoanDetailsBO.class);
				overDraftFacilitiesLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				overDraftFacilitiesLoanDetailsBO.setUserId(userId);
				overDraftFacilitiesLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(overDraftFacilitiesLoanDetailsBO.getCoApplicantId()!=null && overDraftFacilitiesLoanDetailsBO.getCoApplicantId()<=0) {
					overDraftFacilitiesLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				overDraftFacilitiesLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = overDraftFacilitiesLoanDetailsService.save(overDraftFacilitiesLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, overDraftFacilitiesLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;

			case ApplicationType.DROPLINE_OVERDRAFT_FACILITIES_LOAN:
				DropLineOdFacilitiesLoanDetailsBO dropLineOdFacilitiesLoanDetailsBO = (DropLineOdFacilitiesLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								DropLineOdFacilitiesLoanDetailsBO.class);
				dropLineOdFacilitiesLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				dropLineOdFacilitiesLoanDetailsBO.setUserId(userId);
				dropLineOdFacilitiesLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(dropLineOdFacilitiesLoanDetailsBO.getCoApplicantId()!=null && dropLineOdFacilitiesLoanDetailsBO.getCoApplicantId()<=0) {
					dropLineOdFacilitiesLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				dropLineOdFacilitiesLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = dropLineOdFacilitiesLoanDetailsService.save(dropLineOdFacilitiesLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, dropLineOdFacilitiesLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.BANK_GUARANTEE_LOAN:
				BankGuaranteeLoanDetailsBO bankGuaranteeLoanDetailsBO = (BankGuaranteeLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								BankGuaranteeLoanDetailsBO.class);
				bankGuaranteeLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				bankGuaranteeLoanDetailsBO.setUserId(userId);
				bankGuaranteeLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(bankGuaranteeLoanDetailsBO.getCoApplicantId()!=null && bankGuaranteeLoanDetailsBO.getCoApplicantId()<=0) {
					bankGuaranteeLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				bankGuaranteeLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = bankGuaranteeLoanDetailsService.save(bankGuaranteeLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, bankGuaranteeLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.CC_FACILITIES_LOAN:
				CCFacilitiesLoanDetailsBO cCFacilitiesLoanDetailsBO = (CCFacilitiesLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								CCFacilitiesLoanDetailsBO.class);
				cCFacilitiesLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				cCFacilitiesLoanDetailsBO.setUserId(userId);
				cCFacilitiesLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(cCFacilitiesLoanDetailsBO.getCoApplicantId()!=null && cCFacilitiesLoanDetailsBO.getCoApplicantId()<=0) {
					cCFacilitiesLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				cCFacilitiesLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = ccFacilitiesLoanDetailsService.save(cCFacilitiesLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, cCFacilitiesLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.TERM_LOAN:
				TermLoanDetailsBO termLoanDetailsBO = (TermLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), TermLoanDetailsBO.class);
				termLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				termLoanDetailsBO.setUserId(userId);
				termLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(termLoanDetailsBO.getCoApplicantId()!=null && termLoanDetailsBO.getCoApplicantId()<=0) {
					termLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				termLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = termLoanDetailsService.save(termLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, termLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.LOAN_AGAINST_FDS:
				LoanAgainstFDsDetailsBO loanAgainstFDsDetailsBO = (LoanAgainstFDsDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), LoanAgainstFDsDetailsBO.class);
				loanAgainstFDsDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				loanAgainstFDsDetailsBO.setUserId(userId);
				loanAgainstFDsDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(loanAgainstFDsDetailsBO.getCoApplicantId()!=null && loanAgainstFDsDetailsBO.getCoApplicantId()<=0) {
					loanAgainstFDsDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				loanAgainstFDsDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = loanAgainstFDsDetailsService.save(loanAgainstFDsDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, loanAgainstFDsDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.LOAN_AGAINST_SECURITIS:
				LoanAgainstSecuritiesLoanDetailsBO loanAgainstSecuritiesLoanDetailsBO = (LoanAgainstSecuritiesLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								LoanAgainstSecuritiesLoanDetailsBO.class);
				loanAgainstSecuritiesLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				loanAgainstSecuritiesLoanDetailsBO.setUserId(userId);
				loanAgainstSecuritiesLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(loanAgainstSecuritiesLoanDetailsBO.getCoApplicantId()!=null && loanAgainstSecuritiesLoanDetailsBO.getCoApplicantId()<=0) {
					loanAgainstSecuritiesLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				loanAgainstSecuritiesLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = loanAgainstSecuritiesLoanDetailsService.save(loanAgainstSecuritiesLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, loanAgainstSecuritiesLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.PROJECT_FINANCE_LOAN:
				ProjectFinanceLoanDetailsBO projectFinanceLoanDetailsBO = (ProjectFinanceLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								ProjectFinanceLoanDetailsBO.class);
				projectFinanceLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				projectFinanceLoanDetailsBO.setUserId(userId);
				projectFinanceLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(projectFinanceLoanDetailsBO.getCoApplicantId()!=null && projectFinanceLoanDetailsBO.getCoApplicantId()<=0) {
					projectFinanceLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				projectFinanceLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = projectFinanceLoanDetailsService.save(projectFinanceLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, projectFinanceLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.PRIVATE_EQUITY_FINANCE_LOAN:
				PrivateEquityFinanceLoanDetailsBO privateEquityFinanceLoanDetailsBO = (PrivateEquityFinanceLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(),
								PrivateEquityFinanceLoanDetailsBO.class);
				privateEquityFinanceLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				privateEquityFinanceLoanDetailsBO.setUserId(userId);
				privateEquityFinanceLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(privateEquityFinanceLoanDetailsBO.getCoApplicantId()!=null && privateEquityFinanceLoanDetailsBO.getCoApplicantId()<=0) {
					privateEquityFinanceLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				privateEquityFinanceLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = privateEquityFinanceLoanDetailsService.save(privateEquityFinanceLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, privateEquityFinanceLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.GOLD_LOAN:
				GoldLoanDetailsBO goldLoanDetailsBO = (GoldLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), GoldLoanDetailsBO.class);
				goldLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				goldLoanDetailsBO.setUserId(userId);
				goldLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(goldLoanDetailsBO.getCoApplicantId()!=null && goldLoanDetailsBO.getCoApplicantId()<=0) {
					goldLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				goldLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = goldLoanDetailsService.save(goldLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, goldLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.OTHER_LOAN:
				OthersLoanDetailsBO othersLoanDetailsBO = (OthersLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), OthersLoanDetailsBO.class);
				othersLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				othersLoanDetailsBO.setUserId(userId);
				othersLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(othersLoanDetailsBO.getCoApplicantId()!=null && othersLoanDetailsBO.getCoApplicantId()<=0) {
					othersLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				othersLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = othersLoanDetailsService.save(othersLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, othersLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			case ApplicationType.PERSONAL_LOAN:
				PersonalLoanDetailsBO personalLoanDetailsBO = (PersonalLoanDetailsBO) MultipleJSONObjectHelper
						.getObjectFromString(applicationRequestBO.getData().toString(), PersonalLoanDetailsBO.class);
				personalLoanDetailsBO.setApplicationTypeId(applicationRequestBO.getApplicationTypeId());
				personalLoanDetailsBO.setUserId(userId);
				personalLoanDetailsBO.setStatus(CommonUtils.Status.OPEN);
				if(personalLoanDetailsBO.getCoApplicantId()!=null && personalLoanDetailsBO.getCoApplicantId()<=0) {
					personalLoanDetailsBO.setCoApplicantId(applicationRequestBO.getCoApplicantId());
				}
				personalLoanDetailsBO.setLoanCategoryId(applicationRequestBO.getLoanCategoryId());
				
				applicationId = personalLoanDetailsService.save(personalLoanDetailsBO);
				sentMailWhenBRLockDetails(applicationId, userId, personalLoanDetailsBO.getIsLoanDetailsLock());
				return applicationId;
				
			default:
				return null;
			}
		} catch (Exception e) {
			logger.info("Error while save application form data");
			e.printStackTrace();
		}
		return null;
	}

	
	private void sentMailWhenBRLockDetails(Long applicationId, Long userId, Boolean isLockDetails) {
		try {
			if(!CommonUtils.isObjectNullOrEmpty(isLockDetails) && isLockDetails) {
				mailAsynComponent.sendMailToBorrowerWhenBRSubmitForm(applicationId, userId);
				mailAsynComponent.sendMailToLenderWhenBRSubmitForm(applicationId, userId);	
			}
		} catch (Exception e) {
			logger.info("THROW EXCEPTION WHILE SENDING MAIL");
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * CONVERT APPLICATIONS DOMAIN OBJ TO BO OBJ
	 * 
	 * @param applications
	 * @return
	 */
	public ApplicationsBO convertDomainToBO(Applications applications) {
		ApplicationsBO applicationsBO = new ApplicationsBO();
		if (CommonUtils.isObjectNullOrEmpty(applications)) {
			return applicationsBO;
		}
		BeanUtils.copyProperties(applications, applicationsBO);
		if (!CommonUtils.isObjectNullOrEmpty(applications.getApplicationTypeId())) {
			applicationsBO.setApplicationTypeId(applications.getApplicationTypeId().getId());
			applicationsBO.setApplicationTypeName(applications.getApplicationTypeId().getName());
			applicationsBO.setApplicationTypeCode(applications.getApplicationTypeId().getCode());
		}
		if (!CommonUtils.isObjectNullOrEmpty(applications.getLoanTypeId())) {
			applicationsBO.setLoanTypeId(applications.getLoanTypeId().getId());
			applicationsBO.setLoanTypeName(applications.getLoanTypeId().getName());
		}
		
		if(applications.getCoApplicantUserId()!=null) {
			applicationsBO.setCoApplicantId(applications.getCoApplicantUserId());
		}
		return applicationsBO;
	}
	
	@Override
	public List<ApplicationsBO> convertListOfDomainToListOfBO(List<Applications> applications){
		List<ApplicationsBO> bos = new ArrayList<ApplicationsBO>();
		
		for(Applications application : applications) {
			bos.add(convertDomainToBO(application));
		}
		
		return bos;
	}

	public LamsResponse getLoanApplicationDetails(Long id, Long applicationTypeId, Long userId) {

		LamsResponse lamsResponse = new LamsResponse();
		Long employmentType = userMstrRepository.getEmpTypeById(userId);
		switch (applicationTypeId.intValue()) {

		case ApplicationType.HOME_LOAN:
			HomeLoanDetailsBO homeLoanDetailsBO = homeLoanDetailsService.get(id);
			homeLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(homeLoanDetailsBO);
			break;

		case ApplicationType.LOAN_AGAINST_PROPERTY:
			LoanAgainstPropertyDetailsBO loanAgainstPropertyDetailsBO = loanAgainstPropertyDetailsService.get(id);
			loanAgainstPropertyDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(loanAgainstPropertyDetailsBO);
			break;

		case ApplicationType.SECURED_BUSINESS_LOAN:
			SecuredBusinessLoanDetailsBO securedBusinessLoanDetailsBO = securedBusinessLoanDetailsService.get(id);
			securedBusinessLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(securedBusinessLoanDetailsBO);
			break;

		case ApplicationType.WORKING_CAPITAL_LOAN:
			WorkingCapitalLoanDetailsBO workingCapitalLoanDetailsBO = workingCapitalLoanDetailsService.get(id);
			workingCapitalLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(workingCapitalLoanDetailsBO);
			break;

		case ApplicationType.EDUCATION_LOAN:
			EducationLoanDetailsBO educationLoanDetailsBO = educationLoanDetailsService.get(id);
			educationLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(educationLoanDetailsBO);
			break;

		case ApplicationType.CAR_LOAN:
			CarLoanDetailsBO carLoanDetailsBO = carLoanDetailsService.get(id);
			carLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(carLoanDetailsBO);
			break;

		case ApplicationType.OVERDRAFT_FACILITIES_LOAN:
			OverDraftFacilitiesLoanDetailsBO overDraftFacilitiesLoanDetailsBO = overDraftFacilitiesLoanDetailsService
					.get(id);
			overDraftFacilitiesLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(overDraftFacilitiesLoanDetailsBO);
			break;

		case ApplicationType.DROPLINE_OVERDRAFT_FACILITIES_LOAN:
			DropLineOdFacilitiesLoanDetailsBO dropLineOdFacilitiesLoanDetailsBO = dropLineOdFacilitiesLoanDetailsService
					.get(id);
			dropLineOdFacilitiesLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(dropLineOdFacilitiesLoanDetailsBO);
			break;

		case ApplicationType.BANK_GUARANTEE_LOAN:
			BankGuaranteeLoanDetailsBO bankGuaranteeLoanDetailsBO = bankGuaranteeLoanDetailsService.get(id);
			bankGuaranteeLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(bankGuaranteeLoanDetailsBO);
			break;

		case ApplicationType.CC_FACILITIES_LOAN:
			CCFacilitiesLoanDetailsBO ccFacilitiesLoanDetailsBO = ccFacilitiesLoanDetailsService.get(id);
			ccFacilitiesLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(ccFacilitiesLoanDetailsBO);
			break;

		case ApplicationType.TERM_LOAN:
			TermLoanDetailsBO termLoanDetailsBO = termLoanDetailsService.get(id);
			termLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(termLoanDetailsBO);
			break;

		case ApplicationType.LOAN_AGAINST_FDS:
			LoanAgainstFDsDetailsBO loanAgainstFDsDetailsBO = loanAgainstFDsDetailsService.get(id);
			loanAgainstFDsDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(loanAgainstFDsDetailsBO);
			break;

		case ApplicationType.LOAN_AGAINST_SECURITIS:
			LoanAgainstSecuritiesLoanDetailsBO loanAgainstSecuritiesLoanDetailsBO = loanAgainstSecuritiesLoanDetailsService
					.get(id);
			loanAgainstSecuritiesLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(loanAgainstSecuritiesLoanDetailsBO);
			break;

		case ApplicationType.PROJECT_FINANCE_LOAN:
			ProjectFinanceLoanDetailsBO projectFinanceLoanDetailsBO = projectFinanceLoanDetailsService.get(id);
			projectFinanceLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(projectFinanceLoanDetailsBO);
			break;

		case ApplicationType.PRIVATE_EQUITY_FINANCE_LOAN:
			PrivateEquityFinanceLoanDetailsBO privateEquityFinanceLoanDetailsBO = privateEquityFinanceLoanDetailsService
					.get(id);
			privateEquityFinanceLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(privateEquityFinanceLoanDetailsBO);
			break;

		case ApplicationType.GOLD_LOAN:
			GoldLoanDetailsBO goldLoanDetailsBO = goldLoanDetailsService.get(id);
			goldLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(goldLoanDetailsBO);
			break;

		case ApplicationType.OTHER_LOAN:
			OthersLoanDetailsBO othersLoanDetailsBO = othersLoanDetailsService.get(id);
			othersLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(othersLoanDetailsBO);
			break;
		case ApplicationType.PERSONAL_LOAN:
			PersonalLoanDetailsBO personalLoanDetailsBO = personalLoanDetailsService.get(id);
			personalLoanDetailsBO.setEmploymentType(employmentType);
			lamsResponse.setData(personalLoanDetailsBO);
			break;
		default:
			lamsResponse.setMessage("Invalid Application Type Id");
			return lamsResponse;
		}
		lamsResponse.setMessage("Successfully get data");
		return lamsResponse;
	}

	@Override
	public LamsResponse getApplicationsForLender(Long userId) {
		List<Long> applicationTypesByUserId = lenderApplicationMappingRepository.getApplicationTypesByUserId(userId,
				true);
		logger.info("applicationTypesByUserId====" + applicationTypesByUserId + " usrId====>." + userId);
		if (CommonUtils.isListNullOrEmpty(applicationTypesByUserId)) {
			return new LamsResponse(HttpStatus.OK.value(), "No ApplicationType found for User");
		}
		List<Long> borrwersIds = applicationsRepository.getUserIdByApplicationTypeId(applicationTypesByUserId);
		if (CommonUtils.isListNullOrEmpty(borrwersIds)) {
			return new LamsResponse(HttpStatus.OK.value(), "No Borrower found for ApplicationType");
		}

		List<User> list = userMstrRepository.findByIdInAndIsActive(borrwersIds, true);
		List<UserBO> response = new ArrayList<>(list.size());
		for (User user : list) {
			UserBO userBo = new UserBO();
			BeanUtils.copyProperties(user, userBo);
			List<Applications> apps = applicationsRepository
					.findByUserIdAndIsActiveAndApplicationTypeIdIdIn(user.getId(), true, applicationTypesByUserId);
			List<ApplicationsBO> appResponse = new ArrayList<>(apps.size());
			Long employmentType = userMstrRepository.getEmpTypeById(user.getId());
			for (Applications app : apps) {
				switch (app.getApplicationTypeId().getId().intValue()) {

				case ApplicationType.HOME_LOAN:
					HomeLoanDetailsBO homeLoanDetailsBO = homeLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, homeLoanDetailsBO);
					homeLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(homeLoanDetailsBO);
					break;

				case ApplicationType.LOAN_AGAINST_PROPERTY:
					LoanAgainstPropertyDetailsBO loanAgainstPropertyDetailsBO = loanAgainstPropertyDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, loanAgainstPropertyDetailsBO);
					loanAgainstPropertyDetailsBO.setEmploymentType(employmentType);
					appResponse.add(loanAgainstPropertyDetailsBO);
					break;

				case ApplicationType.SECURED_BUSINESS_LOAN:
					SecuredBusinessLoanDetailsBO securedBusinessLoanDetailsBO = securedBusinessLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, securedBusinessLoanDetailsBO);
					securedBusinessLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(securedBusinessLoanDetailsBO);
					break;

				case ApplicationType.WORKING_CAPITAL_LOAN:
					WorkingCapitalLoanDetailsBO workingCapitalLoanDetailsBO = workingCapitalLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, workingCapitalLoanDetailsBO);
					workingCapitalLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(workingCapitalLoanDetailsBO);
					break;

				case ApplicationType.EDUCATION_LOAN:
					EducationLoanDetailsBO educationLoanDetailsBO = educationLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, educationLoanDetailsBO);
					educationLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(educationLoanDetailsBO);
					break;

				case ApplicationType.CAR_LOAN:
					CarLoanDetailsBO carLoanDetailsBO = carLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, carLoanDetailsBO);
					carLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(carLoanDetailsBO);
					break;

				case ApplicationType.OVERDRAFT_FACILITIES_LOAN:
					OverDraftFacilitiesLoanDetailsBO overDraftFacilitiesLoanDetailsBO = overDraftFacilitiesLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, overDraftFacilitiesLoanDetailsBO);
					overDraftFacilitiesLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(overDraftFacilitiesLoanDetailsBO);
					break;

				case ApplicationType.DROPLINE_OVERDRAFT_FACILITIES_LOAN:
					DropLineOdFacilitiesLoanDetailsBO dropLineOdFacilitiesLoanDetailsBO = dropLineOdFacilitiesLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, dropLineOdFacilitiesLoanDetailsBO);
					dropLineOdFacilitiesLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(dropLineOdFacilitiesLoanDetailsBO);
					break;

				case ApplicationType.BANK_GUARANTEE_LOAN:
					BankGuaranteeLoanDetailsBO bankGuaranteeLoanDetailsBO = bankGuaranteeLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, bankGuaranteeLoanDetailsBO);
					bankGuaranteeLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(bankGuaranteeLoanDetailsBO);
					break;

				case ApplicationType.CC_FACILITIES_LOAN:
					CCFacilitiesLoanDetailsBO ccFacilitiesLoanDetailsBO = ccFacilitiesLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, ccFacilitiesLoanDetailsBO);
					ccFacilitiesLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(ccFacilitiesLoanDetailsBO);
					break;

				case ApplicationType.TERM_LOAN:
					TermLoanDetailsBO termLoanDetailsBO = termLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, termLoanDetailsBO);
					termLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(termLoanDetailsBO);
					break;

				case ApplicationType.LOAN_AGAINST_FDS:
					LoanAgainstFDsDetailsBO loanAgainstFDsDetailsBO = loanAgainstFDsDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, loanAgainstFDsDetailsBO);
					loanAgainstFDsDetailsBO.setEmploymentType(employmentType);
					appResponse.add(loanAgainstFDsDetailsBO);
					break;

				case ApplicationType.LOAN_AGAINST_SECURITIS:
					LoanAgainstSecuritiesLoanDetailsBO loanAgainstSecuritiesLoanDetailsBO = loanAgainstSecuritiesLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, loanAgainstSecuritiesLoanDetailsBO);
					loanAgainstSecuritiesLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(loanAgainstSecuritiesLoanDetailsBO);
					break;

				case ApplicationType.PROJECT_FINANCE_LOAN:
					ProjectFinanceLoanDetailsBO projectFinanceLoanDetailsBO = projectFinanceLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, projectFinanceLoanDetailsBO);
					projectFinanceLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(projectFinanceLoanDetailsBO);
					break;

				case ApplicationType.PRIVATE_EQUITY_FINANCE_LOAN:
					PrivateEquityFinanceLoanDetailsBO privateEquityFinanceLoanDetailsBO = privateEquityFinanceLoanDetailsService
							.get(app.getId());
					BeanUtils.copyProperties(app, privateEquityFinanceLoanDetailsBO);
					privateEquityFinanceLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(privateEquityFinanceLoanDetailsBO);
					break;

				case ApplicationType.GOLD_LOAN:
					GoldLoanDetailsBO goldLoanDetailsBO = goldLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, goldLoanDetailsBO);
					goldLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(goldLoanDetailsBO);
					break;

				case ApplicationType.OTHER_LOAN:
					OthersLoanDetailsBO othersLoanDetailsBO = othersLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, othersLoanDetailsBO);
					othersLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(othersLoanDetailsBO);
					break;
				case ApplicationType.PERSONAL_LOAN:
					PersonalLoanDetailsBO personalLoanDetailsBO = personalLoanDetailsService.get(app.getId());
					BeanUtils.copyProperties(app, personalLoanDetailsBO);
					personalLoanDetailsBO.setEmploymentType(employmentType);
					appResponse.add(personalLoanDetailsBO);
					break;
				}
				userBo.setApplications(appResponse);
			}
			response.add(userBo);
		}
		return new LamsResponse(HttpStatus.OK.value(), "Success", response);
	}

	@Override
	public LamsResponse getApplicationsForLenderByApplicationId(Long appId, String status, Long lenderId) {
		List<Applications> applications = null;

		if (CommonUtils.Status.OPEN.equalsIgnoreCase(status)) {
			applications = applicationsRepository.findByApplicationTypeIdIdAndIsActiveAndStatus(appId, true, status);
			List<ApplicationsBO> applicationsBOs = new ArrayList<>(applications.size());
			for (Applications application : applications) {
				
				// Check lender has taken any action on this application, if no action taken then it is open for Lender.
				if(lenderBorrowerConnectionRepository.isActionTakenOnApplicationByLender(application.getId(),lenderId) == 0) {
					ApplicationsBO applicationsBO = new ApplicationsBO();
					BeanUtils.copyProperties(application, applicationsBO);
					if (!CommonUtils.isObjectNullOrEmpty(application.getUserId())) {
						User user = userMstrRepository.findOne(application.getUserId());
						applicationsBO.setFirstName(user.getFirstName());
						applicationsBO.setLastName(user.getLastName());
						applicationsBO.setName(user.getName());
						applicationsBO.setEmail(user.getEmail());
						applicationsBO.setMobile(user.getMobile());
						applicationsBO.setEmploymentType(user.getEmploymentType());
						applicationsBOs.add(applicationsBO);
					}
				}
			}
				
			return new LamsResponse(HttpStatus.OK.value(), "Success", applicationsBOs);
		} else {
			List<LenderBorrowerConnection> listData = lenderBorrowerConnectionRepository.findApplicationsByAppTypeIdAndStatusAndByCreatedId(appId, status, lenderId);
			List<LenderBorrowerConnectionBO> applicationsBOs = new ArrayList<>(listData.size());
			User lender = userMstrRepository.findOne(lenderId);
			
			for (LenderBorrowerConnection borrowerConnection : listData) {
				LenderBorrowerConnectionBO  applicationsBO = new LenderBorrowerConnectionBO();
				BeanUtils.copyProperties(borrowerConnection, applicationsBO);
				if(!CommonUtils.isObjectNullOrEmpty(borrowerConnection.getApplication())) {
					Applications application = borrowerConnection.getApplication();
					ApplicationsBO applicationsbo = new ApplicationsBO();
					BeanUtils.copyProperties(application, applicationsbo);
					if (!CommonUtils.isObjectNullOrEmpty(application.getUserId())) {
						User user = userMstrRepository.findOne(application.getUserId());
						applicationsbo.setFirstName(user.getFirstName());
						applicationsbo.setLastName(user.getLastName());
						applicationsbo.setName(user.getName());
						applicationsbo.setEmail(user.getEmail());
						applicationsbo.setMobile(user.getMobile());
						applicationsbo.setEmploymentType(user.getEmploymentType());
						
						if(lender!=null && lender.getUserType() == 1) {
							if(applicationsBO.getStatus().equals(Status.REJECTED)) {
								applicationsbo.setLenderButtonActionName("REJECTED");
							}
							else {
								if(applicationsbo.getStatus().equals(Status.ACCEPTED)) {
									applicationsbo.setLenderButtonActionName("SUBMIT FORM");
								}
								else if(applicationsbo.getStatus().equals(Status.REJECTED)) {
									applicationsbo.setLenderButtonActionName("REJECTED");
								}
								else if(applicationsbo.getStatus().equals(Status.SUBMIT_FORM)) {
									applicationsbo.setLenderButtonActionName("SUBMIT SACTION FORM");
								}
								else if(applicationsbo.getStatus().equals(Status.SANCTIONED)) {
									applicationsbo.setLenderButtonActionName("DONE");
								}
							}
						}
					}
					applicationsBO.setApplication(applicationsbo);
				}
				applicationsBOs.add(applicationsBO);
			}
			return new LamsResponse(HttpStatus.OK.value(), "Success", applicationsBOs);
		}
	}
	
	@Override
	public LamsResponse getApplicationsForPDUserByApplicationId(Long appId, List<String> status, Long lenderId) {
		List<Applications> applications = applicationsRepository.findByApplicationTypeIdIdAndIsActiveAndStatus(appId, true, "SUBMIT FORM");

		List<LenderBorrowerConnection> listData = lenderBorrowerConnectionRepository.findApplicationsByAppTypeIdAndStatusAndByCreatedId(appId, status);
		List<LenderBorrowerConnectionBO> applicationsBOs = new ArrayList<>(listData.size());
		
		for(Applications app : applications) {
			ApplicationsBO applicationsbo = new ApplicationsBO();
			LenderBorrowerConnectionBO  applicationsBO = new LenderBorrowerConnectionBO();
			BeanUtils.copyProperties(app, applicationsbo);
			if (!CommonUtils.isObjectNullOrEmpty(app.getUserId())) {
				User user = userMstrRepository.findOne(app.getUserId());
				applicationsbo.setFirstName(user.getFirstName());
				applicationsbo.setLastName(user.getLastName());
				applicationsbo.setName(user.getName());
				applicationsbo.setEmail(user.getEmail());
				applicationsbo.setMobile(user.getMobile());
				applicationsbo.setEmploymentType(user.getEmploymentType());
				applicationsbo.setLenderButtonActionName("VIEW");
			}
			applicationsBO.setApplication(applicationsbo);
			applicationsBOs.add(applicationsBO);
		}
		
		return new LamsResponse(HttpStatus.OK.value(), "Success", applicationsBOs);
	}

	@Override
	public Boolean updateStatus(Long applicationId, String status, Long userId) {
		Applications app = applicationsRepository.findOne(applicationId);
		app.setStatus(status);
		applicationsRepository.save(app);
		return Boolean.TRUE;
	}


	@Override
	public Long saveFromCP(Long preApplicationId, ApplicationsBO applicationRequestBO,Long brUserId, Long channelPartnerId,String cpUserCode) {
		logger.info("Enter in Save Application Sevice Impl--------------type----> " + applicationRequestBO.getApplicationTypeId());
		Long id=null;
		applicationRequestBO.setIsFromCP(true);
		applicationRequestBO.setPreApplicationId(applicationRequestBO.getPreApplicationId());
		
		try {
			switch (applicationRequestBO.getApplicationTypeId().intValue()) {

			case ApplicationType.HOME_LOAN:
				HomeLoanDetailsBO homeLoanDetailsBO = new HomeLoanDetailsBO();
				setCommonProperty(applicationRequestBO, homeLoanDetailsBO, cpUserCode, brUserId);
				id =  homeLoanDetailsService.save(homeLoanDetailsBO);
				break;

			case ApplicationType.LOAN_AGAINST_PROPERTY:
				LoanAgainstPropertyDetailsBO loanAgainstPropertyDetailsBO = new LoanAgainstPropertyDetailsBO();
				setCommonProperty(applicationRequestBO, loanAgainstPropertyDetailsBO, cpUserCode, brUserId);
				id = loanAgainstPropertyDetailsService.save(loanAgainstPropertyDetailsBO);
				break;

			case ApplicationType.SECURED_BUSINESS_LOAN:
				SecuredBusinessLoanDetailsBO securedBusinessLoanDetailsBO = new SecuredBusinessLoanDetailsBO();
				setCommonProperty(applicationRequestBO, securedBusinessLoanDetailsBO, cpUserCode, brUserId);
				id = securedBusinessLoanDetailsService.save(securedBusinessLoanDetailsBO);
				break;

			case ApplicationType.WORKING_CAPITAL_LOAN:
				WorkingCapitalLoanDetailsBO workingCapitalLoanDetailsBO = new WorkingCapitalLoanDetailsBO();
				setCommonProperty(applicationRequestBO, workingCapitalLoanDetailsBO, cpUserCode, brUserId);
				id = workingCapitalLoanDetailsService.save(workingCapitalLoanDetailsBO);
				break;

			case ApplicationType.EDUCATION_LOAN:
				EducationLoanDetailsBO educationLoanDetailsBO = new EducationLoanDetailsBO();
				setCommonProperty(applicationRequestBO, educationLoanDetailsBO, cpUserCode, brUserId);
				id = educationLoanDetailsService.save(educationLoanDetailsBO);
				break;

			case ApplicationType.CAR_LOAN:
				CarLoanDetailsBO carLoanDetailsBO = new CarLoanDetailsBO();
				setCommonProperty(applicationRequestBO, carLoanDetailsBO, cpUserCode, brUserId);
				id = carLoanDetailsService.save(carLoanDetailsBO);
				break;

			case ApplicationType.OVERDRAFT_FACILITIES_LOAN:
				OverDraftFacilitiesLoanDetailsBO overDraftFacilitiesLoanDetailsBO = new OverDraftFacilitiesLoanDetailsBO();
				setCommonProperty(applicationRequestBO, overDraftFacilitiesLoanDetailsBO, cpUserCode, brUserId);
				id = overDraftFacilitiesLoanDetailsService.save(overDraftFacilitiesLoanDetailsBO);
				break;

			case ApplicationType.DROPLINE_OVERDRAFT_FACILITIES_LOAN:
				DropLineOdFacilitiesLoanDetailsBO dropLineOdFacilitiesLoanDetailsBO = new DropLineOdFacilitiesLoanDetailsBO();
				setCommonProperty(applicationRequestBO, dropLineOdFacilitiesLoanDetailsBO, cpUserCode, brUserId);
				id = dropLineOdFacilitiesLoanDetailsService.save(dropLineOdFacilitiesLoanDetailsBO);
				break;

			case ApplicationType.BANK_GUARANTEE_LOAN:
				BankGuaranteeLoanDetailsBO bankGuaranteeLoanDetailsBO = new BankGuaranteeLoanDetailsBO();
				setCommonProperty(applicationRequestBO, bankGuaranteeLoanDetailsBO, cpUserCode, brUserId);
				id = bankGuaranteeLoanDetailsService.save(bankGuaranteeLoanDetailsBO);
				break;

			case ApplicationType.CC_FACILITIES_LOAN:
				CCFacilitiesLoanDetailsBO cCFacilitiesLoanDetailsBO = new CCFacilitiesLoanDetailsBO();
				setCommonProperty(applicationRequestBO, cCFacilitiesLoanDetailsBO, cpUserCode, brUserId);
				id = ccFacilitiesLoanDetailsService.save(cCFacilitiesLoanDetailsBO);
				break;

			case ApplicationType.TERM_LOAN:
				TermLoanDetailsBO termLoanDetailsBO = new TermLoanDetailsBO();
				setCommonProperty(applicationRequestBO, termLoanDetailsBO, cpUserCode, brUserId);
				id =  termLoanDetailsService.save(termLoanDetailsBO);
				break;

			case ApplicationType.LOAN_AGAINST_FDS:
				LoanAgainstFDsDetailsBO loanAgainstFDsDetailsBO = new LoanAgainstFDsDetailsBO();
				setCommonProperty(applicationRequestBO, loanAgainstFDsDetailsBO, cpUserCode, brUserId);
				id = loanAgainstFDsDetailsService.save(loanAgainstFDsDetailsBO);
				break;

			case ApplicationType.LOAN_AGAINST_SECURITIS:
				LoanAgainstSecuritiesLoanDetailsBO loanAgainstSecuritiesLoanDetailsBO = new LoanAgainstSecuritiesLoanDetailsBO();
				setCommonProperty(applicationRequestBO, loanAgainstSecuritiesLoanDetailsBO, cpUserCode, brUserId);
				id = loanAgainstSecuritiesLoanDetailsService.save(loanAgainstSecuritiesLoanDetailsBO);
				break;

			case ApplicationType.PROJECT_FINANCE_LOAN:
				ProjectFinanceLoanDetailsBO projectFinanceLoanDetailsBO = new ProjectFinanceLoanDetailsBO();
				setCommonProperty(applicationRequestBO, projectFinanceLoanDetailsBO, cpUserCode, brUserId);
				id = projectFinanceLoanDetailsService.save(projectFinanceLoanDetailsBO);
				break;

			case ApplicationType.PRIVATE_EQUITY_FINANCE_LOAN:
				PrivateEquityFinanceLoanDetailsBO privateEquityFinanceLoanDetailsBO = new PrivateEquityFinanceLoanDetailsBO();
				setCommonProperty(applicationRequestBO, privateEquityFinanceLoanDetailsBO, cpUserCode, brUserId);
				id = privateEquityFinanceLoanDetailsService.save(privateEquityFinanceLoanDetailsBO);
				break;

			case ApplicationType.GOLD_LOAN:
				GoldLoanDetailsBO goldLoanDetailsBO = new GoldLoanDetailsBO();
				setCommonProperty(applicationRequestBO, goldLoanDetailsBO, cpUserCode, brUserId);
				id = goldLoanDetailsService.save(goldLoanDetailsBO);
				break;

			case ApplicationType.OTHER_LOAN:
				OthersLoanDetailsBO othersLoanDetailsBO = new OthersLoanDetailsBO();
				setCommonProperty(applicationRequestBO, othersLoanDetailsBO, cpUserCode, brUserId);
				id = othersLoanDetailsService.save(othersLoanDetailsBO);
				break;

			case ApplicationType.PERSONAL_LOAN:
				PersonalLoanDetailsBO personalLoanDetailsBO = new PersonalLoanDetailsBO();
				setCommonProperty(applicationRequestBO, personalLoanDetailsBO, cpUserCode, brUserId);
				id = personalLoanDetailsService.save(personalLoanDetailsBO);
				break;

			default:
				id = null;
				break;
			}
			
			int i = preApplicationsRepository.inActiveByApplicationUserId(applicationRequestBO.getUserId(), "REJECTED",applicationRequestBO.getApplicationTypeId(), false);
			System.out.println("Row updates : "+i);
			i = preApplicationsRepository.inActiveByAppIdApplicationUserIdAndChannelPartnerId("ACCEPTED", preApplicationId, applicationRequestBO.getUserId(), channelPartnerId, true );
			System.out.println("Row updates : "+i);
			return id;
		} catch (Exception e) {
			logger.info("Error while save application form data");
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public int inActiveByUserId(Long userId) {
		return applicationsRepository.inActiveByUserId(userId);
	}

	private void setCommonProperty(ApplicationsBO from,ApplicationsBO to,String cpUserCode,Long brUserId) {
//		to.setId(from.getId());
		to.setApplicationTypeId(from.getApplicationTypeId());
		to.setUserId(brUserId);
		to.setLoanAmount(from.getLoanAmount());
		to.setLoanTypeId(Long.valueOf(CommonUtils.LoanType.CURRENT_LOAN));
		to.setIsFromCP(from.getIsFromCP());
		to.setStatus(CommonUtils.Status.OPEN);
		to.setLeadReferenceNo(cpUserCode);
		to.setPreApplicationId(from.getPreApplicationId());
	}

	@Override
	public int inActiveByApplicationIdAndUserId(Long applicationId, Long userId) {
		logger.info("Start inActiveByApplicationIdAndUserId()");
		logger.info("ApplicationId====>" + applicationId  + " User Id===> " + userId);
		int count = 0;
		if(CommonUtils.isObjectNullOrEmpty(userId)) {
			logger.log(Level.INFO, "UserId is NUll So Inactivating only by ApplicationId ");
			count = applicationsRepository.inActiveByApplicationId(applicationId);
		}else {
			count = applicationsRepository.inActiveByApplicationIdAndUserId(applicationId, userId);
			logger.log(Level.INFO, "Inactivating Application by UserId and Application Id");
		}
		logger.info("Inactivated Rows=================>" + count);
		logger.log(Level.INFO, "Start inActiveByApplicationIdAndUserId()");
		return count;
	}

	@Override
	public boolean getLastApplication(Long userId) {
		Date currentDate = new Date();
		Date lastDate = new Date();
		lastDate = applicationsRepository.getLastApplication(userId);
		if(lastDate != null) {
			if(getDateDiff(lastDate, currentDate, TimeUnit.DAYS) >= 30) {
				return true;
			}
		}
		return false;
	}
	
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}

	@Override
	public List<ApplicationsBO> getAllWithVerificationReports(Long userId) {
		List<ApplicationsBO> applicationBos = new ArrayList<ApplicationsBO>();
		List<VerificationReports> list = verificationReportRepository.findByCreatedBy(userId, true);
		
		for(VerificationReports vr : list) {
			ApplicationsBO applBO = convertDomainToBO(applicationsRepository.findOne(vr.getApplicationId()));
			applBO.setPdStatus(vr.getStatus());
			applicationBos.add(applBO);
		}
		
		return applicationBos;
	}

}

package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.InterviewQuestions;
import com.lams.api.repository.InterviewQuestionsConfigurationRepository;
import com.lams.api.service.InterviewQuestionsConfigurationService;
import com.lams.model.bo.InterviewQuestionsConfigBO;
import com.lams.model.bo.InterviewQuestionsConfigWrapperBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class InterviewQuestionsConfigurationServiceImpl implements InterviewQuestionsConfigurationService {

	public final static Logger logger = Logger.getLogger(InterviewQuestionsConfigurationServiceImpl.class.getName());

	@Autowired
	private InterviewQuestionsConfigurationRepository interviewQuestionsConfigurationRepository;

	@Override
	public InterviewQuestionsConfigBO getQuestionsByAgencyType(Long agencyType) {
		logger.log(Level.INFO," input id : "+agencyType);
		InterviewQuestions question = interviewQuestionsConfigurationRepository.findByIdAndIsActive(agencyType, true);
		
		InterviewQuestionsConfigBO questionBO = new InterviewQuestionsConfigBO();
		BeanUtils.copyProperties(question, questionBO);
		
		return questionBO;
	}

	@Override
	public LamsResponse updateQuestions(InterviewQuestionsConfigBO questionBO, Long userId) {
		InterviewQuestions question = interviewQuestionsConfigurationRepository.findOne(questionBO.getId());
		if (CommonUtils.isObjectNullOrEmpty(question)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", questionBO.getId());
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "User not Found");
		}
		BeanUtils.copyProperties(questionBO, question);
		
		if(question!=null && question.getId()!=null){
			question.setModifiedDate(new Date());
			question.setModifiedBy(questionBO.getId());
		}
		else{
			question = new InterviewQuestions();
			question.setCreatedBy(userId);
			question.setCreatedDate(new Date());
		}
		
		question = interviewQuestionsConfigurationRepository.save(question);
		logger.log(Level.INFO, "Successfully updated User Details for ID----" + question.getId());
		
		return new LamsResponse(HttpStatus.OK.value(), "Success", question);
	}

	@Override
	public List<InterviewQuestionsConfigBO> getQuestionsByAgencyType(Long agencyType, Boolean isActive) {
		List<InterviewQuestionsConfigBO> questionBOs = new ArrayList<InterviewQuestionsConfigBO>();
		
		logger.log(Level.INFO," input id : "+agencyType);
		List<InterviewQuestions> question = interviewQuestionsConfigurationRepository.findByAgencyType(agencyType);
		
		for(InterviewQuestions questionsBOs : question){
			InterviewQuestionsConfigBO questionBO = new InterviewQuestionsConfigBO();
			
			BeanUtils.copyProperties(questionsBOs, questionBO);
			questionBOs.add(questionBO);
		}
		
		return questionBOs;
	}

	@Override
	public LamsResponse saveAllQuestions(InterviewQuestionsConfigWrapperBO questionWrapperBO, Long userId) {
		for(InterviewQuestionsConfigBO qBO : questionWrapperBO.getAllQuestionsMstr()){
			InterviewQuestions question = new InterviewQuestions();
			BeanUtils.copyProperties(qBO, question);
			
			question.setCreatedBy(userId);
			question.setCreatedDate(new Date());
			question.setAgencyType(questionWrapperBO.getAgencyType());
			
			interviewQuestionsConfigurationRepository.save(question);
		}
		
		return new LamsResponse(HttpStatus.OK.value(), "Success", questionWrapperBO.getAllQuestionsMstr());
	}
}

package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.master.ApplicationTypeMstr;
import com.lams.api.domain.payment.PayOutStructure;
import com.lams.api.domain.payment.UserPayOutMapping;
import com.lams.api.repository.PayOutStructureRepository;
import com.lams.api.repository.UserPayOutMappingRepository;
import com.lams.api.repository.master.ApplicationTypeMstrRepository;
import com.lams.api.service.PayOutStructureService;
import com.lams.api.service.UserPayOutMappingService;
import com.lams.api.utils.PayOutJDBCImpl;
import com.lams.api.utils.SequenceNumberGenerator;
import com.lams.model.bo.ApplicationTypeWithPayOutBO;
import com.lams.model.bo.ChannelPartnerCommisionBO;
import com.lams.model.bo.PayOutStructureBO;
import com.lams.model.bo.UserPayOutMappingBO;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class PayOutStructureServiceImpl implements PayOutStructureService {

	public static final Logger logger = Logger.getLogger(PayOutStructureServiceImpl.class);

	@Autowired
	private PayOutStructureRepository payOutStructureRepository;
	
	@Autowired
	private ApplicationTypeMstrRepository applicationTypeMstrRepository;
	
	@Autowired
	private PayOutJDBCImpl payOutJDBCImpl;
	
	@Autowired
	private UserPayOutMappingService userPayOutMappingService;
	
	@Autowired
	private SequenceNumberGenerator sequenceNumberGenerator;
	
	@Autowired
	private UserPayOutMappingRepository userPayOutMappingRepository;

	/*
	 * GET ALL APPLICATIONS BY USER ID AND IS ACTIVE TRUE
	 */
	@Override
	public List<PayOutStructureBO> getAll(Long userId) {
		List<PayOutStructure> payOutList = payOutStructureRepository.findAll();
		List<PayOutStructureBO> payOutBOList = new ArrayList<>(payOutList.size());
		for (PayOutStructure applications : payOutList) {
			PayOutStructureBO abo = convertDomainToBO(applications);

			payOutBOList.add(abo);
		}
		return payOutBOList;
	}
	
	/**
	 * GET APPLICATION DETAILS BY APPLICATION ID
	 */
	@Override
	public List<PayOutStructureBO> get(Long id) {
		List<PayOutStructureBO> list = new ArrayList<PayOutStructureBO>();
		List<PayOutStructure> payOuts = payOutStructureRepository.findById(id);
		
		for(PayOutStructure payout : payOuts) {
			list.add(convertDomainToBO(payout));
		}
		
		return list;
	}

	/**
	 * SAVE AND UPDATE APPLICATION DATA
	 */
	@Override
	public void save(PayOutStructureBO bo) {
		payOutStructureRepository.save(convertBOToDomain(bo));
	}
	
	public PayOutStructure convertBOToDomain(PayOutStructureBO bo) {
		PayOutStructure domain = new PayOutStructure();
		
		domain.setApplicationTypeId(bo.getApplicationTypeId());
		domain.setDisbursmentFromAmt(bo.getDisbursmentFromAmt());
		domain.setDisbursmentToAmt(bo.getDisbursmentToAmt());
		domain.setPayOutInPerc(bo.getPayOutInPerc());
		domain.setCreatedBy(bo.getCreatedBy());
		domain.setCreatedDate(new Date());
		domain.setIsActive(bo.getIsActive());
		if(bo.getVersion() != null && bo.getVersion().trim().length() > 0) {
			domain.setVersion(bo.getVersion());
		}
		
		return domain;
	}
	
	public UserPayOutMapping convertBOToDomain(UserPayOutMappingBO bo) {
		UserPayOutMapping domain = new UserPayOutMapping();
		
		domain.setId(bo.getId());
		domain.setUserId(bo.getUserId());
		domain.setCreatedBy(bo.getCreatedBy());
		domain.setCreatedDate(bo.getCreatedDate());
		domain.setModifiedBy(bo.getModifiedBy());
		domain.setModifiedDate(new Date());
		domain.setApplTypeId(bo.getApplTypeId());
		domain.setIsActive(bo.getActive());
		if(bo.getPayoutVersion() != null && bo.getPayoutVersion().trim().length() > 0) {
			domain.setPayoutVersion(bo.getPayoutVersion());
		}
		
		return domain;
	}
	
	/**
	 * CONVERT APPLICATIONS DOMAIN OBJ TO BO OBJ
	 * 
	 * @param applications
	 * @return
	 */
	@Override
	public PayOutStructureBO convertDomainToBO(PayOutStructure payOuts) {
		PayOutStructureBO payOutBO = new PayOutStructureBO();
		if (CommonUtils.isObjectNullOrEmpty(payOuts)) {
			return payOutBO;
		}
		BeanUtils.copyProperties(payOuts, payOutBO);
		if (!CommonUtils.isObjectNullOrEmpty(payOuts.getApplicationTypeId())) {
			payOutBO.setId(payOuts.getId());
			payOutBO.setApplicationTypeId(payOuts.getApplicationTypeId());
			if (!CommonUtils.isObjectNullOrEmpty(payOuts.getDisbursmentFromAmt())) {
				payOutBO.setDisbursmentFromAmt(payOuts.getDisbursmentFromAmt());
			}
			
			if (!CommonUtils.isObjectNullOrEmpty(payOuts.getDisbursmentToAmt())) {
				payOutBO.setDisbursmentToAmt(payOuts.getDisbursmentToAmt());
			}
			payOutBO.setPayOutInPerc(payOuts.getPayOutInPerc());
			payOutBO.setVersion(payOuts.getVersion());
			
			if (!CommonUtils.isObjectNullOrEmpty(payOuts.getCreatedDate())) {
				payOutBO.setCreatedDate(payOuts.getCreatedDate());
			}
			
			payOutBO.setCreatedBy(payOuts.getCreatedBy());
			
			if (!CommonUtils.isObjectNullOrEmpty(payOuts.getModifiedDate())) {
				payOutBO.setModifiedDate(payOuts.getModifiedDate());
			}
			payOutBO.setModifiedBy(payOuts.getModifiedBy());
		}

		return payOutBO;
	}

	@Override
	public List<ApplicationTypeWithPayOutBO> getReportingOfPayouts() {
		List<ApplicationTypeWithPayOutBO> list = new ArrayList<ApplicationTypeWithPayOutBO>();
		List<ApplicationTypeMstr> allAppliTyps = applicationTypeMstrRepository.findAll();
		
		for(ApplicationTypeMstr aplType : allAppliTyps) {
			ApplicationTypeWithPayOutBO applTypeBO = new ApplicationTypeWithPayOutBO();
			
			applTypeBO.setId(aplType.getId());
			applTypeBO.setName(aplType.getName());
			applTypeBO.setCode(aplType.getCode());
			List<PayOutStructure> payStructureList = payOutStructureRepository.findByApplicationTypeIdAndVersionIsNull(aplType.getId());
			
			if(payStructureList!=null && payStructureList.size()>0) {
			}
			else {
				payStructureList.add(new PayOutStructure());
			}
			applTypeBO.setPayOuts(convertDomainToBO(payStructureList));
			list.add(applTypeBO);
		}
		return list;
	}

	@Override
	public List<PayOutStructureBO> convertDomainToBO(List<PayOutStructure> payOuts) {
		List<PayOutStructureBO> list = new ArrayList<PayOutStructureBO>();
		
		for(PayOutStructure domain : payOuts) {
			list.add(convertDomainToBO(domain));
		}
		return list;
	}

	@Override
	public void save(ApplicationTypeWithPayOutBO applicationsBO) {
		
		if(applicationsBO!=null && (applicationsBO.getPayOuts()!=null && applicationsBO.getPayOuts().size()>0)) {
			deleteAllPayoutsForApplication(applicationsBO.getId());
			
			for(PayOutStructureBO bo : applicationsBO.getPayOuts()) {
				bo.setApplicationTypeId(applicationsBO.getId());
				bo.setCreatedBy(applicationsBO.getUserId());
				bo.setIsActive(true);
				save(bo);
			}
		}
	}

	@Override
	public void deleteAllPayoutsForApplication(Long applId) {
		int rows = payOutJDBCImpl.deletePayOutStructureEntry(applId);
		logger.info(rows + "No. of Deleted");
	}

	@Override
	public List<ApplicationTypeWithPayOutBO> getReportingOfPayoutsForUser(Long userId) {
		List<ApplicationTypeWithPayOutBO> list = new ArrayList<ApplicationTypeWithPayOutBO>();
		List<ApplicationTypeMstr> allAppliTyps = applicationTypeMstrRepository.findAll();
		
		for(ApplicationTypeMstr mstr : allAppliTyps) {
			ApplicationTypeWithPayOutBO applTypeBO = new ApplicationTypeWithPayOutBO();
			
			applTypeBO.setId(mstr.getId());
			applTypeBO.setName(mstr.getName());
			applTypeBO.setCode(mstr.getCode());
			
			List<PayOutStructure> payStructureList  = new ArrayList<PayOutStructure>();
			List<UserPayOutMappingBO> payStructureBOList  = new ArrayList<UserPayOutMappingBO>();
			
			payStructureBOList = userPayOutMappingService.getCustomPayoutsByUserIdAndApplId(userId, mstr.getId());
			if(payStructureBOList!=null && payStructureBOList.size()>0) {
				payStructureList = payOutStructureRepository.findByVersion(payStructureBOList.get(0).getPayoutVersion());
			}
			else {
				payStructureList = payOutStructureRepository.findByApplicationTypeIdAndVersionIsNull(mstr.getId());
			}
			applTypeBO.setPayOuts(convertDomainToBO(payStructureList));
			list.add(applTypeBO);
		}
		
		return list;
	}

	@Override
	public void savePayoutForChnlPartner(ApplicationTypeWithPayOutBO applicationsBO, Long chnlprtnrId) throws Exception {
		UserPayOutMappingBO existingMapping = userPayOutMappingService.findByUserIdAndApplTypeIdAndIsActive(chnlprtnrId, applicationsBO.getId());
		if(existingMapping != null && existingMapping.getId() != null) {
			int rows = payOutJDBCImpl.deletePayOutByVersion(existingMapping.getPayoutVersion());
			logger.info(rows + "No. of Deleted");
			
			existingMapping.setModifiedDate(new Date());
			existingMapping.setModifiedBy(applicationsBO.getUserId());
			userPayOutMappingRepository.save(convertBOToDomain(existingMapping));
			
			if(applicationsBO != null && (applicationsBO.getPayOuts() != null && applicationsBO.getPayOuts().size() > 0)) {
				for(PayOutStructureBO payouts : applicationsBO.getPayOuts()) {
					payouts.setVersion(existingMapping.getPayoutVersion());
					payouts.setApplicationTypeId(applicationsBO.getId());
					payouts.setCreatedBy(applicationsBO.getUserId());
					payouts.setCreatedDate(new Date());
					payouts.setIsActive(true);
					payOutStructureRepository.save(convertBOToDomain(payouts));
				}
			}
		}
		else {
			UserPayOutMappingBO newMapping = new UserPayOutMappingBO();
			newMapping.setUserId(chnlprtnrId);
			newMapping.setCreatedDate(new Date());
			newMapping.setCreatedBy(applicationsBO.getUserId());
			newMapping.setApplTypeId(applicationsBO.getId());
			
			newMapping.setPayoutVersion(sequenceNumberGenerator.generate("PAYOUTS", chnlprtnrId));
			userPayOutMappingService.save(newMapping);
			
			if(applicationsBO != null && (applicationsBO.getPayOuts() != null && applicationsBO.getPayOuts().size() > 0)) {
				for(PayOutStructureBO payouts : applicationsBO.getPayOuts()) {
					payouts.setVersion(newMapping.getPayoutVersion());
					payouts.setApplicationTypeId(applicationsBO.getId());
					payouts.setCreatedBy(applicationsBO.getUserId());
					payouts.setCreatedDate(new Date());
					payOutStructureRepository.save(convertBOToDomain(payouts));
				}
			}
		}
	}

	@Override
	public List<ChannelPartnerCommisionBO> getChannelPartnerCommisionYearMonthWise(Long chPartnerId) {
		List<ChannelPartnerCommisionBO> chCommisionList = payOutJDBCImpl.getAllChannelPartnerWorkList(chPartnerId);
		
		for(ChannelPartnerCommisionBO item : chCommisionList) {
			List<UserPayOutMappingBO> payStructureBOList  = new ArrayList<UserPayOutMappingBO>();
			List<PayOutStructure> payStructureList  = new ArrayList<PayOutStructure>();
			payStructureBOList = userPayOutMappingService.getCustomPayoutsByUserIdAndApplId(chPartnerId, item.getApplTypeId());
			Double totalDisbursedAmt=item.getTotalDisbursmentAmount();
			Double commision=new Double(0);
			/**
			 * Calculation should done by custom settings if it is applicable, else do by standard way.
			 */
			if(payStructureBOList!=null && payStructureBOList.size()>0) {
				payStructureList = payOutStructureRepository.findByVersion(payStructureBOList.get(0).getPayoutVersion());
			}
			else {
				payStructureList = payOutStructureRepository.findByApplicationTypeIdAndVersionIsNull(item.getApplTypeId());
			}
			
			Double diff = null;
			int listIndex=0;
			for(PayOutStructure pay : payStructureList) {
				if(totalDisbursedAmt <= (pay.getDisbursmentToAmt() - pay.getDisbursmentFromAmt())) {
					/**
					 * This means that we are iterated to last loop,
					 * Here we have to consider balanced amount from total disbursed amount.
					 */
					commision += totalDisbursedAmt * (pay.getPayOutInPerc()/100);//10999.9945, 23349.9945, 37599.9945, 
					//totalDisbursedAmt = totalDisbursedAmt - diff;
					break;
				}
				else {
					if(listIndex == payStructureList.size()-1) {
						//This block depict that we enter into logically last iteration 
						if(totalDisbursedAmt > (pay.getDisbursmentToAmt() - pay.getDisbursmentFromAmt())) {
							diff = totalDisbursedAmt;
							commision += diff * (pay.getPayOutInPerc()/100);
							totalDisbursedAmt = totalDisbursedAmt - diff;
						}
					}
					else {
						diff = pay.getDisbursmentToAmt() - pay.getDisbursmentFromAmt();
						commision += diff * (pay.getPayOutInPerc()/100);
						totalDisbursedAmt = totalDisbursedAmt - diff;
					}
				}
				listIndex++;
			}
			item.setTotalCommision(commision);
		}
		return chCommisionList;
	}

	@Override
	public List<ApplicationTypeWithPayOutBO> getPayOutByUserId(Long userId) {
		List<ApplicationTypeWithPayOutBO> list = new ArrayList<ApplicationTypeWithPayOutBO>();
		List<ApplicationTypeMstr> allAppliTyps = applicationTypeMstrRepository.findAll();
		
		for(ApplicationTypeMstr aplType : allAppliTyps) {
			List<PayOutStructure> payStructureList = new ArrayList<PayOutStructure>();
			ApplicationTypeWithPayOutBO applTypeBO = new ApplicationTypeWithPayOutBO();
			
			applTypeBO.setId(aplType.getId());
			applTypeBO.setName(aplType.getName());
			applTypeBO.setCode(aplType.getCode());
			UserPayOutMapping userPay = userPayOutMappingRepository.findByUserIdAndApplTypeIdAndIsActive(userId,aplType.getId(), true);
			
			if(userPay!=null) {
				payStructureList = payOutStructureRepository.findByVersion(userPay.getPayoutVersion());
			}
			else {
				payStructureList = payOutStructureRepository.findByApplicationTypeIdAndVersionIsNull(aplType.getId());
			}
			
			if(payStructureList!=null && payStructureList.size()>0) {
			}
			else {
				payStructureList.add(new PayOutStructure());
			}
			applTypeBO.setPayOuts(convertDomainToBO(payStructureList));
			list.add(applTypeBO);
		}
		return list;
	}
}

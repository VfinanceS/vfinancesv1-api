package com.lams.api.service;

import java.util.List;

import com.lams.model.bo.InterviewQuestionsAnswerConfigBO;
import com.lams.model.bo.InterviewQuestionsAnswersConfigWrapperBO;
import com.lams.model.bo.LamsResponse;

public interface InterviewQuestionsAnswerConfigurationService {

	public InterviewQuestionsAnswerConfigBO getQuestionsByAgencyType(Long id);

	public LamsResponse writeAnswers(List<InterviewQuestionsAnswerConfigBO> answersList, Long userId);
	
	public List<InterviewQuestionsAnswerConfigBO> getQuestionsByAgencyType(Long agencyType, Boolean isActive);
	
	public LamsResponse saveAllQuestions(InterviewQuestionsAnswersConfigWrapperBO allQuestionsMstr, Long userId);
	
	public InterviewQuestionsAnswersConfigWrapperBO getQuestionsAnsByAgencyType(Long agencyType, Long applicationId);
	
	public InterviewQuestionsAnswersConfigWrapperBO getQuestionsAnsByAgencyType(Long applicationId);

}

package com.lams.api.service;

import java.util.List;

import com.lams.api.domain.PreApplications;
import com.lams.api.domain.User;
import com.lams.model.bo.PreApplicationsBO;
import com.lams.model.bo.UserBO;

public interface PreApplicationsService {
	
	public Boolean updateStatus(Long applicationId, String status,Long userId);
	
	public Long saveFromCP(PreApplicationsBO applicationRequestBO,Long brUserId,User user);
	
	public List<PreApplicationsBO> getAllActivePreApplicationByUserId(Long brUserId);
	
	public List<PreApplicationsBO> getAllActivePreApplicationByUserIdAndChannelPartnerId(Long brUserId, Long channelPartnerId);
	
	public List<UserBO> getAllActivePreApplicationByChannelPartnerId(Long channelPartnerId);

	PreApplicationsBO convertDomainToBO(PreApplications applications);
	
	public List<Long> getDistinctChannelPartnerIdByUserIdAndIsActive(Long userId);

}

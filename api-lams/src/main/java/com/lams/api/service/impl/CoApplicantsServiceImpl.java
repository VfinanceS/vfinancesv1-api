package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.UserCoapplicant;
import com.lams.api.domain.master.AddressMstr;
import com.lams.api.domain.master.CityMstr;
import com.lams.api.domain.master.CountryMstr;
import com.lams.api.domain.master.StateMstr;
import com.lams.api.repository.CoApplicantsRepository;
import com.lams.api.service.CoApplicantsService;
import com.lams.api.utils.AddressJDBCImpl;
import com.lams.model.bo.AddressBO;
import com.lams.model.bo.UserCoApplicantBO;
import com.lams.model.bo.master.CityBO;
import com.lams.model.bo.master.CountryBO;
import com.lams.model.bo.master.StateBO;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class CoApplicantsServiceImpl implements CoApplicantsService {

	public static final Logger logger = Logger.getLogger(CoApplicantsServiceImpl.class);

	@Autowired
	private CoApplicantsRepository coApplicantsRepository;
	
	@Autowired
	private AddressJDBCImpl addressJDBCImpl;
	

	@Override
	public List<UserCoApplicantBO> getAllCoApplicants(Long userId) {
		List<UserCoapplicant> coApplicationsList = coApplicantsRepository.getAllCoApplicants(userId);
		List<UserCoApplicantBO> list = new ArrayList<>(coApplicationsList.size());
		
		UserCoApplicantBO createNewCoApplicant = new UserCoApplicantBO();
		createNewCoApplicant.setId(new Long(5));
		createNewCoApplicant.setNameOfCoapplicant("Create New CoApplicant");
		
		if(coApplicationsList!=null && coApplicationsList.size()>0){
			for(UserCoapplicant uc : coApplicationsList){
				UserCoApplicantBO coAppBO = new UserCoApplicantBO();
				coAppBO.setId(uc.getId());
				coAppBO.setParentUserId(uc.getParentUserId());
				coAppBO.setFirstName(uc.getFirstName());
				coAppBO.setLastName(uc.getLastName());
				coAppBO.setNameOfCoapplicant(uc.getFirstName()+" "+uc.getLastName());
				list.add(coAppBO);
			}
		}
		
		if(list!=null && list.size()<40){
			list.add(createNewCoApplicant);
		}
		
		return list;
	}
	
	@Override
	public UserCoApplicantBO convertDomainToBO(UserCoapplicant applications) {
		UserCoApplicantBO applicationsBO = new UserCoApplicantBO();
		applicationsBO.setId(applications.getId());
		applicationsBO.setParentUserId(applications.getParentUserId());
		applicationsBO.setNameOfCoapplicant("");
		if (CommonUtils.isObjectNullOrEmpty(applications)) {
			return applicationsBO;
		}
		
		applicationsBO.setName(applications.getFirstName()+" "+applications.getLastName());
		applicationsBO.setEmail(applications.getFirstName());
		applicationsBO.setMobile(applications.getFirstName());
		applicationsBO.setInvitationCount(applications.getInvitationCount());
		applicationsBO.setFirstName(applications.getFirstName());
		applicationsBO.setLastName(applications.getLastName());
		applicationsBO.setMiddleName(applications.getMiddleName());
		applicationsBO.setBirthDate(applications.getBirthDate());
		applicationsBO.setGender(applications.getGender());
		//applicationsBO.setUserType(new Long(1));
		applicationsBO.setSalutation(applications.getSalutation());
		applicationsBO.setPanCard(applications.getPanCard());
		applicationsBO.setAadharCardNo(applications.getAadharCardNo());
		applicationsBO.setEduQualification(applications.getEduQualification());
		applicationsBO.setContactNumber(applications.getContactNumber());
		
		applicationsBO.setEmploymentType(applications.getEmploymentType());
		applicationsBO.setEmployerName(applications.getEmployerName());
		applicationsBO.setGrossMonthlyIncome(applications.getGrossMonthlyIncome());
		applicationsBO.setTotalWorkExperience(applications.getTotalWorkExperience());
		applicationsBO.setEntityName(applications.getEntityName());
		applicationsBO.setContactPersonName(applications.getContactPersonName());
		applicationsBO.setGstNumber(applications.getGstNumber());
		applicationsBO.setAboutMe(applications.getAboutMe());
		//applicationsBO.setCode(applications.getco());
		applicationsBO.setEntityType(applications.getEntityType());
		applicationsBO.setSelfEmployedType(applications.getSelfEmployedType());
		applicationsBO.setIsProfileFilled(applications.getIsProfileFilled());

		
		List<AddressMstr> addressMstrList = addressJDBCImpl.getAdressList("select * from address_mstr where user_id=?", new Object[]{applications.getId()});;
		for (AddressMstr addressMstr : addressMstrList) {
			AddressBO addressBO = new AddressBO();
			BeanUtils.copyProperties(addressMstr, addressBO);
			// Copy City
			CityMstr cityMstr = addressMstr.getCity();
			if (!CommonUtils.isObjectNullOrEmpty(cityMstr)) {
				CityBO cityBO = new CityBO();
				BeanUtils.copyProperties(cityMstr, cityBO);
				addressBO.setCity(cityBO);

				// Copy State
				StateMstr state = cityMstr.getState();
				if (!CommonUtils.isObjectNullOrEmpty(state)) {
					StateBO stateBO = new StateBO();
					BeanUtils.copyProperties(state, stateBO);
					addressBO.setState(stateBO);

					// Copy Country
					CountryMstr country = state.getCountry();
					if (!CommonUtils.isObjectNullOrEmpty(country)) {
						CountryBO coutryBO = new CountryBO();
						BeanUtils.copyProperties(country, coutryBO);
						addressBO.setCountry(coutryBO);
					}
				}
			}
			if (addressMstr.getAddType() == CommonUtils.AddressType.PERMANENT) {
				applicationsBO.setPermanentAdd(addressBO);
			} else if (addressMstr.getAddType() == CommonUtils.AddressType.COMMUNICATION) {
				applicationsBO.setCommunicationAdd(addressBO);
			} else if (addressMstr.getAddType() == CommonUtils.AddressType.EMPLOYMENT_ADD) {
				applicationsBO.setEmploymentAddress(addressBO);
			}  
		}
		/*BusinessTypeMstr businessTypeMstr = user.getBusinessTypeMstr();
		if (!CommonUtils.isObjectNullOrEmpty(businessTypeMstr)) {
			BusinessTypeBO bo = new BusinessTypeBO();
			BeanUtils.copyProperties(businessTypeMstr, bo);
			userBo.setBusinessType(bo);
		}
		*/
		return applicationsBO;
	}

	@Override
	public UserCoApplicantBO getCoApplicantProfile(Long id, Long parentUserId) {
		UserCoapplicant coApplicantUser = coApplicantsRepository.findOne(id);
		return convertDomainToBO(coApplicantUser);
	}

	@Override
	public UserCoApplicantBO getCoApplicantProfile(Long id) {
		UserCoapplicant uca = coApplicantsRepository.findOne(id);
		
		UserCoApplicantBO ucb = convertDomainToBO(uca);
		return ucb;
	}
}

package com.lams.api.service;

import java.util.List;

import com.lams.model.bo.VerificationReportsBO;

public interface VerificationReportService {

	public List<VerificationReportsBO> getAll(Long userId);
	
	public Long save(VerificationReportsBO applicationsBO);
}

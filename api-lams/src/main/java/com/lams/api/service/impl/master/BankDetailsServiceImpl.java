package com.lams.api.service.impl.master;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.User;
import com.lams.api.domain.master.BankDetails;
import com.lams.api.repository.master.BankDetailsRepository;
import com.lams.api.service.master.BankDetailsService;
import com.lams.model.bo.BankDetailsBO;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class BankDetailsServiceImpl implements BankDetailsService {

	public static final Logger logger = Logger.getLogger(BankDetailsServiceImpl.class.getName());

	@Autowired
	private BankDetailsRepository bankDetailsRepository;

	@Override
	public void saveBankDetails(BankDetailsBO bankDetailsBO, Long userId) {
		logger.log(Level.INFO, "Start saveBankDetails()");
		if (CommonUtils.isObjectNullOrEmpty(bankDetailsBO)) {
			logger.log(Level.INFO, "saveBankDetails must not be null while saving");
			return;
		}
		BankDetails bankDetails = null;
		if (CommonUtils.isObjectNullOrEmpty(bankDetailsBO.getId())) {
			bankDetails = new BankDetails();
			bankDetails.setUserId(bankDetailsBO.getUserId());
			bankDetails.setBankName(bankDetailsBO.getBankName());
			bankDetails.setAccountNumber(bankDetailsBO.getAccountNumber());
			bankDetails.setAccountName(bankDetailsBO.getAccountName());
			bankDetails.setNeftIfscCode(bankDetailsBO.getNeftIfscCode());
			bankDetails.setUser(new User(userId));
		} else {
			bankDetails = bankDetailsRepository.findByUserId(bankDetailsBO.getUserId());
			if (CommonUtils.isObjectNullOrEmpty(bankDetails)) {
				logger.log(Level.WARNING, "Invalid saveBankDetails Id===>{}", bankDetailsBO.getId());
				return;
			}
			bankDetails.setModifiedBy(userId);
			bankDetails.setModifiedDate(new Date());
		}
		
		bankDetails.setUserId(bankDetailsBO.getUserId());
		bankDetails.setBankName(bankDetailsBO.getBankName());
		bankDetails.setAccountNumber(bankDetailsBO.getAccountNumber());
		bankDetails.setAccountName(bankDetailsBO.getAccountName());
		bankDetails.setNeftIfscCode(bankDetailsBO.getNeftIfscCode());
		
		bankDetailsRepository.save(bankDetails);
		logger.log(Level.INFO, "End saveBankDetails()");
	}


	public BankDetailsBO convertDomainToBO(BankDetails domain){
		BankDetailsBO bo = new BankDetailsBO();
		
		bo.setUserId(domain.getUserId());
		bo.setBankName(domain.getBankName());
		bo.setAccountNumber(domain.getAccountNumber());
		bo.setAccountName(domain.getAccountName());
		bo.setNeftIfscCode(domain.getNeftIfscCode());
		
		return bo;
	}

}

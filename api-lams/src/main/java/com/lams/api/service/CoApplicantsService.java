package com.lams.api.service;

import java.util.List;

import com.lams.api.domain.UserCoapplicant;
import com.lams.model.bo.UserCoApplicantBO;

public interface CoApplicantsService {

	public List<UserCoApplicantBO> getAllCoApplicants(Long userId);
	
	public UserCoApplicantBO getCoApplicantProfile(Long id, Long parentUserId);
	
	public UserCoApplicantBO getCoApplicantProfile(Long id);

	UserCoApplicantBO convertDomainToBO(UserCoapplicant applications);
}

package com.lams.api.service;

import java.util.List;

import com.lams.api.domain.payment.PayOutStructure;
import com.lams.model.bo.ApplicationTypeWithPayOutBO;
import com.lams.model.bo.ChannelPartnerCommisionBO;
import com.lams.model.bo.PayOutStructureBO;

public interface PayOutStructureService {

	public List<PayOutStructureBO> getAll(Long userId);
	
	public List<PayOutStructureBO> get(Long id);
	
	public void save(PayOutStructureBO applicationsBO);
	
	public void save(ApplicationTypeWithPayOutBO applicationsBO);
	
	public void savePayoutForChnlPartner(ApplicationTypeWithPayOutBO applicationsBO, Long chnlprtnrId) throws Exception;
	
	public List<ApplicationTypeWithPayOutBO> getReportingOfPayouts();
	
	public List<ApplicationTypeWithPayOutBO> getReportingOfPayoutsForUser(Long userId);

	PayOutStructureBO convertDomainToBO(PayOutStructure payOuts);
	
	List<PayOutStructureBO> convertDomainToBO(List<PayOutStructure> payOuts);
	
	public void deleteAllPayoutsForApplication(Long applId);
	
	public List<ChannelPartnerCommisionBO> getChannelPartnerCommisionYearMonthWise(Long chPartnerId);
	
	public List<ApplicationTypeWithPayOutBO> getPayOutByUserId(Long userId);

}

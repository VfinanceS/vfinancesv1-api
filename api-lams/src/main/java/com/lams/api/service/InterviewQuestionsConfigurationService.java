package com.lams.api.service;

import java.util.List;

import com.lams.model.bo.InterviewQuestionsConfigBO;
import com.lams.model.bo.InterviewQuestionsConfigWrapperBO;
import com.lams.model.bo.LamsResponse;

public interface InterviewQuestionsConfigurationService {

	public InterviewQuestionsConfigBO getQuestionsByAgencyType(Long id);

	public LamsResponse updateQuestions(InterviewQuestionsConfigBO userBO, Long userId);
	
	public List<InterviewQuestionsConfigBO> getQuestionsByAgencyType(Long agencyType, Boolean isActive);
	
	public LamsResponse saveAllQuestions(InterviewQuestionsConfigWrapperBO allQuestionsMstr, Long userId);

}

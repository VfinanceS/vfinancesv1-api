package com.lams.api.service.impl.master;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.master.BankMstr;
import com.lams.api.repository.master.BankMstrRepository;
import com.lams.api.service.master.BankService;
import com.lams.model.bo.BankBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.master.MasterBaseBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.Enums;
import com.lams.model.utils.Enums.Mode;

@Service
@Transactional
public class BankServiceImpl implements BankService {

	public static final Logger logger = Logger.getLogger(BankServiceImpl.class.getName());

	@Autowired
	private BankMstrRepository bankMstrRepository;

	@Override
	public List<MasterBaseBO> getBanksByMode(Integer mode) {
		logger.log(Level.INFO, "Entry in getBanksByMode");
		List<BankMstr> banks = null;
		Mode type = Enums.Mode.getType(mode);
		switch (type) {
		case ACTIVE:
			banks = bankMstrRepository.findByIsActive(true);
			break;
		case INACTIVE:
			banks = bankMstrRepository.findByIsActive(false);
			break;
		case BOTH:
			banks = bankMstrRepository.findAll();
			break;
		default:
			break;
		}
		if (CommonUtils.isListNullOrEmpty(banks)) {
			logger.log(Level.WARNING, "No salutaions found for MODE===>{0}", mode);
			return Collections.emptyList();
		}

		List<MasterBaseBO> response = new ArrayList<>(banks.size());
		for (BankMstr bank : banks) {
			MasterBaseBO baseBO = new MasterBaseBO();
			BeanUtils.copyProperties(bank, baseBO);
			response.add(baseBO);
		}
		logger.log(Level.INFO, "Exit in getBanksByMode");
		return response;
	}

	@Override
	public List<BankBO> getBanks() {
		List<BankMstr> allBanks = bankMstrRepository.findAll();
		List<BankBO> bankBOs = new ArrayList<BankBO>();
		
		for(BankMstr bank : allBanks){
			bankBOs.add(convertDomainToBO(bank));
		}
		
		return bankBOs;
	}
	
	public BankBO convertDomainToBO(BankMstr domain){
		BankBO bo = new BankBO();
		
		bo.setId(domain.getId());
		bo.setCode(domain.getCode());
		bo.setName(domain.getName());
		bo.setIsActive(domain.getIsActive());
		
		return bo;
	}
	
	public BankMstr convertBOToDomain(BankBO bo){
		BankMstr domain = new BankMstr();
		
		domain.setId(bo.getId());
		domain.setCode(bo.getCode());
		domain.setName(bo.getName());
		domain.setIsActive(bo.getIsActive());
		
		return domain;
	}

	@Override
	public LamsResponse saveBankDetails(BankBO bo, Long createdby) {
		BankMstr bacnkChk = new BankMstr();
		
		if(CommonUtils.isObjectNullOrEmpty(bo.getId())){
			bacnkChk.setCode(bo.getCode());
			bacnkChk.setName(bo.getName());
			bacnkChk.setIsActive(bo.getIsActive());
			bacnkChk.setCreatedBy(createdby);
			bacnkChk.setCreatedDate(new Date());
		}
		else{
			bacnkChk = bankMstrRepository.findOne(bo.getId());
			bacnkChk.setCode(bo.getCode());
			bacnkChk.setName(bo.getName());
			bacnkChk.setIsActive(bo.getIsActive());
			bacnkChk.setModifiedBy(createdby);
			bacnkChk.setModifiedDate(new Date());
		}
		
		bankMstrRepository.save(bacnkChk);
		
		return new LamsResponse(HttpStatus.OK.value(), "Bank Details updated", bacnkChk);
	}
}

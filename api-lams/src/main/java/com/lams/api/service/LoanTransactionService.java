package com.lams.api.service;

import java.util.List;

import com.lams.api.domain.LoanTransaction;
import com.lams.model.bo.LoanTransactionBO;

public interface LoanTransactionService {

	public List<LoanTransaction> getAll(Long userId);
	
	public LoanTransaction get(Long id);
	
	public Long save(LoanTransaction loanTransactionBO);
	
	public List<LoanTransactionBO> findByApplIdAndBrUserId(Long applId, Long brUserId);
	
}

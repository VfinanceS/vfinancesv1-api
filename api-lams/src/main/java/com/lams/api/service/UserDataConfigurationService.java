package com.lams.api.service;

import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.UserDataConfigBO;

public interface UserDataConfigurationService {

	public UserDataConfigBO getUserById(Long id);

	public LamsResponse updateUserDetails(UserDataConfigBO userBO);

	public UserDataConfigBO getUserByTypeAndIsActive(Long id);

	LamsResponse updateUserConfiguration(UserDataConfigBO userBO, Long userId, Long doingByUser);
	
	public LamsResponse getUserByAgencyType(Long agencyTypeId);
	
	public UserDataConfigBO getConfigurationDetails(Long id);
}

package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.payment.UserPayOutMapping;
import com.lams.api.repository.UserPayOutMappingRepository;
import com.lams.api.service.UserPayOutMappingService;
import com.lams.model.bo.UserPayOutMappingBO;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class UserPayOutMappingServiceImpl implements UserPayOutMappingService {

	public static final Logger logger = Logger.getLogger(UserPayOutMappingServiceImpl.class);

	@Autowired
	private UserPayOutMappingRepository userPayOutMappingRepository;


	@Override
	public List<UserPayOutMappingBO> getAll(Long userId) {
		List<UserPayOutMapping> payOutList = userPayOutMappingRepository.findAll();
		List<UserPayOutMappingBO> payOutBOList = new ArrayList<>(payOutList.size());
		for (UserPayOutMapping applications : payOutList) {
			UserPayOutMappingBO abo = convertDomainToBO(applications);

			payOutBOList.add(abo);
		}
		return payOutBOList;
	}
	
	/**
	 * GET APPLICATION DETAILS BY APPLICATION ID
	 */
	@Override
	public UserPayOutMappingBO findByUserIdAndApplTypeIdAndIsActive(Long id, Long applTypeId) {
		UserPayOutMapping applications = userPayOutMappingRepository.findByUserIdAndApplTypeIdAndIsActive(id, applTypeId, true);
		return convertDomainToBO(applications);
	}

	/**
	 * SAVE AND UPDATE APPLICATION DATA
	 */
	@Override
	public void save(UserPayOutMappingBO bo) {
		UserPayOutMapping payOutMapping = new UserPayOutMapping();
		payOutMapping.setApplTypeId(bo.getApplTypeId());
		payOutMapping.setUserId(bo.getUserId());
		payOutMapping.setCreatedDate(new Date());
		payOutMapping.setCreatedBy(bo.getCreatedBy());
		payOutMapping.setPayoutVersion(bo.getPayoutVersion());
		payOutMapping.setIsActive(true);
		
		userPayOutMappingRepository.save(payOutMapping);
	}
	
	/**
	 * CONVERT APPLICATIONS DOMAIN OBJ TO BO OBJ
	 * 
	 * @param applications
	 * @return
	 */
	public UserPayOutMappingBO convertDomainToBO(UserPayOutMapping userPayouts) {
		UserPayOutMappingBO userPayOutMappingBO = new UserPayOutMappingBO();
		if (CommonUtils.isObjectNullOrEmpty(userPayouts)) {
			return userPayOutMappingBO;
		}
		BeanUtils.copyProperties(userPayouts, userPayOutMappingBO);
		
		userPayOutMappingBO.setApplTypeId(userPayouts.getApplTypeId());
		userPayOutMappingBO.setId(userPayouts.getId());
		userPayOutMappingBO.setUserId(userPayouts.getUserId());
		userPayOutMappingBO.setPayoutVersion(userPayouts.getPayoutVersion());
		userPayOutMappingBO.setActive(userPayouts.getIsActive());
		
		if (CommonUtils.isObjectNullOrEmpty(userPayouts.getCreatedDate())) {
			userPayOutMappingBO.setCreatedDate(userPayouts.getCreatedDate());
		}
		
		userPayOutMappingBO.setCreatedBy(userPayouts.getCreatedBy());
		
		if (CommonUtils.isObjectNullOrEmpty(userPayouts.getModifiedDate())) {
			userPayOutMappingBO.setModifiedDate(userPayouts.getModifiedDate());
		}
		userPayOutMappingBO.setModifiedBy(userPayouts.getModifiedBy());
		//userPayOutMappingBO.setChannelPartnerId(userPayouts.getChannelPartnerId());

		return userPayOutMappingBO;
	}

	@Override
	public List<UserPayOutMappingBO> getCustomPayoutsByUserIdAndApplId(Long userId, Long applTypeId) {
		List<UserPayOutMappingBO> listBO = new ArrayList<UserPayOutMappingBO>();
		List<UserPayOutMapping> dbList = userPayOutMappingRepository.findByUserIdAndIsActiveAndApplTypeId(userId, true, applTypeId);
		for(UserPayOutMapping userMapping : dbList) {
			listBO.add(convertDomainToBO(userMapping));
		}
		return listBO;
	}
}

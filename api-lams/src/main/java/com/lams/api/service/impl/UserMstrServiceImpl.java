package com.lams.api.service.impl;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import com.lams.api.domain.PreApplications;
import com.lams.api.domain.User;
import com.lams.api.domain.UserCoapplicant;
import com.lams.api.domain.master.AddressMstr;
import com.lams.api.domain.master.ApplicationTypeMstr;
import com.lams.api.domain.master.BankDetails;
import com.lams.api.domain.master.BankMstr;
import com.lams.api.domain.master.BusinessTypeMstr;
import com.lams.api.domain.master.CityMstr;
import com.lams.api.domain.master.CountryMstr;
import com.lams.api.domain.master.StateMstr;
import com.lams.api.repository.CoApplicantsRepository;
import com.lams.api.repository.LenderApplicationMappingRepository;
import com.lams.api.repository.PreApplicationsRepository;
import com.lams.api.repository.UserMstrRepository;
import com.lams.api.repository.master.AddressMstrRepository;
import com.lams.api.repository.master.BankDetailsRepository;
import com.lams.api.service.ApplicationsService;
import com.lams.api.service.CoApplicantsService;
import com.lams.api.service.LenderApplicationMappingService;
import com.lams.api.service.NotificationService;
import com.lams.api.service.OTPLoggingService;
import com.lams.api.service.PreApplicationsService;
import com.lams.api.service.UserMstrService;
import com.lams.api.service.master.AddressService;
import com.lams.api.service.master.BankDetailsService;
import com.lams.api.utils.AddressJDBCImpl;
import com.lams.api.utils.ServiceUtils;
import com.lams.model.bo.AddressBO;
import com.lams.model.bo.ApplicationsBO;
import com.lams.model.bo.BankBO;
import com.lams.model.bo.BankDetailsBO;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.NotificationBO;
import com.lams.model.bo.NotificationMainBO;
import com.lams.model.bo.NotificationResponse;
import com.lams.model.bo.OTPRequest;
import com.lams.model.bo.PreApplicationsBO;
import com.lams.model.bo.UserBO;
import com.lams.model.bo.UserCoApplicantBO;
import com.lams.model.bo.master.BusinessTypeBO;
import com.lams.model.bo.master.CityBO;
import com.lams.model.bo.master.CountryBO;
import com.lams.model.bo.master.StateBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.Enums;
import com.lams.model.utils.Enums.ContentType;
import com.lams.model.utils.Enums.NotificationType;
import com.lams.model.utils.Enums.OTPType;
import com.lams.model.utils.Enums.UserType;
import com.lams.model.utils.NotificationAlias;

@Service
@Transactional
public class UserMstrServiceImpl implements UserMstrService {

	public final static Logger logger = Logger.getLogger(UserMstrServiceImpl.class.getName());

	@Autowired
	private UserMstrRepository userMstrRepository;

	@Autowired
	private AddressService addressService;
	
	@Autowired
	private AddressJDBCImpl addressJDBCImpl;

	@Autowired
	private AddressMstrRepository addressMstrRepository;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private LenderApplicationMappingService lenderApplicationMappingService;

	@Autowired
	private LenderApplicationMappingRepository lenderApplicationMappingRepository;

	@Autowired
	private ApplicationsService applicationsService;
	
	@Autowired
	private PreApplicationsService preApplicationsService;
	
	@Autowired
	private PreApplicationsRepository preApplicationsRepository;
	
	@Autowired
	private CoApplicantsService coApplicantsService;
	
	@Autowired
	private CoApplicantsRepository coApplicantsRepository;

	@Autowired
	private OTPLoggingService oTPLoggingService;
	
	@Autowired
	private BankDetailsRepository bankDetailsRepository;
	
	@Autowired
	private BankDetailsService bankDetailsService;
	
	@Value("${com.lams.login.url}")
	private String loginUrl;

	@Value("${com.lams.email.base.url}")
	private String baseUrl;

	@Override
	public LamsResponse registration(UserBO userBO, Long userId) {

		User user = null;
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			user = userMstrRepository.findOne(userBO.getId());
		}
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			// CHECK IF EMAIL IS EXIST
			if (userMstrRepository.checkEmail(userBO.getEmail()) > 0) {
				logger.info("Email is already exist ------------------------->" + userBO.getEmail());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Already Exist");
			}

			// CHECK IF MOBILE IS EXIST
			if (userMstrRepository.checkMobile(userBO.getMobile()) > 0) {
				logger.info("Mobile is already exist ------------------------->" + userBO.getMobile());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Already Exist");
			}
			user = new User();
			user.setCreatedDate(new Date());
			user.setCreatedBy(userId);
			user.setIsActive(true);
			user.setInvitationCount(0);
		} else {
			if (Enums.UserType.LENDER.getId() != userBO.getUserType()) {
				if (!user.getIsActive()) {
					logger.info("Current User is Inactive ------------------------->" + userBO.getMobile());
					return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "You are not active user.");
				}
			}
			// CHECK IF EMAIL IS EXIST
			if (userMstrRepository.checkEmailById(userBO.getEmail(), user.getId()) > 0) {
				logger.info("Email is already exist ------------------------->" + userBO.getEmail());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Already Exist");
			}

			// CHECK IF MOBILE IS EXIST
			if (userMstrRepository.checkMobileById(userBO.getMobile(), user.getId()) > 0) {
				logger.info("Mobile is already exist ------------------------->" + userBO.getMobile());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Already Exist");
			}
			user.setModifiedDate(new Date());
			user.setModifiedBy(userId);
		}
		BeanUtils.copyProperties(userBO, user, "id", "isActive", "createdDate", "createdBy", "invitationCount");
		user.setIsAcceptTermCondition(true);
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getUserType())) {
			UserType userType = Enums.UserType.getType(userBO.getUserType().intValue());
			if (!CommonUtils.isObjectNullOrEmpty(userType) && userType.equals(Enums.UserType.LENDER)) {
				user.setIsEmailVerified(true);
				user.setIsOtpVerified(true);
				user.setIsProfileFilled(true);
				if (CommonUtils.isObjectNullOrEmpty(userBO.getIsActive())) {
					user.setIsActive(false);
				} else {
					user.setIsActive(userBO.getIsActive());
				}

				if (!CommonUtils.isObjectNullOrEmpty(userBO.getBank())
						&& !CommonUtils.isObjectNullOrEmpty(userBO.getBank().getId())) {
					user.setBank(new BankMstr(userBO.getBank().getId()));
				}
			} else if (!CommonUtils.isObjectNullOrEmpty(userType)
					&& (userType.equals(Enums.UserType.BORROWER) || userType.equals(Enums.UserType.CHANNEL_PARTNER))) {
				user.setIsEmailVerified(false);
				user.setIsOtpVerified(false);
				user.setIsProfileFilled(false);
			} else if (!CommonUtils.isObjectNullOrEmpty(userType) && userType.equals(Enums.UserType.ADMIN) ) {
				user.setIsActive(userBO.getIsActive());
			} else if (!CommonUtils.isObjectNullOrEmpty(userType) && userType.equals(Enums.UserType.PD_AGENCY) ) {
				user.setIsActive(userBO.getIsActive());
			} else if (!CommonUtils.isObjectNullOrEmpty(userType) && userType.equals(Enums.UserType.FCI_AGENCY) ) {
				user.setIsActive(userBO.getIsActive());
			}
		}
		user.setPassword(DigestUtils.md5DigestAsHex(userBO.getPassword().getBytes()).toString());
		user.setTempPassword(userBO.getPassword());
		user = userMstrRepository.save(user);

		String msg = "Successful Registration";
		
		if (!CommonUtils.isObjectNullOrEmpty(Enums.UserType.getType(userBO.getUserType().intValue())) && Enums.UserType.getType(userBO.getUserType().intValue()).equals(Enums.UserType.PD_AGENCY)) {
			// Adding Mapping
			if (!CommonUtils.isListNullOrEmpty(userBO.getApplications())) {
				lenderApplicationMappingRepository.inActiveByUserId(user.getId(), userId);
				for (ApplicationsBO appBo : userBO.getApplications()) {
					lenderApplicationMappingService.save(appBo.getApplicationTypeId(), user.getId(), userId);
				}
			}
		}
		
		if (!CommonUtils.isObjectNullOrEmpty(Enums.UserType.getType(userBO.getUserType().intValue())) && !Enums.UserType.getType(userBO.getUserType().intValue()).equals(Enums.UserType.ADMIN)) {
			// Adding Mapping
			if (!CommonUtils.isListNullOrEmpty(userBO.getApplications())) {
				lenderApplicationMappingRepository.inActiveByUserId(user.getId(), userId);
				for (ApplicationsBO appBo : userBO.getApplications()) {
					lenderApplicationMappingService.save(appBo.getApplicationTypeId(), user.getId(), userId);
				}
			}

			// Sending OTP to Registered Email
			boolean isMailSend = false;
			boolean sendOtp = false;
			if (!CommonUtils.isObjectNullOrEmpty(user.getUserType())
					&& (Enums.UserType.BORROWER.getId() == user.getUserType().intValue()
							|| Enums.UserType.CHANNEL_PARTNER.getId() == user.getUserType().intValue())) {
				logger.log(Level.INFO, "Is Otp Sent===>{0}", sendOtp);

				try {
					sendOtp = sendOtp(user, OTPType.REGISTRATION, NotificationAlias.SMS);
					String subject = "VfinanceS – E Mail Verification";
					isMailSend = sendLinkOnMail(user, NotificationAlias.EMAIL_VERIFY_ACCOUNT, subject,
							"email-verification");
					BeanUtils.copyProperties(user, userBO, "tempPassword", "password");
					userBO.setIsSent(isMailSend);
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("Error while Sending Mail");
				}

				if (Enums.UserType.CHANNEL_PARTNER.getId() == user.getUserType().intValue()) {
					Long codeCount = userMstrRepository.getCodeCount();
					logger.log(Level.INFO, "Total Channel Partener Users Count===>{0}", new Object[] { codeCount });
					String code = null;
					if (codeCount == null) {
						code = CommonUtils.generateCPCode(0l);
					} else {
						code = CommonUtils.generateCPCode(codeCount);
					}
					user.setCode(code);
				}
			}
			logger.info("Successfully registration --------EMAIL---> " + userBO.getEmail() + "---------ID----" + user.getId());

			if (sendOtp && isMailSend) {
				logger.log(Level.INFO, "OTP Sent For Mobile===>{0}=====Email=={1}",
						new Object[] { user.getMobile(), user.getEmail() });
				msg = msg + " We have sent OTP On " + user.getMobile() + " and We have sent Email Verification Link on " + user.getEmail() + ".";
				userBO.setId(user.getId());
			}else if (sendOtp) {
				msg = msg + " We have sent OTP On " + user.getMobile() + ".";
			} else if (isMailSend) {
				msg = msg + " We have sent Email Verification Link on " + user.getEmail() + ".";
			}
		}
		
		return new LamsResponse(HttpStatus.OK.value(), msg, userBO);
	}
	
	@Override
	public LamsResponse adminRegistration(UserBO userBO, Long userId) {

		User user = null;
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			user = userMstrRepository.findOne(userBO.getId());
		}
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			// CHECK IF EMAIL IS EXIST
			if (userMstrRepository.checkEmail(userBO.getEmail()) > 0) {
				logger.info("Email is already exist ------------------------->" + userBO.getEmail());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Already Exist");
			}

			// CHECK IF MOBILE IS EXIST
			if (userMstrRepository.checkMobile(userBO.getMobile()) > 0) {
				logger.info("Mobile is already exist ------------------------->" + userBO.getMobile());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Already Exist");
			}
			user = new User();
			user.setCreatedDate(new Date());
			user.setCreatedBy(userId);
			user.setIsActive(true);
			user.setInvitationCount(0);
		} else {
			if (Enums.UserType.ADMIN.getId() != userBO.getUserType()) {
				if (!user.getIsActive()) {
					logger.info("Current User is Inactive ------------------------->" + userBO.getMobile());
					return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "You are not active user.");
				}
			}
			// CHECK IF EMAIL IS EXIST
			if (userMstrRepository.checkEmailById(userBO.getEmail(), user.getId()) > 0) {
				logger.info("Email is already exist ------------------------->" + userBO.getEmail());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email is Already Exist");
			}

			// CHECK IF MOBILE IS EXIST
			if (userMstrRepository.checkMobileById(userBO.getMobile(), user.getId()) > 0) {
				logger.info("Mobile is already exist ------------------------->" + userBO.getMobile());
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Mobile is Already Exist");
			}
			user.setModifiedDate(new Date());
			user.setModifiedBy(userId);
		}
		BeanUtils.copyProperties(userBO, user, "id", "isActive", "createdDate", "createdBy", "invitationCount");
		user.setIsAcceptTermCondition(true);
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getUserType())) {
			UserType userType = Enums.UserType.getType(userBO.getUserType().intValue());
			if (!CommonUtils.isObjectNullOrEmpty(userType) && userType.equals(Enums.UserType.LENDER)) {
				user.setIsEmailVerified(true);
				user.setIsOtpVerified(true);
				user.setIsProfileFilled(true);
				if (CommonUtils.isObjectNullOrEmpty(userBO.getIsActive())) {
					user.setIsActive(false);
				} else {
					user.setIsActive(userBO.getIsActive());
				}

				if (!CommonUtils.isObjectNullOrEmpty(userBO.getBank())
						&& !CommonUtils.isObjectNullOrEmpty(userBO.getBank().getId())) {
					user.setBank(new BankMstr(userBO.getBank().getId()));
				}
			} else if (!CommonUtils.isObjectNullOrEmpty(userType)
					&& (userType.equals(Enums.UserType.BORROWER) || userType.equals(Enums.UserType.CHANNEL_PARTNER))) {
				user.setIsEmailVerified(false);
				user.setIsOtpVerified(false);
				user.setIsProfileFilled(false);
			} else if (!CommonUtils.isObjectNullOrEmpty(userType) && userType.equals(Enums.UserType.ADMIN) ) {
				user.setIsActive(userBO.getIsActive());
			}
		}
		user.setPassword(DigestUtils.md5DigestAsHex(userBO.getPassword().getBytes()).toString());
		user.setTempPassword(userBO.getPassword());
		user = userMstrRepository.save(user);

		String msg = "Successful Registration";
		
		if (!CommonUtils.isObjectNullOrEmpty(Enums.UserType.getType(userBO.getUserType().intValue())) && !Enums.UserType.getType(userBO.getUserType().intValue()).equals(Enums.UserType.ADMIN)) {
			// Adding Mapping
			if (!CommonUtils.isListNullOrEmpty(userBO.getApplications())) {
				lenderApplicationMappingRepository.inActiveByUserId(user.getId(), userId);
				for (ApplicationsBO appBo : userBO.getApplications()) {
					lenderApplicationMappingService.save(appBo.getApplicationTypeId(), user.getId(), userId);
				}
			}

			// Sending OTP to Registered Email
			boolean isMailSend = false;
			boolean sendOtp = false;
			if (!CommonUtils.isObjectNullOrEmpty(user.getUserType())
					&& (Enums.UserType.BORROWER.getId() == user.getUserType().intValue()
							|| Enums.UserType.CHANNEL_PARTNER.getId() == user.getUserType().intValue())) {
				logger.log(Level.INFO, "Is Otp Sent===>{0}", sendOtp);

				try {
					sendOtp = sendOtp(user, OTPType.REGISTRATION, NotificationAlias.SMS);
					String subject = "VfinanceS – E Mail Verification";
					isMailSend = sendLinkOnMail(user, NotificationAlias.EMAIL_VERIFY_ACCOUNT, subject,
							"email-verification");
					BeanUtils.copyProperties(user, userBO, "tempPassword", "password");
					userBO.setIsSent(isMailSend);
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("Error while Sending Mail");
				}

				if (Enums.UserType.CHANNEL_PARTNER.getId() == user.getUserType().intValue()) {
					Long codeCount = userMstrRepository.getCodeCount();
					logger.log(Level.INFO, "Total Channel Partener Users Count===>{0}", new Object[] { codeCount });
					String code = null;
					if (codeCount == null) {
						code = CommonUtils.generateCPCode(0l);
					} else {
						code = CommonUtils.generateCPCode(codeCount);
					}
					user.setCode(code);
				}
			}
			logger.info("Successfully registration --------EMAIL---> " + userBO.getEmail() + "---------ID----" + user.getId());

			if (sendOtp && isMailSend) {
				logger.log(Level.INFO, "OTP Sent For Mobile===>{0}=====Email=={1}",
						new Object[] { user.getMobile(), user.getEmail() });
				msg = msg + " We have sent OTP On " + user.getMobile() + " and We have sent Email Verification Link on " + user.getEmail() + ".";
				userBO.setId(user.getId());
			}else if (sendOtp) {
				msg = msg + " We have sent OTP On " + user.getMobile() + ".";
			} else if (isMailSend) {
				msg = msg + " We have sent Email Verification Link on " + user.getEmail() + ".";
			}
		}
		
		return new LamsResponse(HttpStatus.OK.value(), msg, userBO);
	}

	@Override
	public List<UserBO> getUsersByUserType(Long userType) {
		List<User> userList = null;
		if (CommonUtils.isObjectNullOrEmpty(userType) || userType == -1) {
			userList = userMstrRepository.findAll();
		} else {
			userList = userMstrRepository.findByUserType(userType);
		}

		List<UserBO> userBOList = new ArrayList<>(userList.size());
		UserBO userBo = null;
		for (User user : userList) {
			userBo = new UserBO();
			BeanUtils.copyProperties(user, userBo, "password");
			if (!CommonUtils.isObjectNullOrEmpty(user.getBank())) {
				BankBO bankBO = new BankBO();
				BeanUtils.copyProperties(user.getBank(), bankBO);
				userBo.setBank(bankBO);
			}
			if (user.getUserType() == Enums.UserType.BORROWER.getId()) {
				// Set Borrower Applications
				userBo.setApplications(applicationsService.getAll(user.getId()));
			} else if ((user.getUserType() == Enums.UserType.LENDER.getId()) || (user.getUserType() == Enums.UserType.PD_AGENCY.getId())) {
				// Set Lender Applications
				List<ApplicationTypeMstr> list = lenderApplicationMappingRepository
						.getApplicationByUserIdAndIsActive(user.getId(), true);
				List<ApplicationsBO> apps = new ArrayList<>(list.size());
				for (ApplicationTypeMstr mstr : list) {
					ApplicationsBO bo = new ApplicationsBO();
					bo.setApplicationTypeId(mstr.getId());
					bo.setApplicationTypeName(mstr.getName());
					apps.add(bo);
				}
				userBo.setApplications(apps);
			}

			userBOList.add(userBo);
		}
		return userBOList;
	}
	
	
	@Override
	public UserBO getUserBasicDetails(Long userId) {
		User user = userMstrRepository.findOne(userId);
		UserBO userBo = new UserBO();
		if(!CommonUtils.isObjectNullOrEmpty(user)) {
			userBo.setFirstName(user.getFirstName());
			userBo.setMiddleName(user.getMiddleName());
			userBo.setLastName(user.getLastName());
			userBo.setUserType(user.getUserType());
			userBo.setEmail(user.getEmail());
			userBo.setMobile(user.getMobile());
		}
		return userBo;
	}
	

	@Override
	public UserBO getUserById(Long id) {
		logger.log(Level.INFO," input id : "+id);
		User user = userMstrRepository.findOne(id);
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", id);
			return null;
		}
		UserBO userBo = new UserBO();
		BeanUtils.copyProperties(user, userBo, "password", "tempPassword");
		if (user.getUserType() != null && (user.getUserType() == Enums.UserType.BORROWER.getId()
				|| user.getUserType() == Enums.UserType.CHANNEL_PARTNER.getId())) {
			List<AddressMstr> addressMstrList = addressMstrRepository.findByUserIdAndIsActive(user.getId(), true);
			for (AddressMstr addressMstr : addressMstrList) {
				AddressBO addressBO = new AddressBO();
				BeanUtils.copyProperties(addressMstr, addressBO);
				// Copy City
				CityMstr cityMstr = addressMstr.getCity();
				if (!CommonUtils.isObjectNullOrEmpty(cityMstr)) {
					CityBO cityBO = new CityBO();
					BeanUtils.copyProperties(cityMstr, cityBO);
					addressBO.setCity(cityBO);

					// Copy State
					StateMstr state = cityMstr.getState();
					if (!CommonUtils.isObjectNullOrEmpty(state)) {
						StateBO stateBO = new StateBO();
						BeanUtils.copyProperties(state, stateBO);
						addressBO.setState(stateBO);

						// Copy Country
						CountryMstr country = state.getCountry();
						if (!CommonUtils.isObjectNullOrEmpty(country)) {
							CountryBO coutryBO = new CountryBO();
							BeanUtils.copyProperties(country, coutryBO);
							addressBO.setCountry(coutryBO);
						}
					}
				}
				if (addressMstr.getAddType() == CommonUtils.AddressType.PERMANENT) {
					userBo.setPermanentAdd(addressBO);
				} else if (addressMstr.getAddType() == CommonUtils.AddressType.COMMUNICATION) {
					userBo.setCommunicationAdd(addressBO);
				} else if (addressMstr.getAddType() == CommonUtils.AddressType.EMPLOYMENT_ADD) {
					userBo.setEmploymentAddress(addressBO);
				}  
			}
			BusinessTypeMstr businessTypeMstr = user.getBusinessTypeMstr();
			if (!CommonUtils.isObjectNullOrEmpty(businessTypeMstr)) {
				BusinessTypeBO bo = new BusinessTypeBO();
				BeanUtils.copyProperties(businessTypeMstr, bo);
				userBo.setBusinessType(bo);
			}
			userBo.setApplications(applicationsService.getAll(user.getId()));
			userBo.setUserCoapplicants(coApplicantsService.getAllCoApplicants(user.getId()));
			
			BankDetails bankDetails = bankDetailsRepository.findByUserId(user.getId());
			BankDetailsBO bankDetailsBO = new BankDetailsBO();
			if(bankDetails!=null){
				BeanUtils.copyProperties(bankDetails, bankDetailsBO);
				userBo.setBankDetails(bankDetailsBO);
			}
			
			
		} else if (user.getUserType() != null && user.getUserType() == Enums.UserType.LENDER.getId()) {
			// Set Lender Applications
			List<ApplicationTypeMstr> list = lenderApplicationMappingRepository
					.getApplicationByUserIdAndIsActive(user.getId(), true);
			List<ApplicationsBO> apps = new ArrayList<>(list.size());
			for (ApplicationTypeMstr mstr : list) {
				ApplicationsBO bo = new ApplicationsBO();
				bo.setApplicationTypeId(mstr.getId());
				bo.setApplicationTypeName(mstr.getName());
				apps.add(bo);
			}
			if (!CommonUtils.isObjectNullOrEmpty(user.getBank())) {
				BankBO bankBO = new BankBO();
				BeanUtils.copyProperties(user.getBank(), bankBO);
				userBo.setBank(bankBO);
			}
			userBo.setApplications(apps);
		}
		return userBo;
	}

	@Override
	public LamsResponse addCpBorrowerOld(UserBO userBO, Long userId) {
		logger.log(Level.INFO, "Enter in addCpBorrower");
		// CHECK IF EMAIL IS EXIST
		Boolean isUserFound=false;
		
		if(CommonUtils.isObjectNullOrEmpty(userBO.getId())) {
			if (userMstrRepository.checkEmail(userBO.getEmail()) > 0) {
				logger.info("Email is already exist ------------------------->" + userBO.getEmail());
				isUserFound=true;
			}

			// CHECK IF MOBILE IS EXIST
			if (userMstrRepository.checkMobile(userBO.getMobile()) > 0) {
				logger.info("Mobile is already exist ------------------------->" + userBO.getMobile());
				isUserFound=true;
			}			
		}else {
			if (userMstrRepository.checkEmailById(userBO.getEmail(),userBO.getId()) > 0) {
				logger.info("Email is already exist ------------------------->" + userBO.getEmail());
				isUserFound=true;
			}

			// CHECK IF MOBILE IS EXIST
			if (userMstrRepository.checkEmailById(userBO.getMobile(),userBO.getId()) > 0) {
				logger.info("Mobile is already exist ------------------------->" + userBO.getMobile());
				isUserFound=true;
			}
		}
		
		//Saving User Profile
		User user = userMstrRepository.findByEmailAndIsActive(userBO.getEmail(), true);
		if(!CommonUtils.isObjectNullOrEmpty(user)) {
			//user = userMstrRepository.findOne(userBO.getId());
			user.setModifiedBy(userId);
			user.setModifiedDate(new Date());
			if(CommonUtils.isObjectNullOrEmpty(user)) {
				logger.log(Level.INFO, "No User Detail Found for Given User Id. Something goes Wrong===={0}", new Object[] {userId});
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "No User Details Found for UserId");
			}
		}else {
			user = new User();
			user.setIsOtpVerified(true);
			user.setIsEmailVerified(true);
			user.setIsAcceptTermCondition(true);
			user.setIsActive(true);
			user.setChannelPartnerId(new User(userId));
			user.setUserType(userBO.getUserType());
			user.setCreatedDate(new Date());
			String tempPassword = ServiceUtils.generateOTP(10, CommonUtils.CHAR_LIST);
			user.setTempPassword(tempPassword);
			user.setPassword(DigestUtils.md5DigestAsHex(tempPassword.getBytes()));
			isUserFound=false;
		}
		
		if(!isUserFound) {
			//Create or Update User (Borrower)
			user.setEmail(userBO.getEmail());
			user.setMobile(userBO.getMobile());
			user.setName(userBO.getName());
			user.setSalutation(userBO.getSalutation());
			user = userMstrRepository.save(user);
			
			try {
				String subject = "VfinanceS – E Mail Verification";
				user.setChannelPartnerId(userMstrRepository.findOne(userId));
				sendLinkOnMail(user, NotificationAlias.EMAIL_EMAIL_CHANNELPARTNER_REGISTER_BORROWER, subject, "email-verification");
			} catch (Exception e) {
				logger.log(Level.INFO, "Error Occurred while sending meila", new Object[]{e.getMessage()});
			}
		}

		//Inactive All Previous Loans
		int inActiveByUserId = applicationsService.inActiveByUserId(user.getId());
		logger.info("inActiveByUserId====>" + inActiveByUserId);
		//Create Or Update Application
		if(!CommonUtils.isListNullOrEmpty(userBO.getPreApplications())) {
			for(PreApplicationsBO bo : userBO.getPreApplications()) {
				PreApplicationsBO preApplicationsBO = new PreApplicationsBO();
				preApplicationsBO.setId(bo.getId());
				preApplicationsBO.setApplicationTypeCode(bo.getApplicationTypeCode());
				preApplicationsBO.setApplicationTypeId(bo.getApplicationTypeId());
				preApplicationsBO.setLoanAmount(bo.getLoanAmount());
				preApplicationsService.saveFromCP(preApplicationsBO,user.getId(),userMstrRepository.findOne(userId));
			}
		}else {
			logger.log(Level.INFO, "No Application Selected by Channel Partner User =====>{0}", new Object[]{userId});
		}
		BeanUtils.copyProperties(user, userBO);
		//userBO.setPreApplications(preApplicationsService.getAllActivePreApplicationByUserIdAndChannelPartnerId(brUserId, channelPartnerId));
		logger.log(Level.INFO, "Exit in addCpBorrower");
		return new LamsResponse(HttpStatus.OK.value(),"Success",userBO);
	}
	
	@Override
	public LamsResponse addCpBorrower(PreApplicationsBO preApplicationsBO, Long userId) {
		logger.log(Level.INFO, "Enter in addCpBorrower");
		
		PreApplications preApplication = preApplicationsRepository.findOne(preApplicationsBO.getId());
		
		User user = userMstrRepository.findOne(preApplication.getUserId());

		ApplicationsBO applicationBO = new ApplicationsBO();
		applicationBO.setApplicationTypeId(preApplication.getApplicationTypeId().getId());
		applicationBO.setLoanAmount(preApplication.getLoanAmount());
		applicationBO.setLoanCategoryId(preApplication.getLoanTypeId().getId());
		applicationBO.setUserId(preApplication.getUserId());
		applicationBO.setPreApplicationId(preApplication.getId());
		
		applicationsService.saveFromCP(preApplication.getId(), applicationBO, preApplication.getUserId(),preApplication.getChannelPartnerId(), userMstrRepository.findOne(preApplication.getChannelPartnerId()).getCode());
		
		BeanUtils.copyProperties(user, preApplicationsBO);
		logger.log(Level.INFO, "Exit in addCpBorrower");
		return new LamsResponse(HttpStatus.OK.value(),"Success", preApplicationsService.getAllActivePreApplicationByUserId(userId));
	}

	@Override
	public LamsResponse getCpUsersByUserType(Long cpUserId,Long userType) {
		List<UserBO> userBOs = preApplicationsService.getAllActivePreApplicationByChannelPartnerId(cpUserId);
			
		if(CommonUtils.isListNullOrEmpty(userBOs)) {
			return new LamsResponse(HttpStatus.OK.value(), "No Borrower Added Yet !",Collections.emptyList());
		}
		
		return new LamsResponse(HttpStatus.OK.value(), "Data Found",userBOs);
	}

	@Override
	public LamsResponse updateUserDetails(UserBO userBO) {
		User user = userMstrRepository.findOne(userBO.getId());
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", userBO.getId());
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "User not Found");
		}
		if (user.getUserType() == Enums.UserType.LENDER.getId()) {
			user.setFirstName(userBO.getFirstName());
			user.setMiddleName(userBO.getMiddleName());
			user.setLastName(userBO.getLastName());
			user.setSalutation(userBO.getSalutation());
			user.setIsProfileFilled(true);

		} else if (user.getUserType() == Enums.UserType.BORROWER.getId()
				|| user.getUserType() == Enums.UserType.CHANNEL_PARTNER.getId()) {
			BeanUtils.copyProperties(userBO, user, "password", "tempPassword", "email", "mobile","userType","isActive");
			if (!CommonUtils.isObjectNullOrEmpty(userBO.getPermanentAdd())) {
				addressService.saveAddress(userBO.getPermanentAdd(), userBO.getId(), CommonUtils.AddressType.PERMANENT);
			}
			if (!CommonUtils.isObjectNullOrEmpty(userBO.getCommunicationAdd())) {
				addressService.saveAddress(userBO.getCommunicationAdd(), userBO.getId(),
						CommonUtils.AddressType.COMMUNICATION);
			}
			if (!CommonUtils.isObjectNullOrEmpty(userBO.getEmploymentAddress())) {
				addressService.saveAddress(userBO.getEmploymentAddress(), userBO.getId(),
						CommonUtils.AddressType.EMPLOYMENT_ADD);
			}
			if (!CommonUtils.isObjectNullOrEmpty(userBO.getBusinessType())
					&& !CommonUtils.isObjectNullOrEmpty(userBO.getBusinessType().getId())) {
				user.setBusinessTypeMstr(new BusinessTypeMstr(userBO.getBusinessType().getId()));
			}
			// Updating already Existing Applications code
			if (CommonUtils.isObjectNullOrEmpty(userBO.getCode())
					&& Enums.UserType.CHANNEL_PARTNER.getId() == user.getUserType().intValue()) {
				Long codeCount = userMstrRepository.getCodeCount();
				logger.log(Level.INFO, "Total Channel Partener Users Count===>{0}", new Object[] { codeCount });
				String code = null;
				if (codeCount == null) {
					code = CommonUtils.generateCPCode(0l);
				} else {
					code = CommonUtils.generateCPCode(codeCount);
				}
				user.setCode(code);
			}
			
			if(Enums.UserType.CHANNEL_PARTNER.getId() == user.getUserType().intValue()){
				userBO.getBankDetails().setUserId(userBO.getId());
				bankDetailsService.saveBankDetails(userBO.getBankDetails(), userBO.getId());
			}
		}
		user.setModifiedDate(new Date());
		user.setModifiedBy(userBO.getId());
		user = userMstrRepository.save(user);
		// Setting Address
		logger.log(Level.INFO, "Successfully updated User Details for ID----" + user.getId());
		return new LamsResponse(HttpStatus.OK.value(), "Success", getUserById(user.getId()));
	}
	
	@Override
	public LamsResponse updateUserCoApplicantDetails(UserBO userBO, Long coApplicantUserId) {
		User user = userMstrRepository.findOne(userBO.getId());
		UserCoapplicant uc = new UserCoapplicant();
		
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", userBO.getId());
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "User not Found");
		}
		
		uc.setId(coApplicantUserId);
		uc.setFirstName(userBO.getFirstName());
		uc.setMiddleName(userBO.getMiddleName());
		uc.setLastName(userBO.getLastName());
		uc.setSalutation(userBO.getSalutation());
		uc.setIsProfileFilled(true);
	
		BeanUtils.copyProperties(userBO, uc, "password", "tempPassword", "email", "mobile", "userType", "isActive");
		
		//Setting CoApplicant user id to updated appropriate addresses
		userBO.setId(coApplicantUserId);
				
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getPermanentAdd())) {
			addressJDBCImpl.updateAddress(userBO.getPermanentAdd(), user.getId(), CommonUtils.AddressType.PERMANENT);
		}
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getCommunicationAdd())) {
			addressJDBCImpl.updateAddress(userBO.getCommunicationAdd(), user.getId(),
					CommonUtils.AddressType.COMMUNICATION);
		}
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getEmploymentAddress())) {
			addressJDBCImpl.updateAddress(userBO.getEmploymentAddress(), user.getId(),
					CommonUtils.AddressType.EMPLOYMENT_ADD);
		}
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getBusinessType())
				&& !CommonUtils.isObjectNullOrEmpty(userBO.getBusinessType().getId())) {
			uc.setBusinessTypeMstr(new BusinessTypeMstr(userBO.getBusinessType().getId()));
		}

		//uc.setModifiedDate(new Date());
		//uc.setModifiedBy(userBO.getId());
		uc = coApplicantsRepository.save(uc);
		// Setting Address
		logger.log(Level.INFO, "Successfully updated User Details for ID----" + user.getId());
		return new LamsResponse(HttpStatus.OK.value(), "Success", getUserById(user.getId()));
	}

	@Override
	public UserBO inviteLender(UserBO userBO, Long userId) throws Exception {
		logger.log(Level.INFO, "inviteLender==>{0}", userBO.toString());
		NotificationBO notificationBO = new NotificationBO();
		notificationBO.setClientRefId(String.valueOf(1l));
		List<NotificationMainBO> mainBolist = new ArrayList<>();
		NotificationMainBO mainBO = new NotificationMainBO();
		String to[] = { userBO.getEmail() };
		mainBO.setTo(to);
		mainBO.setContentType(ContentType.TEMPLATE);
		mainBO.setType(NotificationType.EMAIL);
		mainBO.setTemplateName(NotificationAlias.EMAIL_LENDER_INVITATION);
		Map<String, Object> data = new HashMap<>();
		data.put("title", "Hi," + userBO.getFirstName() + " " + userBO.getLastName());
		data.put("userName", userBO.getEmail());
		data.put("password", userBO.getTempPassword());
		data.put("loginUrl", loginUrl);
		mainBO.setParameters(data);
		mainBO.setSubject("VfinanceS Registrations");
		mainBolist.add(mainBO);
		notificationBO.setNotifications(mainBolist);
		notificationService.sendNotification(notificationBO);

		// Increasing Invitation Count
		User user = userMstrRepository.findByEmailAndIsActive(userBO.getEmail(), true);
		user.setInvitationCount(user.getInvitationCount() + 1);
		user.setModifiedBy(userId);
		user.setModifiedDate(new Date());
		user = userMstrRepository.save(user);

		// Setting updated Fields
		userBO.setModifiedBy(userId);
		userBO.setInvitationCount(user.getInvitationCount());
		userBO.setModifiedDate(user.getModifiedDate());
		return userBO;
	}

	@Override
	public boolean sendOtp(User user, OTPType type, String templateName) {
		OTPRequest otpRequest = new OTPRequest();
		otpRequest.setMasterId(user.getId());
		otpRequest.setEmailId(user.getEmail());
		otpRequest.setMobileNo(user.getMobile());
		otpRequest.setRequestType(type.getId());
		otpRequest.setTemplateName(templateName);
		return oTPLoggingService.sendOTP(otpRequest);
	}

	@Override
	public LamsResponse verifyOTP(UserBO userBO, OTPType type) throws ParseException {
		User user = userMstrRepository.findOne(userBO.getId());
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST);
		}

		OTPRequest otpRequest = new OTPRequest();
		otpRequest.setEmailId(user.getEmail());
		otpRequest.setMasterId(user.getId());
		otpRequest.setMobileNo(user.getMobile());
		otpRequest.setRequestType(type.getId());
		otpRequest.setOtp(userBO.getOtp());
		boolean isVerified = oTPLoggingService.verifyOTP(otpRequest);
		user.setIsOtpVerified(isVerified);
		userMstrRepository.save(user);
		if (isVerified) {
			return new LamsResponse(HttpStatus.OK.value(), "Mobile No " + user.getMobile() + " SuccessFully Verified.");
		}
		return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid or Expired OTP.");
	}

	@Override
	public LamsResponse resendOtp(UserBO userBO, OTPType type, String templateName) {
		User user = userMstrRepository.findOne(userBO.getId());
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.INVALID_REQUEST);
		}
		if (!templateName.contains("ftl")) {
			templateName = templateName.concat(".ftl");
		}
		boolean sendOtp = sendOtp(user, type, templateName);
		String msg = null;
		if (sendOtp) {
			msg = "Otp Successfully Sent on " + user.getMobile();
			return new LamsResponse(HttpStatus.OK.value(), msg);
		}
		msg = CommonUtils.SOMETHING_WENT_WRONG + " Please try again after Sometime!";
		return new LamsResponse(HttpStatus.BAD_REQUEST.value(), msg);
	}

	@Override
	public LamsResponse changePassword(UserBO userBO) {
		logger.log(Level.INFO, "userBO===>", userBO.toString());
		User user = userMstrRepository.findOne(userBO.getId());
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.WARNING, "Invalid User Id while Getting User by Id ==>{0}", userBO.getId());
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid UserId. Please try to Relogin!");
		}
		String currPass = DigestUtils.md5DigestAsHex(userBO.getTempPassword().getBytes()).toString();
		if (!user.getPassword().equals(currPass)) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(),
					"Current Password does not Match with the Database Record.");
		}

		String newPass = DigestUtils.md5DigestAsHex(userBO.getPassword().getBytes()).toString();
		if (currPass.equals(newPass)) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(),
					"Current Password And New Password Must not be Same!");
		}
		user.setTempPassword(userBO.getPassword());
		user.setPassword(newPass);
		user = userMstrRepository.save(user);
		BeanUtils.copyProperties(user, userBO, "password", "tempPassword");
		return new LamsResponse(HttpStatus.OK.value(), "Password Successfully Updated", userBO);
	}

	@Override
	public String generateEncryptString(Date signUp, String email) {
		logger.info("generateEncryptString=================>" + signUp);
		String signUpDate = null;
		SimpleDateFormat print = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		signUpDate = print.format(signUp);
		try {
			String encEmail = encrypt(email);
			String encDate = encrypt(signUpDate);
			return encEmail + "|" + encDate;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error while encrypt url for email verification");
			e.printStackTrace();
			return null;
		}
	}

	private String encrypt(String data) throws UnsupportedEncodingException {
		return Base64.getEncoder().encodeToString(data.getBytes("utf-8"));
	}

	private String decrypt(String data) throws UnsupportedEncodingException {
		return new String(Base64.getDecoder().decode(data), "utf-8");
	}

	@Override
	public boolean sendLinkOnMail(User user, String template, String subject, String postUrl) throws Exception {
		logger.log(Level.INFO, "Send Email Verification OBject==>{0}", user.toString());
		NotificationBO notificationBO = new NotificationBO();
		notificationBO.setClientRefId(String.valueOf(1l));
		List<NotificationMainBO> mainBolist = new ArrayList<>();
		NotificationMainBO mainBO = new NotificationMainBO();
		String to[] = { user.getEmail() };
		mainBO.setTo(to);
		mainBO.setContentType(ContentType.TEMPLATE);
		mainBO.setType(NotificationType.EMAIL);
		mainBO.setTemplateName(template);
		Map<String, Object> data = new HashMap<>();
		data.put("user_name", user.getName());
		data.put("emailAddress", user.getEmail());
		data.put("temp_password", user.getTempPassword());
		if(user.getChannelPartnerId()!=null) {
			data.put("channelpartner_name", user.getChannelPartnerId().getFirstName()+" "+user.getChannelPartnerId().getLastName());
		}
		
		String encryptString = generateEncryptString(user.getCreatedDate(), user.getEmail());
		logger.log(Level.INFO, "encryptString===>{0}", new Object[] { encryptString });
		logger.log(Level.INFO, "VerificationURL===>{0}", baseUrl + encryptString);
		data.put("link", baseUrl + postUrl + "/" + encryptString);
		mainBO.setParameters(data);
		mainBO.setSubject(subject);
		mainBolist.add(mainBO);
		notificationBO.setNotifications(mainBolist);
		NotificationResponse response = notificationService.sendNotification(notificationBO);
		if (CommonUtils.isObjectNullOrEmpty(response)) {
			logger.log(Level.SEVERE, "Something went wrong while Sending Email Verification on === >", user.getEmail());
			return false;
		}
		logger.log(Level.INFO, "Response while Sending Email Verification Mail===>{0}",
				new Object[] { response.toString() });

		if (CommonUtils.NotificationProperty.STATUS_SUCCESSFULL.equals(response.getStatus())) {
			return true;
		}
		return false;
	}

	@Override
	public LamsResponse verifyEmail(String link) {
		logger.log(Level.INFO, "Link From Web===>{0}", new Object[] { link });
		String[] arr = link.toString().split("\\|");
		logger.log(Level.INFO, "arrarrarrarrarr====={0}", new Object[] { arr.length });
		if (arr == null || arr.length < 2) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Link. Please try Again!");
		}
		try {
			String decEmail = decrypt(arr[0]);
			logger.log(Level.INFO, "Descrypted Email=======>{0}", new Object[] { decEmail });
			User user = userMstrRepository.findByEmailAndIsActive(decEmail, true);
			if (CommonUtils.isObjectNullOrEmpty(user)) {
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Email Id");
			}
			if (!CommonUtils.isObjectNullOrEmpty(user.getIsEmailVerified()) && user.getIsEmailVerified()) {
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Email already Verified.");
			}
			SimpleDateFormat print = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String encDate = encrypt(print.format(user.getCreatedDate()));
			logger.log(Level.INFO, "New Encrypted Date String === {0}", new Object[] { encDate });
			logger.log(Level.INFO, "Old Encrypted Date String === {0}", new Object[] { arr[1] });
			if (!encDate.equals(arr[1])) {
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Link. Please try Again!");
			}
			user.setIsEmailVerified(true);
			userMstrRepository.save(user);
			return new LamsResponse(HttpStatus.OK.value(),
					"Your Email " + user.getEmail() + " Successfully Verified !");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Error while Descrypting Email Verificaiton Link");
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.SOMETHING_WENT_WRONG);
		}

	}

	@Override
	public LamsResponse sendForgotPasswordLink(String email) throws Exception {
		User user = userMstrRepository.findByEmailAndIsActive(email, true);
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Email Id");
		}
		String subject = "VfinanceS – Forgot Password";
		boolean onMail = sendLinkOnMail(user, NotificationAlias.FORGOT_PASSWORD_EMAIL, subject, "reset-password");
		if (onMail) {
			return new LamsResponse(HttpStatus.OK.value(),
					"We have Sent Link on " + user.getEmail() + " email to Reset Password.");
		}
		return new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.SOMETHING_WENT_WRONG);

	}

	@Override
	public LamsResponse resetPassword(UserBO userBO, String link) {
		logger.log(Level.INFO, "Link From Web===>{0}", new Object[] { link });
		String[] arr = link.toString().split("\\|");
		if (arr == null || arr.length < 2) {
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Link. Please try Again!");
		}
		try {
			String decEmail = decrypt(arr[0]);
			logger.log(Level.INFO, "Descrypted Email=======>{0}", new Object[] { decEmail });
			User user = userMstrRepository.findByEmailAndIsActive(decEmail, true);
			if (CommonUtils.isObjectNullOrEmpty(user)) {
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Email Id");
			}
			SimpleDateFormat print = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String encDate = encrypt(print.format(user.getCreatedDate()));
			logger.log(Level.INFO, "New Encrypted Date String === {0}", new Object[] { encDate });
			logger.log(Level.INFO, "Old Encrypted Date String === {0}", new Object[] { arr[1] });
			if (!encDate.equals(arr[1])) {
				return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "Invalid Link. Please try Again!");
			}
			user.setTempPassword(userBO.getPassword());
			user.setPassword(DigestUtils.md5DigestAsHex(userBO.getPassword().getBytes()).toString());
			userMstrRepository.save(user);
			return new LamsResponse(HttpStatus.OK.value(),
					"Your Email " + user.getEmail() + " Password Successfully updated !");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Error while Descrypting Email Verificaiton Link");
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), CommonUtils.SOMETHING_WENT_WRONG);
		}
	}
	
	@Override
	public List<UserBO> getLenderUsersByApplicationType(Long applicationType){
		List<User> userList = userMstrRepository.getLenderUsersByApplicationType(applicationType);
		List<UserBO> userBoList = new ArrayList<>(userList.size());
		UserBO userBo = null;
		for(User user : userList) {
			userBo = new UserBO();
			BeanUtils.copyProperties(user, userBo);
			userBoList.add(userBo);
		}
		return userBoList;
	}
	
	@Override
	public Boolean sendMailRequestFromSite(String email, String name, String msg, String contact) throws Exception {
		NotificationBO notificationBO = new NotificationBO();
		notificationBO.setClientRefId(String.valueOf(1l));
		List<NotificationMainBO> mainBolist = new ArrayList<>();
		NotificationMainBO mainBO = new NotificationMainBO();
		String to[] = { "info@vfinances.in" };
		mainBO.setTo(to);
		mainBO.setContentType(ContentType.TEMPLATE);
		mainBO.setType(NotificationType.EMAIL);
		mainBO.setTemplateName(NotificationAlias.QUERY_EMAIL_FROM_WEBSITE);
		Map<String, Object> data = new HashMap<>();
		data.put("name", name);
		data.put("email", email);
		data.put("msg", msg);
		data.put("contact", contact);
		mainBO.setParameters(data);
		mainBO.setSubject("New query from " + email);
		mainBolist.add(mainBO);
		notificationBO.setNotifications(mainBolist);
		NotificationResponse response = notificationService.sendNotification(notificationBO);
		if (CommonUtils.isObjectNullOrEmpty(response)) {
			logger.log(Level.SEVERE, "Something went wrong while Sending Email from Website on === >", email);
			return false;
		}
		logger.log(Level.INFO, "Response while Sending Email from Website Mail===>{0}",
				new Object[] { response.toString() });

		if (CommonUtils.NotificationProperty.STATUS_SUCCESSFULL.equals(response.getStatus())) {
			return true;
		}
		return false;
	}

	@Override
	public LamsResponse createCoApplicantProfile(UserBO userBO) {

		User user = userMstrRepository.findOne(userBO.getId());
		UserCoapplicant uc = new UserCoapplicant();
		uc.setParentUserId(user.getId());
		
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", userBO.getId());
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "User not Found");
		}
		
		BeanUtils.copyProperties(userBO, uc, "password", "tempPassword", "id", "email", "mobile", "userType", "isActive");
		UserCoapplicant newUc = coApplicantsRepository.save(uc);
		
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getPermanentAdd())) {
			addressService.saveAddress(userBO.getPermanentAdd(), newUc.getId(), CommonUtils.AddressType.PERMANENT);
		}
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getCommunicationAdd())) {
			addressService.saveAddress(userBO.getCommunicationAdd(), newUc.getId(), CommonUtils.AddressType.COMMUNICATION);
		}
		if (!CommonUtils.isObjectNullOrEmpty(userBO.getEmploymentAddress())) {
			addressService.saveAddress(userBO.getEmploymentAddress(), newUc.getId(), CommonUtils.AddressType.EMPLOYMENT_ADD);
		}
		
		UserCoApplicantBO newUCBO = new UserCoApplicantBO();
		newUCBO.setId(newUc.getId());
		newUCBO.setFirstName(newUc.getFirstName());
		newUCBO.setLastName(newUc.getLastName());
		newUCBO.setNameOfCoapplicant(user.getFirstName()+" "+user.getLastName());
		newUCBO.setParentUserId(newUc.getParentUserId());
		
		// Setting Address
		logger.log(Level.INFO, "Successfully updated User Details for ID----" + user.getId());
		return new LamsResponse(HttpStatus.OK.value(), "Success", newUCBO);
	}
}

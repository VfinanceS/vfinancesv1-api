package com.lams.api.service;

import com.lams.model.bo.PromoCodeBO;

public interface PromoCodesTransactionService {

	public PromoCodeBO getPromoCodeById(Long id);
}

package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.LoanTransaction;
import com.lams.api.repository.LoanTransactionRepository;
import com.lams.api.service.LoanTransactionService;
import com.lams.model.bo.LoanTransactionBO;


@Service
@Transactional
public class LoanTransactionServiceImpl implements LoanTransactionService {

	public static final Logger logger = Logger.getLogger(LoanTransactionServiceImpl.class);

	@Autowired
	private LoanTransactionRepository LoanTransactionRepository;

	@Override
	public List<LoanTransaction> getAll(Long userId) {
		return LoanTransactionRepository.findAll();
	}

	@Override
	public LoanTransaction get(Long id) {
		return LoanTransactionRepository.findOne(id);
	}

	@Override
	public Long save(LoanTransaction loanTransactionBO) {
		return LoanTransactionRepository.save(loanTransactionBO).getId();
	}

	@Override
	public List<LoanTransactionBO> findByApplIdAndBrUserId(Long applId, Long brUserId) {
		List<LoanTransactionBO> listBO = new ArrayList<LoanTransactionBO>();
		List<LoanTransaction> list = LoanTransactionRepository.findByApplIdAndBrUserId(applId, brUserId);
		
		for(LoanTransaction domain : list){
			if(domain.getDisbursmentAmount() > 0) {
				listBO.add(convertDomainToBO(domain));
			}
		}
		return listBO;
	}
	
	public LoanTransactionBO convertDomainToBO(LoanTransaction domain){
		LoanTransactionBO bo = new LoanTransactionBO();
		
		bo.setId(domain.getId());
		bo.setApplId(domain.getApplId());
		bo.setBrUserId(domain.getBrUserId());
		bo.setDisbursmentDate(domain.getDisbursmentDate()!=null?domain.getDisbursmentDate():null);
		bo.setCreatedDate(domain.getCreatedDate()!=null?domain.getCreatedDate():null);
		bo.setCreatedBy(domain.getCreatedBy()!=null?domain.getCreatedBy():null);
		bo.setModifiedDate(domain.getModifiedDate()!=null?domain.getModifiedDate():null);
		bo.setModifiedBy(domain.getModifiedBy()!=null?domain.getModifiedBy():null);
		bo.setDisbursmentAmount(domain.getDisbursmentAmount());

		
		return bo;
	}
}

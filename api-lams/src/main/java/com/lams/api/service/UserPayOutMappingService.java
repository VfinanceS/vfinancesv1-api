package com.lams.api.service;

import java.util.List;

import com.lams.model.bo.UserPayOutMappingBO;

public interface UserPayOutMappingService {

	public List<UserPayOutMappingBO> getAll(Long userId);
	
	public UserPayOutMappingBO findByUserIdAndApplTypeIdAndIsActive(Long id, Long applTypeId);
	
	public void save(UserPayOutMappingBO userPayOutMappingBO);

	public List<UserPayOutMappingBO> getCustomPayoutsByUserIdAndApplId(Long userId, Long applId);
}

package com.lams.api.service;

import java.util.List;

import com.lams.model.bo.report.Reports;

public interface ReportStructureService {

	public List<Reports> getAdminReport(Long userId);
	
	public List<Reports> getGeneralReport(Long userId, Long userType);
	
}

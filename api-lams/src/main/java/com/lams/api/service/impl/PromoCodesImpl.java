package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.PromotionalCodes;
import com.lams.api.repository.ApplicationsRepository;
import com.lams.api.repository.PromoCodesRepository;
import com.lams.api.repository.master.ApplicationTypeMstrRepository;
import com.lams.api.service.PromoCodesService;
import com.lams.api.utils.CodeConfig;
import com.lams.api.utils.VoucherCodes;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.PromoCodeBO;

@Service
@Transactional
public class PromoCodesImpl implements PromoCodesService {

	public static final Logger logger = Logger.getLogger(PromoCodesImpl.class);

	@Autowired
	private PromoCodesRepository promoCodesRepository;
	
	@Autowired
	private ApplicationsRepository applicationsRepository;
	
	@Autowired
	private ApplicationTypeMstrRepository applicationTypeMstrRepository;

	@Override
	public List<PromoCodeBO> getPromoCodeById(Long id) {
		return null;
	}

	@Override
	public List<PromoCodeBO> getAllPromoCodes() {
		List<PromotionalCodes> list = promoCodesRepository.findAll();

		return convertDomainToBO(list);
	}
	
	public PromoCodeBO convertDomainToBO(PromotionalCodes domain) {
		PromoCodeBO bo = new PromoCodeBO();
		BeanUtils.copyProperties(domain, bo);
		return bo;
	}

	public List<PromoCodeBO> convertDomainToBO(List<PromotionalCodes> domainList) {
		List<PromoCodeBO> boList = new ArrayList<PromoCodeBO>();
		
		for (PromotionalCodes code : domainList) {
			PromoCodeBO bo = convertDomainToBO(code);
			bo.setProduct(applicationTypeMstrRepository.findOne(bo.getApplicationTypeId()).getName());
			bo.setUsedBy(applicationsRepository.countByPromoCode(code.getPromoCode()));
			bo.setDescription(code.getDescription());
			boList.add(bo);
		}

		return boList;
	}

	@Override
	public List<PromoCodeBO> generateNewFreeCoupons(Long length, Integer quantity, Long userId, Long validDays) {
		List<String> newCoupons = new ArrayList<String>();
		List<PromotionalCodes> freshCoupons = new ArrayList<PromotionalCodes>();
		List<PromotionalCodes> alreadyExistCoupons = new ArrayList<PromotionalCodes>();

		CodeConfig config = CodeConfig.length(length.intValue());
        
		for(int i=0;i<=quantity;i++) {
			String couponCode = VoucherCodes.generate(config);
        	newCoupons.add(couponCode);
        	if(!newCouponCodeAlreadyExist(couponCode)) {
        		freshCoupons.add(instantiateCouponCode(couponCode, userId, validDays));
        	}
        	else {
        		alreadyExistCoupons.add(instantiateCouponCode(couponCode, userId, validDays));
        	}
		}
        
		return saveCoupons(freshCoupons);
	}
	
	public List<PromoCodeBO> saveCoupons(List<PromotionalCodes> freshCoupons){
		List<PromoCodeBO> list = new ArrayList<>();
		
		for(PromotionalCodes code : freshCoupons) {
			promoCodesRepository.save(code);
		}
		
		for(PromotionalCodes pCode : freshCoupons) {
			PromoCodeBO bo = new PromoCodeBO();
			BeanUtils.copyProperties(pCode, bo);
			list.add(bo);
		}
		
		return list;
	}
	
	public boolean newCouponCodeAlreadyExist(String couponCode) {
		Boolean isFound=false;
		
		PromotionalCodes code = promoCodesRepository.findByPromoCode(couponCode);
		
		if(code!=null) {
			isFound=true;
		}
		else {
			return isFound;
		}
		
		return isFound;
	}
	
	public boolean newCouponCodeAlreadyExist(String couponCode, Long applicationTypeId) {
		Boolean isFound=false;
		
		PromotionalCodes code = promoCodesRepository.findByPromoCodeAndApplicationTypeId(couponCode, applicationTypeId);
		
		if(code!=null) {
			isFound=true;
		}
		else {
			return isFound;
		}
		
		return isFound;
	}
	
	public PromotionalCodes instantiateCouponCode(String couponCode, Long userId, Long validDays) {
		PromotionalCodes code = new PromotionalCodes();
		
		code.setPromoCode(couponCode);
		code.setCreatedDate(new Date());
		code.setCreatedBy(userId);
		code.setIsActive(true);
		
		return code;
	}

	@Override
	public PromoCodeBO findDetailsByPromoCode(String promoCode) {
		PromoCodeBO bo = new PromoCodeBO();
		PromotionalCodes code = promoCodesRepository.findByPromoCode(promoCode);
		
		if(code!=null) {
			BeanUtils.copyProperties(code, bo);
		}
		else {
			return null;
		}
		
		return bo;
	}

	@Override
	public Boolean validatePromoCode(String promoCode, Long applicationTypeId) {
		/*PromoCodeBO promoCodeBO = findDetailsByPromoCode(promoCode);
		if((promoCodeBO!=null && promoCodeBO.getId()!=null) && promoCodeBO.getIsActive()) {
			PromotionalCodesTransactions txn = promoCodesTransactionsRepository.findByPromotionalCodes(promoCodesRepository.findByPromoCode(promoCode));
			if(txn!=null) {
				return false;
			}
			else {
				return true;
			}
		}
		return false;*/
		
		PromotionalCodes pCode = promoCodesRepository.findByPromoCodeAndApplicationTypeId(promoCode, applicationTypeId);
		
		if(pCode != null) {
			return true;
		}
		
		return false;
	}

	@Override
	public LamsResponse generateNewFreeCouponManually(PromoCodeBO promoCodeBO, Long userId) {

		PromotionalCodes code = promoCodesRepository.findOne(promoCodeBO.getId());//findByPromoCodeAndApplicationTypeId(promoCode, applicationTypeId);
		
		if(code!=null) {
			code.setApplicationTypeId(promoCodeBO.getApplicationTypeId());
			code.setIsActive(promoCodeBO.getIsActive());
			code.setModifiedBy(userId);
			code.setDescription(promoCodeBO.getDescription());
			code.setModifiedDate(new Date());
		}
		else {
			code = new PromotionalCodes();
			code.setPromoCode(promoCodeBO.getPromoCode());
			code.setDescription(promoCodeBO.getDescription());
			code.setApplicationTypeId(promoCodeBO.getApplicationTypeId());
			code.setIsActive(promoCodeBO.getIsActive());
			code.setCreatedBy(userId);
			code.setCreatedDate(new Date());
		}
		promoCodesRepository.save(code);
		
		return new LamsResponse(HttpStatus.OK.value(), "Coupon code saved successfully !", getAllPromoCodes());
	}
}

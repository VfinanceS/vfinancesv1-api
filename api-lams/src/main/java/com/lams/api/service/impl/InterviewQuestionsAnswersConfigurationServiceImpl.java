package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.InterviewQuestions;
import com.lams.api.domain.InterviewQuestionsAnswers;
import com.lams.api.repository.InterviewQuestionsAnswerConfigurationRepository;
import com.lams.api.repository.InterviewQuestionsConfigurationRepository;
import com.lams.api.service.InterviewQuestionsAnswerConfigurationService;
import com.lams.model.bo.InterviewQuestionsAnswerConfigBO;
import com.lams.model.bo.InterviewQuestionsAnswersConfigWrapperBO;
import com.lams.model.bo.LamsResponse;

@Service
@Transactional
public class InterviewQuestionsAnswersConfigurationServiceImpl implements InterviewQuestionsAnswerConfigurationService {

	public final static Logger logger = Logger.getLogger(InterviewQuestionsAnswersConfigurationServiceImpl.class.getName());

	@Autowired
	private InterviewQuestionsAnswerConfigurationRepository interviewQuestionsAnswerConfigurationRepository;
	
	@Autowired
	private InterviewQuestionsConfigurationRepository interviewQuestionsConfigurationRepository;

	@Override
	public InterviewQuestionsAnswerConfigBO getQuestionsByAgencyType(Long agencyType) {
		logger.log(Level.INFO," input id : "+agencyType);
		InterviewQuestionsAnswers question = interviewQuestionsAnswerConfigurationRepository.findByIdAndIsActive(agencyType, true);
		
		InterviewQuestionsAnswerConfigBO questionBO = new InterviewQuestionsAnswerConfigBO();
		BeanUtils.copyProperties(question, questionBO);
		
		return questionBO;
	}

	@Override
	public LamsResponse writeAnswers(List<InterviewQuestionsAnswerConfigBO> answers, Long userId) {
		for(InterviewQuestionsAnswerConfigBO qAnsBO : answers){
			InterviewQuestionsAnswers question = interviewQuestionsAnswerConfigurationRepository.findOne(qAnsBO.getId());
			
			question.setApplicationId(qAnsBO.getApplicationId());
			question.setAnswer(qAnsBO.getAnswer());
			question.setAgencyType(qAnsBO.getAgencyType());
			question.setModifiedDate(new Date());
			question.setModifiedBy(userId);
			
			question = interviewQuestionsAnswerConfigurationRepository.save(question);
		}
		
		logger.log(Level.INFO, "Successfully updated User Details for ID----" + answers);
		
		return new LamsResponse(HttpStatus.OK.value(), "Success", answers);
	}

	@Override
	public List<InterviewQuestionsAnswerConfigBO> getQuestionsByAgencyType(Long agencyType, Boolean isActive) {
		List<InterviewQuestionsAnswerConfigBO> questionBOs = new ArrayList<InterviewQuestionsAnswerConfigBO>();
		
		logger.log(Level.INFO," input id : "+agencyType);
		List<InterviewQuestionsAnswers> question = interviewQuestionsAnswerConfigurationRepository.findByAgencyTypeAndIsActive(agencyType, true);
		
		for(InterviewQuestionsAnswers questionsBOs : question){
			InterviewQuestionsAnswerConfigBO questionBO = new InterviewQuestionsAnswerConfigBO();
			
			BeanUtils.copyProperties(questionsBOs, questionBO);
			questionBOs.add(questionBO);
		}
		
		return questionBOs;
	}

	@Override
	public LamsResponse saveAllQuestions(InterviewQuestionsAnswersConfigWrapperBO allQuestionsMstr, Long userId) {
		for(InterviewQuestionsAnswerConfigBO qBO : allQuestionsMstr.getAllQuestionsMstr()){
			InterviewQuestionsAnswers question = new InterviewQuestionsAnswers();
			BeanUtils.copyProperties(qBO, question);
			
			question.setCreatedBy(userId);
			question.setCreatedDate(new Date());
			question.setApplicationId(allQuestionsMstr.getApplicationId());
			question.setAgencyType(allQuestionsMstr.getAgencyType());
			question.setIsActive(true);
			
			interviewQuestionsAnswerConfigurationRepository.save(question);
		}
		
		return new LamsResponse(HttpStatus.OK.value(), "Success", allQuestionsMstr.getAllQuestionsMstr());
	}

	@Override
	public InterviewQuestionsAnswersConfigWrapperBO getQuestionsAnsByAgencyType(Long agencyType, Long applicationId) {
		InterviewQuestionsAnswersConfigWrapperBO iQABO = new InterviewQuestionsAnswersConfigWrapperBO();
		List<InterviewQuestionsAnswerConfigBO> boList = new ArrayList<InterviewQuestionsAnswerConfigBO>();
		
		List<InterviewQuestions> questions = interviewQuestionsConfigurationRepository.findByAgencyTypeAndIsActive(agencyType, true);
		
		for(InterviewQuestions question : questions) {
			InterviewQuestionsAnswerConfigBO bo = new InterviewQuestionsAnswerConfigBO();
			InterviewQuestionsAnswers qAndAnswer = interviewQuestionsAnswerConfigurationRepository.findByQuestionIdAndAgencyTypeAndApplicationIdAndIsActive(question.getId(), agencyType, applicationId, true);
			bo.setQuestionDesc(question.getQuestionDesc());
			bo.setQuestionId(question.getId());
			if(qAndAnswer!=null) {
				BeanUtils.copyProperties(qAndAnswer, bo);
				
				if(qAndAnswer.getAnswer()!=null && !qAndAnswer.getAnswer().isEmpty()) {
					iQABO.setMarkedInterviewDone(true);
				}
			}
			
			boList.add(bo);
		}
		
		iQABO.setAgencyType(agencyType);
		iQABO.setAllQuestionsMstr(boList);
		return iQABO;
	}

	@Override
	public InterviewQuestionsAnswersConfigWrapperBO getQuestionsAnsByAgencyType(Long applicationId) {
		InterviewQuestionsAnswersConfigWrapperBO iQABO = new InterviewQuestionsAnswersConfigWrapperBO();
		List<InterviewQuestionsAnswerConfigBO> boList = new ArrayList<InterviewQuestionsAnswerConfigBO>();
		
		List<InterviewQuestionsAnswers> list = interviewQuestionsAnswerConfigurationRepository.findByApplicationIdAndIsActive(applicationId, true);
		
		for(InterviewQuestionsAnswers quiz : list) {
			InterviewQuestionsAnswerConfigBO quizBO = new InterviewQuestionsAnswerConfigBO();
			InterviewQuestions question = interviewQuestionsConfigurationRepository.findByIdAndIsActive(quiz.getQuestionId(), true);
			if(iQABO.getAgencyType()==null) {
				iQABO.setAgencyType(quiz.getAgencyType());
			}
			
			BeanUtils.copyProperties(quiz, quizBO);
			quizBO.setQuestionDesc(question.getQuestionDesc());
			boList.add(quizBO);
		}
		
		
		iQABO.setAllQuestionsMstr(boList);
		return iQABO;
	}
}

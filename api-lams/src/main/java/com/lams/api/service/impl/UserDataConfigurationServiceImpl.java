package com.lams.api.service.impl;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.UserDataConfig;
import com.lams.api.domain.UserDataConfigVersions;
import com.lams.api.repository.UserDataConfigurationRepository;
import com.lams.api.repository.UserDataConfigurationVersionRepository;
import com.lams.api.service.UserDataConfigurationService;
import com.lams.api.service.UserMstrService;
import com.lams.api.utils.SequenceNumberGenerator;
import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.UserBO;
import com.lams.model.bo.UserDataConfigBO;
import com.lams.model.utils.CommonUtils;
import com.lams.model.utils.Enums;

@Service
@Transactional
public class UserDataConfigurationServiceImpl implements UserDataConfigurationService {

	public final static Logger logger = Logger.getLogger(UserDataConfigurationServiceImpl.class.getName());

	@Autowired
	private UserDataConfigurationRepository userDataConfigurationRepository;
	
	@Autowired
	private UserDataConfigurationVersionRepository userDataConfigurationVersionRepository;
	
	@Autowired
	private SequenceNumberGenerator sequenceNumberGenerator;
	
	@Autowired
	private UserMstrService userMstrService;

	@Override
	public UserDataConfigBO getUserById(Long id) {
		logger.log(Level.INFO," input id : "+id);
		UserDataConfig user = userDataConfigurationRepository.findByAgencyTypeIdAndIsActiveAndVersionIsNull(id, true);
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", id);
			return null;
		}
		UserDataConfigBO userDataConfigBO = new UserDataConfigBO();
		BeanUtils.copyProperties(user, userDataConfigBO);
		
		return userDataConfigBO;
	}
	
	@Override
	public LamsResponse updateUserDetails(UserDataConfigBO userDataConfigBO) {
		UserDataConfig user = userDataConfigurationRepository.findOne(userDataConfigBO.getId());
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", userDataConfigBO.getId());
			return new LamsResponse(HttpStatus.BAD_REQUEST.value(), "User not Found");
		}
		BeanUtils.copyProperties(userDataConfigBO, user);
		
		user.setModifiedDate(new Date());
		user.setModifiedBy(userDataConfigBO.getId());
		user = userDataConfigurationRepository.save(user);
		logger.log(Level.INFO, "Successfully updated User Details for ID----" + user.getId());
		return new LamsResponse(HttpStatus.OK.value(), "Success", getUserById(user.getId()));
	}

	@Override
	public UserDataConfigBO getUserByTypeAndIsActive(Long id) {
		logger.log(Level.INFO," input id : "+id);
		UserDataConfig user = userDataConfigurationRepository.findOne(id);
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", id);
			return null;
		}
		UserDataConfigBO userDataConfigBO = new UserDataConfigBO();
		BeanUtils.copyProperties(user, userDataConfigBO);
		
		return userDataConfigBO;
	}
	
	@Override
	public LamsResponse updateUserConfiguration(UserDataConfigBO userBO, Long userType, Long doingByUser) {

		if(userBO.getUserId()!=null) {
			UserBO user = userMstrService.getUserById(userBO.getUserId());
			UserDataConfigVersions existingUsrConfg = userDataConfigurationVersionRepository.findByUserIdAndIsActive(userBO.getUserId(), true);
			if(existingUsrConfg!=null) {
				UserDataConfig configuration = userDataConfigurationRepository.findByVersion(existingUsrConfg.getConfgVersion());
				userDataConfigurationRepository.delete(configuration);
				
				existingUsrConfg.setModifiedBy(doingByUser);
				existingUsrConfg.setModifiedDate(new Date());
				userDataConfigurationVersionRepository.save(existingUsrConfg);
				
				//Add new entry on main data configuration table
				UserDataConfig userDataConfig = new UserDataConfig();
				BeanUtils.copyProperties(userBO, userDataConfig, "id");
				userDataConfig.setAgencyTypeId(user.getUserType());
				userDataConfig.setVersion(existingUsrConfg.getConfgVersion());
				userDataConfig.setCreatedBy(doingByUser);
				userDataConfig.setCreatedDate(new Date());
				
				userDataConfigurationRepository.save(userDataConfig);
			}
			else {
				UserDataConfigVersions newUsrConfg = new UserDataConfigVersions();
				newUsrConfg.setUserId(userBO.getUserId());
				try {
					String version = sequenceNumberGenerator.generateSequence(Enums.Sequence.SQ_USER_CINFIG_VERSION, "CONFIG", userBO.getUserId());
					newUsrConfg.setConfgVersion(version);
					newUsrConfg.setIsActive(true);
					
					userDataConfigurationVersionRepository.save(newUsrConfg);
					
					UserDataConfig userDataConfig = new UserDataConfig();
					BeanUtils.copyProperties(userBO, userDataConfig, "id");
					userDataConfig.setAgencyTypeId(user.getUserType());
					userDataConfig.setVersion(version);
					userDataConfig.setCreatedBy(doingByUser);
					userDataConfig.setCreatedDate(new Date());
					
					userDataConfigurationRepository.save(userDataConfig);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			UserDataConfig userDataConfig = userDataConfigurationRepository.findOne(userType);
			
			if(CommonUtils.isObjectNullOrEmpty(userDataConfig)) {
				userDataConfig = new UserDataConfig();
				userDataConfig.setCreatedDate(new Date());
				userDataConfig.setCreatedBy(doingByUser);
				userDataConfig.setIsActive(true);
			}
			else {
				userDataConfig.setModifiedDate(new Date());
				userDataConfig.setModifiedBy(doingByUser);
			}
			
			BeanUtils.copyProperties(userBO, userDataConfig);
			userDataConfigurationRepository.save(userDataConfig);
		}
		
		String msg = "Successful Registration";
		return new LamsResponse(HttpStatus.OK.value(), msg, userBO);
	}

	@Override
	public LamsResponse getUserByAgencyType(Long agencyTypeId) {
		logger.log(Level.INFO," input id : "+agencyTypeId);
		UserDataConfig user = userDataConfigurationRepository.findByAgencyTypeIdAndIsActiveAndVersionIsNull(agencyTypeId, true);
		if (CommonUtils.isObjectNullOrEmpty(user)) {
			logger.log(Level.INFO, "Invalid User Id while getting Object by Id==>{}", agencyTypeId);
			return null;
		}
		UserDataConfigBO userDataConfigBO = new UserDataConfigBO();
		BeanUtils.copyProperties(user, userDataConfigBO);
		
		return new LamsResponse(HttpStatus.OK.value(), "Data Found",userDataConfigBO);
	}

	@Override
	public UserDataConfigBO getConfigurationDetails(Long userId) {
		logger.log(Level.INFO," input id : "+userId);
		UserDataConfig userConfig;
		
		UserBO user = userMstrService.getUserById(userId);
		UserDataConfigVersions configVersion = userDataConfigurationVersionRepository.findByUserIdAndIsActive(userId, true);
		
		if(configVersion!=null) {
			userConfig = userDataConfigurationRepository.findByVersion(configVersion.getConfgVersion());
		}
		else {
			userConfig = userDataConfigurationRepository.findByAgencyTypeIdAndIsActiveAndVersionIsNull(user.getUserType(), true);
		}
		
		UserDataConfigBO userDataConfigBO = new UserDataConfigBO();
		BeanUtils.copyProperties(userConfig, userDataConfigBO);
		
		return userDataConfigBO;
	}
}

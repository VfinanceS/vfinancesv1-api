package com.lams.api.service;

import java.util.List;

import com.lams.api.domain.Applications;
import com.lams.model.bo.ApplicationRequestBO;
import com.lams.model.bo.ApplicationsBO;
import com.lams.model.bo.LamsResponse;

public interface ApplicationsService {

	public List<ApplicationsBO> getAll(Long userId);
	
	public ApplicationsBO get(Long id);
	
	public Long save(ApplicationRequestBO applicationsBO);
	
	public LamsResponse getLoanApplicationDetails(Long id,Long applicationTypeId,Long userId);
	
	public LamsResponse getApplicationsForLender(Long userId);
	
	public LamsResponse getApplicationsForLenderByApplicationId(Long appId,String status,Long lenderId);
	
	public Boolean updateStatus(Long applicationId, String status,Long userId);
	
	public List<ApplicationsBO> getAllByCP(Long userId,Long cpUserId);
	
	public Long saveFromCP(Long preApplicationId, ApplicationsBO applicationRequestBO,Long brUserId, Long channelPartnerId,String cpUserCode);
	
	public int inActiveByUserId(Long userId);
	
	public int inActiveByApplicationIdAndUserId(Long applicationId,Long userId);
	
	public boolean getLastApplication(Long userId);

	public List<ApplicationsBO> convertListOfDomainToListOfBO(List<Applications> applications);

	public LamsResponse getApplicationsForPDUserByApplicationId(Long appId, List<String> status, Long lenderId);
	
	public List<ApplicationsBO> getAllWithVerificationReports(Long userId);
}

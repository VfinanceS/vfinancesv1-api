package com.lams.api.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.service.DocumentUploadService;
import com.lams.api.service.ReportStructureService;
import com.lams.api.utils.ReportsJDBCImpl;
import com.lams.api.utils.VFExcelWriterHelper;
import com.lams.model.bo.DocumentRequest;
import com.lams.model.bo.report.Reports;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class ReportStructureServiceImpl implements ReportStructureService {

	@Autowired
	ReportsJDBCImpl reportsJDBCImpl;
	
	@Autowired
	private Environment environment;
	
	private VFExcelWriterHelper excelHelper;
	
	@Autowired
	private DocumentUploadService documentUploadService;
	
	@Override
	public List<Reports> getAdminReport(Long userId) {
		List<Reports> repotList = reportsJDBCImpl.getReportData();
		excelHelper = new VFExcelWriterHelper();
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
			String date = format.format(new Date()).trim();
	        date = date.replaceAll("-", "_");
	        date = date.replaceAll(":", "-");
	        date = date.trim();
			String storeFileName="VF-Admin-Report_"+date+".xlsx";

			DocumentRequest documentRequest = new DocumentRequest();
			documentRequest.setDocumentId(CommonUtils.DocumentType.ADMIN_REPORT.getId());
			documentRequest.setIsUserDocument(false);
			documentRequest.setUserId(userId);

			documentUploadService.upload(storeFileName, excelHelper.generateAdminReport(repotList, environment), documentRequest);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return repotList;
	}

	@Override
	public List<Reports> getGeneralReport(Long userId, Long userType) {
		List<Reports> repotList = reportsJDBCImpl.getReportData(userId, userType);
		excelHelper = new VFExcelWriterHelper();
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
			String date = format.format(new Date()).trim();
	        date = date.replaceAll("-", "_");
	        date = date.replaceAll(":", "-");
	        date = date.trim();
			String storeFileName="VF-Admin-Report_"+date+".xlsx";

			DocumentRequest documentRequest = new DocumentRequest();
			documentRequest.setDocumentId(CommonUtils.DocumentType.ADMIN_REPORT.getId());
			documentRequest.setIsUserDocument(false);
			documentRequest.setUserId(userId);

			documentUploadService.upload(storeFileName, excelHelper.generateAdminReport(repotList, environment), documentRequest);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return repotList;
	}
}

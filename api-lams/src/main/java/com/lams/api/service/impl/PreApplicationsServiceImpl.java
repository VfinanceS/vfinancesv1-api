package com.lams.api.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lams.api.domain.PreApplications;
import com.lams.api.domain.User;
import com.lams.api.domain.master.ApplicationTypeMstr;
import com.lams.api.domain.master.LoanTypeMstr;
import com.lams.api.repository.PreApplicationsRepository;
import com.lams.api.repository.master.ApplicationTypeMstrRepository;
import com.lams.api.repository.master.LoanTypeMstrRepository;
import com.lams.api.service.ApplicationsService;
import com.lams.api.service.PreApplicationsService;
import com.lams.api.service.UserMstrService;
import com.lams.model.bo.PreApplicationsBO;
import com.lams.model.bo.UserBO;
import com.lams.model.utils.CommonUtils;

@Service
@Transactional
public class PreApplicationsServiceImpl implements PreApplicationsService {

	public static final Logger logger = Logger.getLogger(PreApplicationsServiceImpl.class);

	@Autowired
	private PreApplicationsRepository preApplicationsRepository;
	
	@Autowired
	private ApplicationTypeMstrRepository applicationTypeMstrRepository;
	
	@Autowired
	private LoanTypeMstrRepository loanTypeMstrRepository;
	
	@Autowired
	private UserMstrService userMstrService;
	
	@Autowired
	private ApplicationsService applicationsService;
	

	/**
	 * CONVERT APPLICATIONS DOMAIN OBJ TO BO OBJ
	 * 
	 * @param applications
	 * @return
	 */
	@Override
	public PreApplicationsBO convertDomainToBO(PreApplications applications) {
		PreApplicationsBO applicationsBO = new PreApplicationsBO();
		if (CommonUtils.isObjectNullOrEmpty(applications)) {
			return applicationsBO;
		}
		BeanUtils.copyProperties(applications, applicationsBO);
		if (!CommonUtils.isObjectNullOrEmpty(applications.getApplicationTypeId())) {
			applicationsBO.setApplicationTypeId(applications.getApplicationTypeId().getId());
			applicationsBO.setApplicationTypeCode(applications.getApplicationTypeId().getCode());
			applicationsBO.setApplicationTypeName(applications.getApplicationTypeId().getName());
		}
		if (!CommonUtils.isObjectNullOrEmpty(applications.getLoanTypeId())) {
			applicationsBO.setLoanTypeId(applications.getLoanTypeId().getId());
		}
		
		if(!CommonUtils.isObjectListNull(applications.getApplicationId())) {
			applicationsBO.setApplicationId(applicationsService.convertListOfDomainToListOfBO(applications.getApplicationId()));
		}
		
		return applicationsBO;
	}
	
	public PreApplications convertBOToDomain(PreApplicationsBO bo) {
		PreApplications domain = new PreApplications();
		ApplicationTypeMstr apMstr = new ApplicationTypeMstr();
		apMstr.setId(bo.getApplicationTypeId());
		domain.setApplicationTypeId(apMstr);
		
		LoanTypeMstr lTypMstr = new LoanTypeMstr();
		lTypMstr.setId(bo.getLoanTypeId());
		domain.setLoanTypeId(lTypMstr);
		domain.setLoanAmount(bo.getLoanAmount());
		domain.setUserId(bo.getUserId());
		domain.setStatus(bo.getStatus());
		domain.setChannelPartnerId(bo.getChannelPartnerId());
		
		return domain;
	}

	@Override
	public Boolean updateStatus(Long applicationId, String status, Long userId) {
		PreApplications app = preApplicationsRepository.findOne(applicationId);
		app.setStatus(status);
		preApplicationsRepository.save(app);
		return Boolean.TRUE;
	}

	@Override
	public Long saveFromCP(PreApplicationsBO applicationRequestBO,Long brUserId,User user) {
		logger.info("Enter in Save Application Sevice Impl--------------type----> "
				+ applicationRequestBO.getApplicationTypeId());
		PreApplications domain = null;
		PreApplications preApplication = null;
		try {
			if(applicationRequestBO.getId()!=null) {
				preApplication = preApplicationsRepository.findOne(applicationRequestBO.getId());
				preApplication.setApplicationTypeId(applicationTypeMstrRepository.findOne(applicationRequestBO.getApplicationTypeId()));
				preApplication.setLoanTypeId(loanTypeMstrRepository.findOne(preApplication.getLoanTypeId().getId()));
				preApplication.setModifiedBy(user.getId());
				preApplication.setModifiedDate(new Date());
				domain = preApplication;
			}
			else {
				setCommonProperty(applicationRequestBO, applicationRequestBO, user.getCode(), brUserId);
				domain = convertBOToDomain(applicationRequestBO);
				initDomain(domain, applicationRequestBO);
				domain.setChannelPartnerId(user.getId());
			}
			
			return preApplicationsRepository.save(domain).getId();
		} catch (Exception e) {
			logger.info("Error while save application form data");
			e.printStackTrace();
		}
		return null;
	}
	
	private void setCommonProperty(PreApplicationsBO from,PreApplicationsBO to,String cpUserCode,Long brUserId) {
		to.setApplicationTypeId(from.getApplicationTypeId());
		to.setUserId(brUserId);
		to.setLoanAmount(from.getLoanAmount());
		to.setLoanTypeId(Long.valueOf(CommonUtils.LoanType.CURRENT_LOAN));
		to.setStatus(CommonUtils.Status.OPEN);
	}
	
	private void initDomain(PreApplications domainObj, PreApplicationsBO requestLoanDetailsBO) {
		
		if(!CommonUtils.isObjectNullOrEmpty(requestLoanDetailsBO.getId())) {
			domainObj = preApplicationsRepository.findByIdAndIsActive(requestLoanDetailsBO.getId(), true);
		}
		if(domainObj.getId()!=null) {
			domainObj.setModifiedBy(requestLoanDetailsBO.getUserId());
			domainObj.setModifiedDate(new Date());
		} else {
			domainObj.setCreatedBy(requestLoanDetailsBO.getUserId());
			domainObj.setCreatedDate(new Date());
			domainObj.setIsActive(true);
			domainObj.setUserId(requestLoanDetailsBO.getUserId());
		}
		BeanUtils.copyProperties(requestLoanDetailsBO, domainObj,CommonUtils.skipFieldsForCreateApp);
		if(!CommonUtils.isObjectNullOrEmpty(requestLoanDetailsBO.getApplicationTypeId())) {
			domainObj.setApplicationTypeId(applicationTypeMstrRepository.findOne(requestLoanDetailsBO.getApplicationTypeId()));
		}
		if(!CommonUtils.isObjectNullOrEmpty(requestLoanDetailsBO.getLoanTypeId())) {
			domainObj.setLoanTypeId(loanTypeMstrRepository.findOne(requestLoanDetailsBO.getLoanTypeId()));
		}
	}

	@Override
	public List<PreApplicationsBO> getAllActivePreApplicationByUserId(Long brUserId) {
		List<PreApplicationsBO> preApplicationsBO = new ArrayList<PreApplicationsBO>();
		List<PreApplications> preApplications = preApplicationsRepository.findByUserIdAndIsActiveAndStatus(brUserId, true, "OPEN");
		
		for(PreApplications applications : preApplications) {
			PreApplicationsBO bo = convertDomainToBO(applications);
			bo.setChannelPartner(userMstrService.getUserById(applications.getChannelPartnerId()));
			preApplicationsBO.add(bo);
		}
		
		return preApplicationsBO;
	}

	@Override
	public List<UserBO> getAllActivePreApplicationByChannelPartnerId(Long channelPartnerId) {
		List<PreApplications> list = preApplicationsRepository.findByChannelPartnerId(channelPartnerId);
		List<UserBO> userBOs = new ArrayList<>();
		
		Set<Long> userIds = new HashSet<Long>();
		
		for(PreApplications application : list) {
			userIds.add(application.getUserId());
		}
		
		Iterator<Long> itr = userIds.iterator();
		while (itr.hasNext()) {
			Long userId = itr.next();
			userBOs.add(userMstrService.getUserById(userId));
		}
		
		for(UserBO usr : userBOs) {
			List<PreApplicationsBO> preApplicationsBO = new ArrayList<PreApplicationsBO>();
			List<PreApplications> preApplications = preApplicationsRepository.findByUserIdAndChannelPartnerId(usr.getId(),channelPartnerId);
			for(PreApplications preApplication : preApplications) {
				preApplicationsBO.add(convertDomainToBO(preApplication));preApplication.getApplicationId();
			}
			usr.setPreApplications(preApplicationsBO);
		}
		return userBOs;
	}

	@Override
	public List<Long> getDistinctChannelPartnerIdByUserIdAndIsActive(Long userId) {
		return preApplicationsRepository.getDistinctChannelPartnerIdByUserIdAndIsActive(userId);
	}

	@Override
	public List<PreApplicationsBO> getAllActivePreApplicationByUserIdAndChannelPartnerId(Long brUserId, Long channelPartnerId) {
		List<PreApplicationsBO> preApplicationsBO  = new ArrayList<PreApplicationsBO>();
		List<PreApplications> list = preApplicationsRepository.findByUserIdAndChannelPartnerIdAndIsActive(brUserId, channelPartnerId, true);
		
		for(PreApplications preApp : list) {
			preApplicationsBO.add(convertDomainToBO(preApp));
		}
		return preApplicationsBO;
	}
}

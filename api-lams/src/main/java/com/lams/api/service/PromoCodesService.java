package com.lams.api.service;

import java.util.List;

import com.lams.model.bo.LamsResponse;
import com.lams.model.bo.PromoCodeBO;

public interface PromoCodesService {

	public List<PromoCodeBO> getPromoCodeById(Long id);
	
	public List<PromoCodeBO> getAllPromoCodes();
	
	public List<PromoCodeBO> generateNewFreeCoupons(Long length, Integer quatity, Long UserId, Long validDays);
	
	public LamsResponse generateNewFreeCouponManually(PromoCodeBO promoCodeBO, Long userId);
	
	public PromoCodeBO findDetailsByPromoCode(String promoCode);
	
	public Boolean validatePromoCode(String promoCode, Long applicationTypeId);
}

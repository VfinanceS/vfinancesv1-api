package com.lams.model.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChannelPartnerCommisionBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String disbursmentMonthYear;
	private String applicationName;
	private Long appId;
	private Long applTypeId;
	private Double totalDisbursmentAmount;
	private Long totalCount;
	private Double totalCommision;
	

	public String getDisbursmentMonthYear() {
		return disbursmentMonthYear;
	}
	public void setDisbursmentMonthYear(String disbursmentMonthYear) {
		this.disbursmentMonthYear = disbursmentMonthYear;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public Double getTotalDisbursmentAmount() {
		return totalDisbursmentAmount;
	}
	public void setTotalDisbursmentAmount(Double totalDisbursmentAmount) {
		this.totalDisbursmentAmount = totalDisbursmentAmount;
	}
	public Long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	public Double getTotalCommision() {
		return Double.parseDouble(String.format("%.3f", totalCommision));
	}
	public void setTotalCommision(Double totalCommision) {
		this.totalCommision = totalCommision;
	}
	public Long getApplTypeId() {
		return applTypeId;
	}
	public void setApplTypeId(Long applTypeId) {
		this.applTypeId = applTypeId;
	}
	public Long getAppId() {
		return appId;
	}
	public void setAppId(Long appId) {
		this.appId = appId;
	}
}
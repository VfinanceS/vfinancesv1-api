package com.lams.model.bo.report;

import java.util.Date;

public class DisbursmentDetails{
	private Date disbursmentDate;
	private Double disbursmentAmount;
	
	public Date getDisbursmentDate() {
		return disbursmentDate;
	}
	public void setDisbursmentDate(Date disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}
	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}
	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}
}

package com.lams.model.bo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCoApplicantBO {

private Long id;
	
	private Long parentUserId;

	private String name;

	private String email;

	private String mobile;

	private Integer invitationCount;

	private String firstName;

	private String lastName;

	private String middleName;

	private Date birthDate;

	private Long gender;

	private Long userType;

	private Long salutation;

	private String panCard;

	private String aadharCardNo;

	private String eduQualification;

	private String contactNumber;

	private Boolean isSameUsAddress;

	private Long employmentType;

	private String employerName;

	private AddressBO employmentAddress;

	private Double grossMonthlyIncome;

	private Integer totalWorkExperience;

	private String entityName;

	private String contactPersonName;

	private String gstNumber;

	private String aboutMe;

	private String code;

	private Long entityType;

	private Long selfEmployedType;

	private Boolean isProfileFilled;
	
	private String nameOfCoapplicant;
	
	private AddressBO communicationAdd;

	private AddressBO permanentAdd;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(Long parentUserId) {
		this.parentUserId = parentUserId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getInvitationCount() {
		return invitationCount;
	}

	public void setInvitationCount(Integer invitationCount) {
		this.invitationCount = invitationCount;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Long getGender() {
		return gender;
	}

	public void setGender(Long gender) {
		this.gender = gender;
	}

	public Long getUserType() {
		return userType;
	}

	public void setUserType(Long userType) {
		this.userType = userType;
	}

	public Long getSalutation() {
		return salutation;
	}

	public void setSalutation(Long salutation) {
		this.salutation = salutation;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getEduQualification() {
		return eduQualification;
	}

	public void setEduQualification(String eduQualification) {
		this.eduQualification = eduQualification;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Boolean getIsSameUsAddress() {
		return isSameUsAddress;
	}

	public void setIsSameUsAddress(Boolean isSameUsAddress) {
		this.isSameUsAddress = isSameUsAddress;
	}

	public Long getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(Long employmentType) {
		this.employmentType = employmentType;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public AddressBO getEmploymentAddress() {
		return employmentAddress;
	}

	public void setEmploymentAddress(AddressBO employmentAddress) {
		this.employmentAddress = employmentAddress;
	}

	public Double getGrossMonthlyIncome() {
		return grossMonthlyIncome;
	}

	public void setGrossMonthlyIncome(Double grossMonthlyIncome) {
		this.grossMonthlyIncome = grossMonthlyIncome;
	}

	public Integer getTotalWorkExperience() {
		return totalWorkExperience;
	}

	public void setTotalWorkExperience(Integer totalWorkExperience) {
		this.totalWorkExperience = totalWorkExperience;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getEntityType() {
		return entityType;
	}

	public void setEntityType(Long entityType) {
		this.entityType = entityType;
	}

	public Long getSelfEmployedType() {
		return selfEmployedType;
	}

	public void setSelfEmployedType(Long selfEmployedType) {
		this.selfEmployedType = selfEmployedType;
	}

	public Boolean getIsProfileFilled() {
		return isProfileFilled;
	}

	public void setIsProfileFilled(Boolean isProfileFilled) {
		this.isProfileFilled = isProfileFilled;
	}

	public String getNameOfCoapplicant() {
		return nameOfCoapplicant;
	}

	public void setNameOfCoapplicant(String nameOfCoapplicant) {
		this.nameOfCoapplicant = nameOfCoapplicant;
	}

	public AddressBO getCommunicationAdd() {
		return communicationAdd;
	}

	public void setCommunicationAdd(AddressBO communicationAdd) {
		this.communicationAdd = communicationAdd;
	}

	public AddressBO getPermanentAdd() {
		return permanentAdd;
	}

	public void setPermanentAdd(AddressBO permanentAdd) {
		this.permanentAdd = permanentAdd;
	}

}

package com.lams.model.bo;

import java.util.Date;


public class UserPayOutMappingBO  {
	private Long id;
	
	private Long userId;
	
	private Long applTypeId;
	
	private String payoutVersion;
	
	private Boolean active;

	private Date createdDate;
	
	private Long createdBy;
	
	private Date modifiedDate;
	
	private Long modifiedBy;

	private UserBO channelPartnerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getPayoutVersion() {
		return payoutVersion;
	}

	public void setPayoutVersion(String payoutVersion) {
		this.payoutVersion = payoutVersion;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public UserBO getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(UserBO channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public Long getApplTypeId() {
		return applTypeId;
	}

	public void setApplTypeId(Long applTypeId) {
		this.applTypeId = applTypeId;
	}
	
}
package com.lams.model.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDataConfigBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long userId;

	private Boolean salutation;
	
	private Long agencyTypeId;
	
	private Boolean profileImage;

	private Boolean name;

	private Boolean email;

	private Boolean mobile;

	private Boolean firstName;

	private Boolean lastName;
	
	private Boolean middleName;

	private Boolean communicationAdd;

	private Boolean permanentAdd;

	private Boolean birthDate;

	private Boolean gender;

	private Boolean userType;

	private Boolean bank;

	private Boolean panCard;

	private Boolean aadharCardNo;

	private Boolean eduQualification;

	private Boolean contactNumber;

	private Boolean employmentType;

	private Boolean employerName;

	private Boolean employmentAddress;

	private Boolean grossMonthlyIncome;

	private Boolean totalWorkExperience;

	private Boolean entityName;

	private Boolean selfEmployedType;

	private Boolean entityType;
	
	private Boolean businessType;
	
	private Boolean aboutMe;
	
	private Boolean channelPartner;
	
	private Boolean code;
	
	private Boolean gstNumber;
	
	private Boolean contactPersonName;

	private Boolean bankDetails;
	
	private Boolean coApplicantDetails;
	
	private Boolean brDocuments;
	
	private Boolean respondedDocuments;
	
	private Boolean coApplicantDocuments;
	
	private Boolean existingLoan;
	
	private Boolean closedLoan;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getSalutation() {
		return salutation;
	}

	public void setSalutation(Boolean salutation) {
		this.salutation = salutation;
	}

	public Boolean getName() {
		return name;
	}

	public void setName(Boolean name) {
		this.name = name;
	}

	public Boolean getEmail() {
		return email;
	}

	public void setEmail(Boolean email) {
		this.email = email;
	}

	public Boolean getMobile() {
		return mobile;
	}

	public void setMobile(Boolean mobile) {
		this.mobile = mobile;
	}

	public Boolean getFirstName() {
		return firstName;
	}

	public void setFirstName(Boolean firstName) {
		this.firstName = firstName;
	}

	public Boolean getLastName() {
		return lastName;
	}

	public void setLastName(Boolean lastName) {
		this.lastName = lastName;
	}

	public Boolean getMiddleName() {
		return middleName;
	}

	public void setMiddleName(Boolean middleName) {
		this.middleName = middleName;
	}

	public Boolean getCommunicationAdd() {
		return communicationAdd;
	}

	public void setCommunicationAdd(Boolean communicationAdd) {
		this.communicationAdd = communicationAdd;
	}

	public Boolean getPermanentAdd() {
		return permanentAdd;
	}

	public void setPermanentAdd(Boolean permanentAdd) {
		this.permanentAdd = permanentAdd;
	}

	public Boolean getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Boolean birthDate) {
		this.birthDate = birthDate;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Boolean getUserType() {
		return userType;
	}

	public void setUserType(Boolean userType) {
		this.userType = userType;
	}

	public Boolean getBank() {
		return bank;
	}

	public void setBank(Boolean bank) {
		this.bank = bank;
	}

	public Boolean getPanCard() {
		return panCard;
	}

	public void setPanCard(Boolean panCard) {
		this.panCard = panCard;
	}

	public Boolean getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(Boolean aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public Boolean getEduQualification() {
		return eduQualification;
	}

	public void setEduQualification(Boolean eduQualification) {
		this.eduQualification = eduQualification;
	}

	public Boolean getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Boolean contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Boolean getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(Boolean employmentType) {
		this.employmentType = employmentType;
	}

	public Boolean getEmployerName() {
		return employerName;
	}

	public void setEmployerName(Boolean employerName) {
		this.employerName = employerName;
	}

	public Boolean getEmploymentAddress() {
		return employmentAddress;
	}

	public void setEmploymentAddress(Boolean employmentAddress) {
		this.employmentAddress = employmentAddress;
	}

	public Boolean getGrossMonthlyIncome() {
		return grossMonthlyIncome;
	}

	public void setGrossMonthlyIncome(Boolean grossMonthlyIncome) {
		this.grossMonthlyIncome = grossMonthlyIncome;
	}

	public Boolean getTotalWorkExperience() {
		return totalWorkExperience;
	}

	public void setTotalWorkExperience(Boolean totalWorkExperience) {
		this.totalWorkExperience = totalWorkExperience;
	}

	public Boolean getEntityName() {
		return entityName;
	}

	public void setEntityName(Boolean entityName) {
		this.entityName = entityName;
	}

	public Boolean getSelfEmployedType() {
		return selfEmployedType;
	}

	public void setSelfEmployedType(Boolean selfEmployedType) {
		this.selfEmployedType = selfEmployedType;
	}

	public Boolean getEntityType() {
		return entityType;
	}

	public void setEntityType(Boolean entityType) {
		this.entityType = entityType;
	}

	public Boolean getBusinessType() {
		return businessType;
	}

	public void setBusinessType(Boolean businessType) {
		this.businessType = businessType;
	}

	public Boolean getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(Boolean aboutMe) {
		this.aboutMe = aboutMe;
	}

	public Boolean getChannelPartner() {
		return channelPartner;
	}

	public void setChannelPartner(Boolean channelPartner) {
		this.channelPartner = channelPartner;
	}

	public Boolean getCode() {
		return code;
	}

	public void setCode(Boolean code) {
		this.code = code;
	}

	public Boolean getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(Boolean gstNumber) {
		this.gstNumber = gstNumber;
	}

	public Boolean getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(Boolean contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public Boolean getBankDetails() {
		return bankDetails;
	}

	public void setBankDetails(Boolean bankDetails) {
		this.bankDetails = bankDetails;
	}

	public Long getAgencyTypeId() {
		return agencyTypeId;
	}

	public void setAgencyTypeId(Long agencyTypeId) {
		this.agencyTypeId = agencyTypeId;
	}

	public Boolean getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(Boolean profileImage) {
		this.profileImage = profileImage;
	}

	public Boolean getCoApplicantDetails() {
		return coApplicantDetails;
	}

	public void setCoApplicantDetails(Boolean coApplicantDetails) {
		this.coApplicantDetails = coApplicantDetails;
	}

	public Boolean getBrDocuments() {
		return brDocuments;
	}

	public void setBrDocuments(Boolean brDocuments) {
		this.brDocuments = brDocuments;
	}

	public Boolean getRespondedDocuments() {
		return respondedDocuments;
	}

	public void setRespondedDocuments(Boolean respondedDocuments) {
		this.respondedDocuments = respondedDocuments;
	}

	public Boolean getCoApplicantDocuments() {
		return coApplicantDocuments;
	}

	public void setCoApplicantDocuments(Boolean coApplicantDocuments) {
		this.coApplicantDocuments = coApplicantDocuments;
	}

	public Boolean getExistingLoan() {
		return existingLoan;
	}

	public void setExistingLoan(Boolean existingLoan) {
		this.existingLoan = existingLoan;
	}

	public Boolean getClosedLoan() {
		return closedLoan;
	}

	public void setClosedLoan(Boolean closedLoan) {
		this.closedLoan = closedLoan;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
package com.lams.model.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BankDetailsBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Long userId;
	
	private String bankName;
	
	private Long accountNumber;

	private String accountName;
	
	private Long neftIfscCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Long getNeftIfscCode() {
		return neftIfscCode;
	}

	public void setNeftIfscCode(Long neftIfscCode) {
		this.neftIfscCode = neftIfscCode;
	}

}

package com.lams.model.bo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InterviewQuestionsAnswersConfigWrapperBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long agencyType;
	
	private Boolean markedInterviewDone=false;
	
	private Long applicationId;
	
	private List<InterviewQuestionsConfigBO> questions;
	
	private List<InterviewQuestionsAnswerConfigBO> allQuestionsMstr;

	public Long getAgencyType() {
		return agencyType;
	}

	public void setAgencyType(Long agencyType) {
		this.agencyType = agencyType;
	}

	public List<InterviewQuestionsAnswerConfigBO> getAllQuestionsMstr() {
		return allQuestionsMstr;
	}

	public void setAllQuestionsMstr(List<InterviewQuestionsAnswerConfigBO> allQuestionsMstr) {
		this.allQuestionsMstr = allQuestionsMstr;
	}

	public Boolean getMarkedInterviewDone() {
		return markedInterviewDone;
	}

	public void setMarkedInterviewDone(Boolean markedInterviewDone) {
		this.markedInterviewDone = markedInterviewDone;
	}

	public List<InterviewQuestionsConfigBO> getQuestions() {
		return questions;
	}

	public void setQuestions(List<InterviewQuestionsConfigBO> questions) {
		this.questions = questions;
	}

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}
	
}
package com.lams.model.bo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InterviewQuestionsConfigWrapperBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long agencyType;
	
	private List<InterviewQuestionsConfigBO> allQuestionsMstr;

	public Long getAgencyType() {
		return agencyType;
	}

	public void setAgencyType(Long agencyType) {
		this.agencyType = agencyType;
	}

	public List<InterviewQuestionsConfigBO> getAllQuestionsMstr() {
		return allQuestionsMstr;
	}

	public void setAllQuestionsMstr(List<InterviewQuestionsConfigBO> allQuestionsMstr) {
		this.allQuestionsMstr = allQuestionsMstr;
	}
}
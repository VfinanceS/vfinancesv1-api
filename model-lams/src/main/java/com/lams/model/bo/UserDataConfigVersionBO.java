package com.lams.model.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDataConfigVersionBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long userId;
	
	private String confgVersion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getConfgVersion() {
		return confgVersion;
	}

	public void setConfgVersion(String confgVersion) {
		this.confgVersion = confgVersion;
	}
}
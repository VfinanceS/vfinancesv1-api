package com.lams.model.bo.report;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.lams.model.bo.AuditorBO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Reports extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long brUserId;
	private String brFullName;
	private String loanReferenceNumber;
	private Long applTypeId;
	private String loanType;
	private Double loanAmount;
	private String loanStatus;
	private String pdReportStatus;
	private List<String> lendersResponded;
	private String lenderRespondedName;
	private String lenderResponded;
	private Long brSelectedLender;
	private String brSelectedLenderName="";
	private Double brAppliedForLoanAmt;
	private List<DisbursmentDetails> disbursmentReport;
	
	
	public String getLoanReferenceNumber() {
		return loanReferenceNumber;
	}
	public void setLoanReferenceNumber(String loanReferenceNumber) {
		this.loanReferenceNumber = loanReferenceNumber;
	}
	public Long getBrUserId() {
		return brUserId;
	}
	public void setBrUserId(Long brUserId) {
		this.brUserId = brUserId;
	}
	public String getBrFullName() {
		return brFullName;
	}
	public void setBrFullName(String brFullName) {
		this.brFullName = brFullName;
	}
	public Long getApplTypeId() {
		return applTypeId;
	}
	public void setApplTypeId(Long applTypeId) {
		this.applTypeId = applTypeId;
	}
	public Double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getLoanStatus() {
		return loanStatus;
	}
	public void setLoanStatus(String loanStatus) {
		this.loanStatus = loanStatus;
	}
	public List<String> getLendersResponded() {
		return lendersResponded;
	}
	public void setLendersResponded(List<String> lendersResponded) {
		this.lendersResponded = lendersResponded;
	}
	public Long getBrSelectedLender() {
		return brSelectedLender;
	}
	public void setBrSelectedLender(Long brSelectedLender) {
		this.brSelectedLender = brSelectedLender;
	}
	public Double getBrAppliedForLoanAmt() {
		return brAppliedForLoanAmt;
	}
	public void setBrAppliedForLoanAmt(Double brAppliedForLoanAmt) {
		this.brAppliedForLoanAmt = brAppliedForLoanAmt;
	}
	public List<DisbursmentDetails> getDisbursmentReport() {
		return disbursmentReport;
	}
	public void setDisbursmentReport(List<DisbursmentDetails> disbursmentReport) {
		this.disbursmentReport = disbursmentReport;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public String getLenderResponded() {
		return lenderResponded;
	}
	public void setLenderResponded(String lenderResponded) {
		this.lenderResponded = lenderResponded;
	}
	public String getLenderRespondedName() {
		return lenderRespondedName;
	}
	public void setLenderRespondedName(String lenderRespondedName) {
		this.lenderRespondedName = lenderRespondedName;
	}
	public String getBrSelectedLenderName() {
		return brSelectedLenderName;
	}
	public void setBrSelectedLenderName(String brSelectedLenderName) {
		this.brSelectedLenderName = brSelectedLenderName;
	}
	public String getPdReportStatus() {
		return pdReportStatus;
	}
	public void setPdReportStatus(String pdReportStatus) {
		this.pdReportStatus = pdReportStatus;
	}
	
}


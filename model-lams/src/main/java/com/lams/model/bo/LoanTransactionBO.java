package com.lams.model.bo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanTransactionBO implements Serializable{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	public Long applId;
	
	public Long brUserId;
	
	private Date disbursmentDate;
	
	private Double disbursmentAmount;
	
	private Date createdDate;

	public Long createdBy;
	
	private Date modifiedDate;

	public Long modifiedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplId() {
		return applId;
	}

	public void setApplId(Long applId) {
		this.applId = applId;
	}

	public Long getBrUserId() {
		return brUserId;
	}

	public void setBrUserId(Long brUserId) {
		this.brUserId = brUserId;
	}

	public Date getDisbursmentDate() {
		return disbursmentDate;
	}

	public void setDisbursmentDate(Date disbursmentDate) {
		this.disbursmentDate = disbursmentDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Double getDisbursmentAmount() {
		return disbursmentAmount;
	}

	public void setDisbursmentAmount(Double disbursmentAmount) {
		this.disbursmentAmount = disbursmentAmount;
	}
}

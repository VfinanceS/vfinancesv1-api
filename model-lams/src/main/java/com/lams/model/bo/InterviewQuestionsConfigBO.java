package com.lams.model.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InterviewQuestionsConfigBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String questionDesc;
	
	private Long agencyType;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuestionDesc() {
		return questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public Long getAgencyType() {
		return agencyType;
	}

	public void setAgencyType(Long agencyType) {
		this.agencyType = agencyType;
	}
	
}
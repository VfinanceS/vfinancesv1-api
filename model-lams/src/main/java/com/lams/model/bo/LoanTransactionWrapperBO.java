package com.lams.model.bo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanTransactionWrapperBO implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<LoanTransactionBO> loanTransactionList;
	
	public String status;

	public List<LoanTransactionBO> getLoanTransactionList() {
		return loanTransactionList;
	}

	public void setLoanTransactionList(List<LoanTransactionBO> loanTransactionList) {
		this.loanTransactionList = loanTransactionList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}

package com.lams.model.bo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PreApplicationsBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private Long applicationTypeId;

	private String applicationTypeCode;
	
	private String applicationTypeName;

	private Long loanTypeId;

	private Long userId;

	private Double loanAmount;

	private String status;

	private Long channelPartnerId;

	private Long loanCategoryId;
	
	private UserBO channelPartner;
	
	private List<ApplicationsBO> applicationId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplicationTypeId() {
		return applicationTypeId;
	}

	public void setApplicationTypeId(Long applicationTypeId) {
		this.applicationTypeId = applicationTypeId;
	}

	public Long getLoanTypeId() {
		return loanTypeId;
	}

	public void setLoanTypeId(Long loanTypeId) {
		this.loanTypeId = loanTypeId;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getApplicationTypeCode() {
		return applicationTypeCode;
	}

	public void setApplicationTypeCode(String applicationTypeCode) {
		this.applicationTypeCode = applicationTypeCode;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getLoanCategoryId() {
		return loanCategoryId;
	}

	public void setLoanCategoryId(Long loanCategoryId) {
		this.loanCategoryId = loanCategoryId;
	}

	public Long getChannelPartnerId() {
		return channelPartnerId;
	}

	public void setChannelPartnerId(Long channelPartnerId) {
		this.channelPartnerId = channelPartnerId;
	}

	public String getApplicationTypeName() {
		return applicationTypeName;
	}

	public void setApplicationTypeName(String applicationTypeName) {
		this.applicationTypeName = applicationTypeName;
	}

	public UserBO getChannelPartner() {
		return channelPartner;
	}

	public void setChannelPartner(UserBO channelPartner) {
		this.channelPartner = channelPartner;
	}

	public List<ApplicationsBO> getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(List<ApplicationsBO> applicationId) {
		this.applicationId = applicationId;
	}
}

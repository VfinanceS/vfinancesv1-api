package com.lams.model.bo.report;

public class LenderDetails {

	private Long id;
	private String lenderName;
	private String brRespondedToLenderStatus;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLenderName() {
		return lenderName;
	}
	public void setLenderName(String lenderName) {
		this.lenderName = lenderName;
	}
	public String getBrRespondedToLenderStatus() {
		return brRespondedToLenderStatus;
	}
	public void setBrRespondedToLenderStatus(String brRespondedToLenderStatus) {
		this.brRespondedToLenderStatus = brRespondedToLenderStatus;
	}
}

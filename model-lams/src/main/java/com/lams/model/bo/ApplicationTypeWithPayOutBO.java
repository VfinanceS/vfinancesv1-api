package com.lams.model.bo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationTypeWithPayOutBO {

	private Long id;
	private String name;
	private String code;
	private Long userId;
	
	private List<PayOutStructureBO> payOuts;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<PayOutStructureBO> getPayOuts() {
		return payOuts;
	}
	public void setPayOuts(List<PayOutStructureBO> payOuts) {
		this.payOuts = payOuts;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}

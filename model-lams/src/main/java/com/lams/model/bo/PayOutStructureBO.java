package com.lams.model.bo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayOutStructureBO  {

	private Long id;
	
	private Long applicationTypeId;
	
	private Double disbursmentFromAmt;
	
	private Double disbursmentToAmt;
	
	private Double payOutInPerc;
	
	private String version;
	
	private Date createdDate;
	
	private Long createdBy;
	
	private Date modifiedDate;
	
	private Double modifiedBy;
	
	private Boolean isActive;

	private List<UserBO> channelPartners;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplicationTypeId() {
		return applicationTypeId;
	}

	public void setApplicationTypeId(Long applicationTypeId) {
		this.applicationTypeId = applicationTypeId;
	}

	public Double getDisbursmentFromAmt() {
		return disbursmentFromAmt;
	}

	public void setDisbursmentFromAmt(Double disbursmentFromAmt) {
		this.disbursmentFromAmt = disbursmentFromAmt;
	}

	public Double getDisbursmentToAmt() {
		return disbursmentToAmt;
	}

	public void setDisbursmentToAmt(Double disbursmentToAmt) {
		this.disbursmentToAmt = disbursmentToAmt;
	}

	public Double getPayOutInPerc() {
		return payOutInPerc;
	}

	public void setPayOutInPerc(Double payOutInPerc) {
		this.payOutInPerc = payOutInPerc;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Double getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Double modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public List<UserBO> getChannelPartners() {
		return channelPartners;
	}

	public void setChannelPartners(List<UserBO> channelPartners) {
		this.channelPartners = channelPartners;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicationTypeId == null) ? 0 : applicationTypeId.hashCode());
		result = prime * result + ((channelPartners == null) ? 0 : channelPartners.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((disbursmentFromAmt == null) ? 0 : disbursmentFromAmt.hashCode());
		result = prime * result + ((disbursmentToAmt == null) ? 0 : disbursmentToAmt.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isActive == null) ? 0 : isActive.hashCode());
		result = prime * result + ((modifiedBy == null) ? 0 : modifiedBy.hashCode());
		result = prime * result + ((modifiedDate == null) ? 0 : modifiedDate.hashCode());
		result = prime * result + ((payOutInPerc == null) ? 0 : payOutInPerc.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PayOutStructureBO other = (PayOutStructureBO) obj;
		if (applicationTypeId == null) {
			if (other.applicationTypeId != null)
				return false;
		} else if (!applicationTypeId.equals(other.applicationTypeId))
			return false;
		if (channelPartners == null) {
			if (other.channelPartners != null)
				return false;
		} else if (!channelPartners.equals(other.channelPartners))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (disbursmentFromAmt == null) {
			if (other.disbursmentFromAmt != null)
				return false;
		} else if (!disbursmentFromAmt.equals(other.disbursmentFromAmt))
			return false;
		if (disbursmentToAmt == null) {
			if (other.disbursmentToAmt != null)
				return false;
		} else if (!disbursmentToAmt.equals(other.disbursmentToAmt))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isActive == null) {
			if (other.isActive != null)
				return false;
		} else if (!isActive.equals(other.isActive))
			return false;
		if (modifiedBy == null) {
			if (other.modifiedBy != null)
				return false;
		} else if (!modifiedBy.equals(other.modifiedBy))
			return false;
		if (modifiedDate == null) {
			if (other.modifiedDate != null)
				return false;
		} else if (!modifiedDate.equals(other.modifiedDate))
			return false;
		if (payOutInPerc == null) {
			if (other.payOutInPerc != null)
				return false;
		} else if (!payOutInPerc.equals(other.payOutInPerc))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
	
}
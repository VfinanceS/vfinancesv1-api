package com.lams.model.bo;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromoCodeTransactionsBO extends AuditorBO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Long promoCode;
	
	private Long validityDays;
	
	private Date createdDate;
	
	private Boolean isActive;
	
	private Long usedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(Long promoCode) {
		this.promoCode = promoCode;
	}

	public Long getValidityDays() {
		return validityDays;
	}

	public void setValidityDays(Long validityDays) {
		this.validityDays = validityDays;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(Long usedBy) {
		this.usedBy = usedBy;
	}
}